# Payment Funnel with Universal React and Redux

This is based upon the React comment box example from [the React tutorial](http://facebook.github.io/react/docs/tutorial.html).

## To use

Requirements:
* NodeJS (https://nodejs.org/en/download/)
* Gulp (```sudo npm install -g gulp```)
* docker-manager-nodejs (https://github.com/abaenglish/docker-manager-nodejs)

_min version: 5.6.0 (https://nodejs.org/en/blog/release/v5.6.0/)_

If you prefer you can use a [Node Version Manager](#node-version-manager)

### Start

If you just have cloned the project, you need to create a file called ```.env``` with:
``` sh
ENVIRONMENT=local
NODE_ENV=development
PORT=3000
```

Nexus Repository Manager is currently used to store ABA's private npm pacakges as well as acting as a proxy to npm's central repo. npm must be configured so that it points to nexus. instructions of how to do this can be found in the [Software Guidebook](https://github.com/abaenglish/software-guidebook/blob/23427811fc6c28e755b3c59c4f5025576acf8089/subplatforms/new_aba_platform.md#npm-packages).

Then install npm packages and start the app:
```sh
$ npm install
$ npm run buildAndStart
```

And visit <http://localhost:3000/>.

## Live reloading

You can view any changes made to css and jsx during development when running the project with gulp with an appropriate
browser extension installed (<http://livereload.com/extensions/>).

## App configuration & environment variables

The app's config (api endpoints, google tag manager sdk keys, etc.) varies between deploys (qa, production, developer environments, etc.).
In order to seperate config from code, the app's config is built at compile-time using variables configured on a remote server. This server is an instance of
Spring Cloud Config. Spring Cloud Config provides server and client-side support for externalized configuration in a
distributed system. With the Config Server you have a central place to manage external properties for applications across all environments.

The enpoint of the config server is hardcoded into the build script, but the application environment (dev, production, etc.)
must be specified by a local environment variable called ENVIRONMENT. This is used to determine which set of config values
the Spring Cloud Config server returns. You can see the available options for ENVIRONMENT in [payment-funnel.yml](https://github.com/abaenglish/cloud-config-server-properties/blob/master/payment-funnel.yml)


### Updating the shared app config

The app's config is stored in an intstance of Spring Cloud Config Server. To update the config,
first git clone https://github.com/abaenglish/cloud-config-server-properties.git. The config file for this project
is called [payment-funnel.yml](https://github.com/abaenglish/cloud-config-server-properties/blob/master/payment-funnel.yml). Any ammendments made to this file and pushed back to master will get used in the
next build of the project provided that the profile under which changes were made matches the local env variable ENVIRONMENT

## Linting

To analyse the code for potential errors, run

```
npm run-script lint
```

## Run tests

### Unit tests
These tests are written with Mocha, and mocking of es6 modules permitted with babel-plugin-rewire
```sh
$ gulp tests
```

TODO: go into greater depth about mocking

## Creating docker image

You can create a docker image of the project using the following command (note that Docker must be installed on your machine):

```sh
$ docker build .
```

## Deploy

Jenkins is an open source continuous integration tool, and is the tool we have chosen to deploy the app to be made publicly accessible
either internally or externally. On jenkins, a Pipeline Plugin has been installed to facilitate the creation of a continuous delivery pipeline.
The pipeline (a series of automated steps to execute in order to deploy the app, e.g. build app, run unit tests, finally deploy app) is expressed as a script
 (written with the Groovy syntax) that the Pipeline Plugin is able to understand and execute. This script is found in the root of this project in a file
  called Jenkinsfile.develop

### Docker and Amazon Web Service

During the deploy process, a docker image of the app will be created (and tagged with the git commit used to trigger the build),
and then pushed to a centralised repository (defined in the Jenkinsfile script mentioned above).
This docker image will get deployed to amazon elastic beanstalk

## Crash reports

Reports are sent to [New Relic](https://rpm.newrelic.com).

## Node Version Manager

 - install NVM using cURL:

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
```

 - Download and install a required version of Node. Uses .nvmrc
```sh
nvm install
```

 - Modify PATH to use the selected version Uses .nvmrc
```sh
nvm use
```

 - Install *gulp* in this version of Node (**whithout sudo!!!**)
```sh
npm install -g gulp
```

 - go to [start](#start)



### Braindump (please ignore)

#### Useful commands
docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_DATABASE=subscription-service --name mysql mysql:5.7
docker run -p 4000:8080 --link mysql:database nexus.aba.land:5000/subscription-service:1.1.1.1 --spring.datasource.url="jdbc:mysql://database/subscription-service?useSSL=false"

curl -i -u guest:guest -H "Content-Type: application/json" -XPOST -d '{"properties":{"content_type":"application/json"},"routing_key":"CREATED","payload":"{\"transactionId\":\"bfb2be6f-5e0a-4df9-8081-38bba86e4bb6\",\"zuoraId\":\"123\"}","payload_encoding":"string"}' http://192.168.64.3:32703/api/exchanges/%2f/subscription-service/publish

curl -i -u guest:guest -H "Content-Type: application/json" -XPOST -d '{"properties":{"content_type":"application/json"},"routing_key":"CREATED","payload":"{\"publicId\":\"bb5fa1c7-51be-47da-a9ba-a013d1fa923f\",\"zuoraId\":\"123\"}","payload_encoding":"string"}' http://192.168.64.3:32703/api/exchanges/%2f/subscription-service/publish

curl -i -u guest:guest -H "Content-Type: application/json" -XPOST -d '{"properties":{"content_type":"application/json"},"routing_key":"CREATED","payload":"{\"publicId\":\"bb5fa1c7-51be-47da-a9ba-a013d1fa923f\",\"zuoraId\":\"123\"}","payload_encoding":"string"}' http://a17c3008783d511e695750a6cda13ea5-1588048260.eu-west-1.elb.amazonaws.com:32703/api/exchanges/%2f/subscription-service-dev/publish


