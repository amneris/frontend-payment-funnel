FROM node:5.11.1-slim

MAINTAINER abaenglish

ENV HOME=/www

# Bundle app source
COPY dist $HOME/dist/
COPY package.json $HOME/package.json
COPY gulpfile.js $HOME/gulpfile.js
#COPY npm-shrinkwrap.json $HOME/npm-shrinkwrap.json
# node modules are installed in this path by jenkins to be allowed to use npmrc
COPY production_node_modules/node_modules $HOME/node_modules/

WORKDIR $HOME

EXPOSE 80:80

ENTRYPOINT ["npm", "run", "fetchconfigandstartserver"]
