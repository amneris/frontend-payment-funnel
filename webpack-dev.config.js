var webpack = require('webpack');
var path = require('path');

module.exports = {
  cache: true,
  entry: {

    config: ['./source/config/appConfig.js'],
    vendor: ['react',
      'react-dom',
      'react-redux',
      'react-router',
      'redux-persist',
      'query-string',
      'react-bootstrap/lib/Panel',
      'date-utils',
      'react-scroll',
      'react-bootstrap/lib/Carousel',
      'react-bootstrap/lib/CarouselItem',
      'isomorphic-fetch'
    ],
    build: ['./source/client.js']
  },
  output: {
    filename: '[name].min.js',
    path: path.resolve(__dirname, 'dist')/*,
    devtoolModuleFilenameTemplate: function(info){
      return "file:///"+encodeURI(info.absoluteResourcePath);
    }*/
  },
  module: {
    loaders: [{
      loader: 'babel-loader',
      test: /\.(js|es6)$/,
      exclude: [/(node_modules)/],
      query: {
        plugins: ['transform-runtime'],
        presets: ['es2015', 'stage-0', 'react']
      }
    }, {
      test: /\.json?$/,
      loader: 'json-loader'
    }]
  },
  resolve: {
    extensions: ['', '.js', '.json']
  },
  devtool: 'eval-source-map',
  plugins: [
    new webpack.SourceMapDevToolPlugin({
      columns: false,
      filename: '[file].map',
      exclude: [
        'vendor.min.js',
        'config.min.js'
      ]
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: `"${process.env.NODE_ENV || 'development'}"`
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'config']
    })
  ]
};
