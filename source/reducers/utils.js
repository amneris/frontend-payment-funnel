export const DEFAULT_LANGUAGE = 'en';
export const DEFAULT_LOCALE = 'en-EN';
export const DEFAULT_CONTACT_PHONE = '+34 93 220 24 83';
export const GUARANTEE_DAYS = 14;
export const DEFAULT_CONTACT_EMAIL = 'support@abaenglish.com';
export const ZENDESK_URL = 'https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998';
export const ZENDESK_URL_LOCALE = 'https://abaenglish.zendesk.com/hc/{locale}/requests/new?ticket_form_id=191998';
export const ZENDESK_DEFAULT_LOCALE = 'en-gb';
export const LEGAL_TEXT_URL = 'https://static.abaenglish.com/legal/';
export const LEGAL_TEXT_TC_URL = 'tc_{locale}.html';
export const LEGAL_TEXT_PRIVACY_URL = 'privacy_{locale}.html';
const SUPPORTED_LANGUAGES = ['de', 'en', 'es', 'fr', 'it', 'pt', 'ru'];
export const SUPPORTED_LOCALES = ['de-DE', 'en-EN', 'es-ES', 'fr-FR', 'it-IT', 'pt-PT', 'ru-RU'];
const PATH_SEPARATOR = '/';
const ZENDESK_SUPPORTED_LOCALES = ['de','en-gb','es','fr','it','pt-br','ru'];
export const SOCIAL_URL_LINKEDIN = 'https://www.linkedin.com/edu/school?id=171043';
export const SOCIAL_URL_FACEBOOK = 'https://www.facebook.com/ABAEnglish.es/?brand_redir=22689541360'
export const SOCIAL_URL_TWITTER = 'https://twitter.com/ABAEnglish';
export const SOCIAL_URL_BLOG = 'http://blog.abaenglish.com/{locale}';
export const SOCIAL_URL_SOUNDCLOUD = 'https://soundcloud.com/abaenglish';
export const SOCIAL_URL_YOUTUBE = 'https://www.youtube.com/user/abaenglish';
const SUPPORTED_SOCIAL_BLOG_LANGUAGES = ['en', 'es', 'it', 'pt', 'fr'];

// returns a valid locale to load the app
export function convertLanguageToLocale(language) {
  language = language || getDefaultLanguage();

  if (language.length === 2) {
    for (var i = 0; i < SUPPORTED_LOCALES.length; i++) {
      if (startsWith(SUPPORTED_LOCALES[i],language)) {
        return SUPPORTED_LOCALES[i];
      }
    }
  }

  return getDefaultLocale();
}

export function getLanguageFromUrl(pathname = PATH_SEPARATOR) {
  if (pathname) {
    const urlParts = pathname.split(PATH_SEPARATOR);

    if (urlParts.length > 1) {
      var found = SUPPORTED_LANGUAGES.find(x => x === urlParts[1]);

      return found || getDefaultLanguage();
    }
  }

  return getDefaultLanguage();
}

export function getDefaultLanguage() {
  return DEFAULT_LANGUAGE;
}

export function getDefaultLocale() {
  return DEFAULT_LOCALE;
}

export function getDefaultContactPhone() {
  return DEFAULT_CONTACT_PHONE;
}

export function getDefaultContactEmail() {
  return DEFAULT_CONTACT_EMAIL;
}

export function getDefaultHelpDeskLink() {
  return ZENDESK_URL;
}

export function getHelpDeskLink(language) {
  if (language) {
    language = language || getDefaultLanguage();
    var locale = convertLanguageToHelpDeskLocale(language);
    var link = ZENDESK_URL_LOCALE;
    link = link.replace('{locale}', locale);

    return link;
  }

  return getDefaultHelpDeskLink();
}

export function getDefaultHelpDeskLocale() {
  return ZENDESK_DEFAULT_LOCALE;
}

export function getHelpDeskLinkWithEmail(language, email) {
 var link = getHelpDeskLink(language);
  if (email)  link = link.concat('&email=').concat(encodeURIComponent(email));
  return link;
}

export function convertLanguageToHelpDeskLocale(language) {
  language = language || getDefaultLanguage();

  if (language.length === 2) {
    for (var i = 0; i < ZENDESK_SUPPORTED_LOCALES.length; i++) {
      if (startsWith(ZENDESK_SUPPORTED_LOCALES[i],language)) {
        return ZENDESK_SUPPORTED_LOCALES[i];
      }
    }
  }

  return getDefaultHelpDeskLocale();
}

export function getLegalTermsAndConditionsUrl(language = getDefaultLanguage()) {
  let link = LEGAL_TEXT_URL.concat(LEGAL_TEXT_TC_URL);
  link = link.replace('{locale}', language);

  return link;
}

export function getLegalPrivacyPolicyUrl(language = getDefaultLanguage()) {
  let link = LEGAL_TEXT_URL.concat(LEGAL_TEXT_PRIVACY_URL);
  link = link.replace('{locale}', language);

  return link;
}

export function priceToString(price, decimalSeparator) {
  var strPrice = price.toFixed(2);
  var priceParts = strPrice.split('.');

  strPrice = priceParts[0] + decimalSeparator + priceParts[1];

  return strPrice;
}

export function priceWithCurrency(price, currency) {
  var strPrice = priceToString(price, currency.decimalSeparator);

  if (currency.symbolShowBeforePrice) {
    strPrice = currency.symbol + strPrice;
  }
  else {
    strPrice = strPrice + currency.symbol;
  }

  return strPrice;
}

export function createInputElement(type,name,value) {
  if (!type || !name)  return undefined;

  let field = document.createElement('input');
  field.setAttribute('type',type);
  field.setAttribute('name',name);
  if (value)  field.setAttribute('value',value);

  return field;
}

export function createInputHiddenElement(name,value) {
  return createInputElement('hidden',name,value);
}

export function startsWith(origin,str) {
  if (!str) return false;
  if (!String.prototype.startsWith) {
    //return (origin.indexOf(str) === 0);
    return (origin.lastIndexOf(str, 0) === 0);
  } else {
    return origin.startsWith(str);
  }
}

export function getSocialLinkedinLink() {
  return SOCIAL_URL_LINKEDIN;
}

export function getSocialFacebookLink() {
  return SOCIAL_URL_FACEBOOK;
}

export function getSocialTwitterLink() {
  return SOCIAL_URL_TWITTER;
}

export function getSocialBlogLink(language) {

  language = language || '';

  var found = SUPPORTED_SOCIAL_BLOG_LANGUAGES.find(x => x === language);
  if(found) {
    return SOCIAL_URL_BLOG.replace('{locale}', language);
  }
  return SOCIAL_URL_BLOG.replace('{locale}', '');
}

export function getSocialSoundcloudLink() {
  return SOCIAL_URL_SOUNDCLOUD;
}
export function getSocialYoutubeLink() {
  return SOCIAL_URL_YOUTUBE;
}
