import {
  REQUEST_PLANS, RECEIVE_PLANS, RECEIVE_PLANS_ERROR
} from '../actions/actionTypes';

export function plans(state = { //NOSONAR
  isFetching: false,
  items: undefined,
  currency: undefined
}, action) {
  switch (action.type) {
    case REQUEST_PLANS:
      return Object.assign({}, state, {
        isFetching: true
      });

    case RECEIVE_PLANS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.items,
        currency: action.currency
      });

    case RECEIVE_PLANS_ERROR:
      return Object.assign({}, state, {
        isFetching: false
      });

    default:
      return state;
  }
}
