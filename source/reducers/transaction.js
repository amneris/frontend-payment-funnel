import {
  REQUEST_PAYPAL_TOKEN, RECEIVE_PAYPAL_TOKEN, START_TRANSACTION, REQUEST_TRANSACTION_INFO,
  QUEUE_REQUEST_TRANSACTION_INFO, RECEIVE_TRANSACTION_INFO, SET_TRANSACTION_ERROR
} from '../actions/actionTypes';

export function transaction(state = {
  isFetching: false,
  haveQueuedRequest: false,
  transactionInfo: undefined,
  transactionError : undefined
}, action) {
  switch (action.type) {
    case REQUEST_PAYPAL_TOKEN:
      return Object.assign({}, state, {
        isFetching: true,
        transactionError: undefined
      });

    case RECEIVE_PAYPAL_TOKEN:
      return Object.assign({}, state, {
        isFetching: false,
        paypalToken: action.paypalToken,
        transactionError: undefined
      });

    case START_TRANSACTION:
      return Object.assign({}, state, {
        transactionInfo: undefined,
        transactionError: undefined
      });

    case REQUEST_TRANSACTION_INFO:
      return Object.assign({}, state, {
        isFetching: true,
        haveQueuedRequest: false,
        transactionError: undefined
      });

    case QUEUE_REQUEST_TRANSACTION_INFO:
      return Object.assign({}, state, {
        haveQueuedRequest: true
      });

    case RECEIVE_TRANSACTION_INFO:
      return Object.assign({}, state, {
        isFetching: false,
        transactionInfo: action.transactionInfo,
        transactionError: undefined
      });

    case SET_TRANSACTION_ERROR:
      return Object.assign({}, state, {
        isFetching: false,
        transactionError: action.errorType
      });

    default:
      return state;
  }
}
