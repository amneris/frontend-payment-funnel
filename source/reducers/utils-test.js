import expect from 'expect'

import {
  DEFAULT_LANGUAGE, DEFAULT_LOCALE, DEFAULT_CONTACT_PHONE, DEFAULT_CONTACT_EMAIL, ZENDESK_URL, ZENDESK_DEFAULT_LOCALE,
  SUPPORTED_LOCALES, LEGAL_TEXT_URL,
  getLanguageFromUrl, getDefaultLocale, getDefaultLanguage, getDefaultContactPhone, getDefaultContactEmail,
  getDefaultHelpDeskLink, getHelpDeskLink, getDefaultHelpDeskLocale, convertLanguageToHelpDeskLocale, getHelpDeskLinkWithEmail,
  convertLanguageToLocale, priceToString, priceWithCurrency,
  getLegalTermsAndConditionsUrl, getLegalPrivacyPolicyUrl, startsWith
} from './utils';

describe('Convert language to locale', () => {
  it('should return the best locale depending on language', () => {
    expect(convertLanguageToLocale('es')).toBe('es-ES');
    expect(convertLanguageToLocale('ru')).toBe('ru-RU');
    expect(convertLanguageToLocale('something')).toBe(DEFAULT_LOCALE);
    expect(convertLanguageToLocale(getDefaultLanguage())).toBe(DEFAULT_LOCALE);
    expect(convertLanguageToLocale()).toBe(DEFAULT_LOCALE);
  });
});

describe('Get the language from the URL', () => {
  it('should return the best locale depending on language', () => {
    expect(getLanguageFromUrl('/it/plans')).toBe('it');
    expect(getLanguageFromUrl('/something/plans')).toBe(DEFAULT_LANGUAGE);
    expect(getLanguageFromUrl('/something')).toBe(DEFAULT_LANGUAGE);
    expect(getLanguageFromUrl('no_slash')).toBe(DEFAULT_LANGUAGE);
    expect(getLanguageFromUrl()).toBe(DEFAULT_LANGUAGE);
  });
});

describe('Get the default values', () => {
  it('should return the default value of language', () => {
    expect(getDefaultLanguage()).toBe(DEFAULT_LANGUAGE);
  });

  it('should return the default value of locale', () => {
    expect(getDefaultLocale()).toBe(DEFAULT_LOCALE);
  });

  it('should return the default value of contact phone', () => {
    expect(getDefaultContactPhone()).toBe(DEFAULT_CONTACT_PHONE);
  });

  it('should return the default value of contact email', () => {
    expect(getDefaultContactEmail()).toBe(DEFAULT_CONTACT_EMAIL);
  });

  it('should return the default value of the Help Desk Link', () => {
    expect(getDefaultHelpDeskLink()).toBe(ZENDESK_URL);
  });

  it('should return the default value of the Help Desk locale', () => {
    expect(getDefaultHelpDeskLocale()).toBe(ZENDESK_DEFAULT_LOCALE);
  });
});

describe('Get the Help Desk Link', () => {
  it('should return the value of help desk link depending on language', () => {
    expect(getHelpDeskLink()).toBe('https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink(null)).toBe('https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('de')).toBe('https://abaenglish.zendesk.com/hc/de/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('en')).toBe('https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('es')).toBe('https://abaenglish.zendesk.com/hc/es/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('fr')).toBe('https://abaenglish.zendesk.com/hc/fr/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('it')).toBe('https://abaenglish.zendesk.com/hc/it/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('pt')).toBe('https://abaenglish.zendesk.com/hc/pt-br/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLink('ru')).toBe('https://abaenglish.zendesk.com/hc/ru/requests/new?ticket_form_id=191998');
  });
});

describe('Convert language to Zendesk locale', () => {
  it('should return the correct locale depending on language', () => {
    expect(convertLanguageToHelpDeskLocale()).toBe('en-gb');
    expect(convertLanguageToHelpDeskLocale('null')).toBe('en-gb');
    expect(convertLanguageToHelpDeskLocale('de')).toBe('de');
    expect(convertLanguageToHelpDeskLocale('en')).toBe('en-gb');
    expect(convertLanguageToHelpDeskLocale('es')).toBe('es');
    expect(convertLanguageToHelpDeskLocale('fr')).toBe('fr');
    expect(convertLanguageToHelpDeskLocale('it')).toBe('it');
    expect(convertLanguageToHelpDeskLocale('pt')).toBe('pt-br');
    expect(convertLanguageToHelpDeskLocale('ru')).toBe('ru');
  });
});

describe('Get the Help Desk Link with email', () => {
  it('should return the value of help desk with email depending on language', () => {
    expect(getHelpDeskLinkWithEmail()).toBe('https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLinkWithEmail(null,null)).toBe('https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLinkWithEmail('de')).toBe('https://abaenglish.zendesk.com/hc/de/requests/new?ticket_form_id=191998');
    expect(getHelpDeskLinkWithEmail('de',null)).toBe('https://abaenglish.zendesk.com/hc/de/requests/new?ticket_form_id=191998');

    let email = 'useremail@abaenglish.com';
    expect(getHelpDeskLinkWithEmail('de', email)).toBe('https://abaenglish.zendesk.com/hc/de/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
    expect(getHelpDeskLinkWithEmail('en', email)).toBe('https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
    expect(getHelpDeskLinkWithEmail('es', email)).toBe('https://abaenglish.zendesk.com/hc/es/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
    expect(getHelpDeskLinkWithEmail('fr', email)).toBe('https://abaenglish.zendesk.com/hc/fr/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
    expect(getHelpDeskLinkWithEmail('it', email)).toBe('https://abaenglish.zendesk.com/hc/it/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
    expect(getHelpDeskLinkWithEmail('pt', email)).toBe('https://abaenglish.zendesk.com/hc/pt-br/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
    expect(getHelpDeskLinkWithEmail('ru', email)).toBe('https://abaenglish.zendesk.com/hc/ru/requests/new?ticket_form_id=191998&email=' + encodeURIComponent(email));
  });
});

describe('Get the Legal texts url', () => {
  it('should return the Terms & Conditions url depending on language' , () => {
    expect(getLegalTermsAndConditionsUrl()).toBe('https://static.abaenglish.com/legal/tc_en.html');
    expect(getLegalTermsAndConditionsUrl('de')).toBe('https://static.abaenglish.com/legal/tc_de.html');
    expect(getLegalTermsAndConditionsUrl('en')).toBe('https://static.abaenglish.com/legal/tc_en.html');
    expect(getLegalTermsAndConditionsUrl('es')).toBe('https://static.abaenglish.com/legal/tc_es.html');
    expect(getLegalTermsAndConditionsUrl('fr')).toBe('https://static.abaenglish.com/legal/tc_fr.html');
    expect(getLegalTermsAndConditionsUrl('it')).toBe('https://static.abaenglish.com/legal/tc_it.html');
    expect(getLegalTermsAndConditionsUrl('pt')).toBe('https://static.abaenglish.com/legal/tc_pt.html');
    expect(getLegalTermsAndConditionsUrl('ru')).toBe('https://static.abaenglish.com/legal/tc_ru.html');
  });

  it('should return the Privacy url depending on language' , () => {
    expect(getLegalPrivacyPolicyUrl()).toBe('https://static.abaenglish.com/legal/privacy_en.html');
    expect(getLegalPrivacyPolicyUrl('de')).toBe('https://static.abaenglish.com/legal/privacy_de.html');
    expect(getLegalPrivacyPolicyUrl('en')).toBe('https://static.abaenglish.com/legal/privacy_en.html');
    expect(getLegalPrivacyPolicyUrl('es')).toBe('https://static.abaenglish.com/legal/privacy_es.html');
    expect(getLegalPrivacyPolicyUrl('fr')).toBe('https://static.abaenglish.com/legal/privacy_fr.html');
    expect(getLegalPrivacyPolicyUrl('it')).toBe('https://static.abaenglish.com/legal/privacy_it.html');
    expect(getLegalPrivacyPolicyUrl('pt')).toBe('https://static.abaenglish.com/legal/privacy_pt.html');
    expect(getLegalPrivacyPolicyUrl('ru')).toBe('https://static.abaenglish.com/legal/privacy_ru.html');
  });
});

describe('Format price from number to string', () => {
  it('should return the price with the proper decimal separator', () => {
    expect(priceToString(2, '.')).toBe('2.00');
    expect(priceToString(20.450, '.')).toBe('20.45');
    expect(priceToString(2.30, ',')).toBe('2,30');
  });
});

describe('Format price from number to string with proper currency', () => {
  it('should return the price with the proper currency after the price', () => {
    const currency = { symbol: '€', iso: 'EUR', symbolShowBeforePrice: false, decimalSeparator: '.' };

    expect(priceWithCurrency(2, currency)).toBe('2.00€');
    expect(priceWithCurrency(20.450, currency)).toBe('20.45€');
    expect(priceWithCurrency(2.30, currency)).toBe('2.30€');
  });

  it('should return the price with the proper currency after the price', () => {
    const currency = { symbol: '$', iso: 'USD', symbolShowBeforePrice: true, decimalSeparator: ',' };

    expect(priceWithCurrency(2, currency)).toBe('$2,00');
    expect(priceWithCurrency(20.450, currency)).toBe('$20,45');
    expect(priceWithCurrency(2.30, currency)).toBe('$2,30');
  });
});

describe('Check if a string starts with another string', () => {
  it('should return if a string starts with another', () => {
    expect(startsWith('mytestisawesome','my')).toBeTruthy();
    expect(startsWith('mytestisawesome','mytest')).toBeTruthy();
    expect(startsWith('mytestisawesome','awesome')).toBeFalsy();
    expect(startsWith('mytestisawesome','your')).toBeFalsy();
    expect(startsWith('mytestisawesome','')).toBeFalsy();
    expect(startsWith('mytestisawesome',null)).toBeFalsy();
    expect(startsWith('mytestisawesome')).toBeFalsy();
    expect(startsWith('mytestisawesome',undefined)).toBeFalsy();
  });

  it('should return if an array elemento starts with a string', () => {
    expect(startsWith(SUPPORTED_LOCALES[0],'de')).toBeTruthy();
    expect(startsWith(SUPPORTED_LOCALES[0],'es')).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0],'')).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0],null)).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0])).toBeFalsy();
    expect(startsWith(SUPPORTED_LOCALES[0],undefined)).toBeFalsy();
  });
});
