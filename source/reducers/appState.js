import { getDefaultContactPhone,getDefaultContactEmail } from './utils';
import {
  SET_APP_STATE, SET_URL_PARAMS, SELECT_PLAN
} from '../actions/actionTypes';

export const appStateInit = {
  language: null,
  urlParams: {},
  title: null,
  startApp: false,
  phoneNumber: getDefaultContactPhone(),
  emailContact: getDefaultContactEmail(),
  segmentId: undefined,
  shoppingCart: undefined
};

export function appState(state = appStateInit, action) { //NOSONAR

  switch (action.type) {
    case SET_APP_STATE:
      return Object.assign({}, state,
        action.appState
      );

    case SET_URL_PARAMS:
      return Object.assign({}, state, {
          urlParams: action.urlParams
        }
      );

    case SELECT_PLAN:
      return Object.assign({}, state,
        {
          shoppingCart: {
            plan: action.planId,
            discount: action.discountId,
            period: action.period,
            planTitleKey: action.planTitleKey
          }
        }
      );

    default:
      return state;
  }
}
