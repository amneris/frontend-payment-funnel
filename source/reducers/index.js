import { combineReducers } from 'redux';
import {
  REQUEST_USER_SESSION, RECEIVE_USER_SESSION, CONVERT_USER_TO_PREMIUM, RECEIVE_TRANSACTION_INFO
} from '../actions/actionTypes';
import { getDefaultLocale, convertLanguageToLocale } from './utils';
import { appState } from './appState';
import { plans } from './plans';
import { checkout } from './checkout';
import { review } from './review';
import { externalContent } from './externalContent';
import { transaction } from './transaction';

function userSession(state = { //NOSONAR
  isFetching: false,
  locale: getDefaultLocale(),
  language: null,
  id: null,
  type: null,
  name: null,
  email: null,
  currency: null
}, action) {

  switch (action.type) {

    case REQUEST_USER_SESSION:
      return Object.assign({}, state, {
        isFetching: true
      });

    case RECEIVE_USER_SESSION:
      return Object.assign({}, state, {
        isFetching: false,
        fetchResult: 'ok',
        locale: convertLanguageToLocale(action.userSession.language)
      }, action.userSession);

    case CONVERT_USER_TO_PREMIUM:

      return Object.assign({}, state, {
        type: 'premium'
      });

    case RECEIVE_TRANSACTION_INFO:

      let userIsPremiumTransactionStates = ["PROCESS_ABA", "CLOSED"]; //todo use ABA_CONFIRMED instead of PROCESS_ABA

      if (action.transactionInfo && userIsPremiumTransactionStates.includes(action.transactionInfo.state)){

        return Object.assign({}, state, {
          type: 'premium'
        });
      }

    default:
      return state;
  }
}

const rootReducer = combineReducers({
  appState, userSession, plans, checkout, review, externalContent, transaction
});

export default rootReducer;
