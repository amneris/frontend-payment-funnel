import expect from 'expect'

import {
  REQUEST_PLANS, RECEIVE_PLANS, RECEIVE_PLANS_ERROR
} from '../actions/actionTypes';

import { plans }
  from './plans';

describe

describe('plans reducer', () => {
  it('should return the initial state', () => {
    expect(
      plans(undefined, {})
    ).toEqual({
      isFetching: false,
      items: undefined,
      currency: undefined
    });
  });

  it('should handle REQUEST_PLANS', () => {
    expect(
      plans(undefined, { type: REQUEST_PLANS })
    ).toEqual({
      isFetching: true,
      items: undefined,
      currency: undefined
    });
  });

  it('should handle RECEIVE_PLANS', () => {
    expect(
      plans({}, {
        type: RECEIVE_PLANS,
        items: [ { plan1: 'valuePlan1' }, { plan2: 'valuePlan1' } ],
        currency: { paramCurrency: 'valueCurrency' }
      })
    ).toEqual({
      isFetching: false,
      items: [ { plan1: 'valuePlan1' }, { plan2: 'valuePlan1' } ],
      currency: { paramCurrency: 'valueCurrency' }
    });
  });

  it('should handle RECEIVE_PLANS_ERROR', () => {
    expect(
      plans({}, { type: RECEIVE_PLANS_ERROR })
    ).toEqual({
      isFetching: false
    });
  });
});
