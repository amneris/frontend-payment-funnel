import {
  SWIPE_REWIEW_CAROUSEL, SELECT_REWIEW_CAROUSEL
} from '../actions/actionTypes';

export function review(state = {
  activeIndex: 0

}, action) {
  switch (action.type) {
    case SWIPE_REWIEW_CAROUSEL:
      let numberOfReviews = action.numberOfReviews;
      let firstFeatureIndex = 0;
      let lastFeatureIndex = numberOfReviews - 1;
      let currentActiveFeatureIndex = state.activeIndex;
      let newActiveFeatureIndex;

      switch (action.direction) {
        case 'prev':
          newActiveFeatureIndex = currentActiveFeatureIndex - 1;
          break;
        case 'next':
          newActiveFeatureIndex = currentActiveFeatureIndex + 1;
          break;
        default:
          throw new Error('Direction not found');
      }

      if (newActiveFeatureIndex < 0)
        newActiveFeatureIndex = lastFeatureIndex;

      if (newActiveFeatureIndex > lastFeatureIndex)
        newActiveFeatureIndex = firstFeatureIndex;

      return Object.assign({}, state, {
        direction: action.direction,
        numberOfReviews: action.numberOfReviews,
        activeIndex: newActiveFeatureIndex
      });

    case SELECT_REWIEW_CAROUSEL:
      return Object.assign({}, state, {
        activeIndex: action.index
      });

    default:
      return state;
  }
}
