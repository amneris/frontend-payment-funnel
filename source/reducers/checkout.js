import {
  REQUEST_METHODS, RECEIVE_METHODS, RECEIVE_METHODS_ERROR,
  REQUEST_SIGNATURE, RECEIVE_SIGNATURE, SELECT_METHOD, RESET_CREDENTIALS
} from '../actions/actionTypes';

export function checkout(state = {
  methods: {
    items: undefined,
    isFetching: false
  },
  credentials: {
    data: undefined,
    isFetching: false
  },
  selectedMethod: undefined
}, action) {
  switch (action.type) {
    case REQUEST_METHODS:
      return Object.assign({}, state, {
        methods: {
          isFetching: true
        }
      });

    case RECEIVE_METHODS:
      return Object.assign({}, state, {
        methods: {
          items: action.methods,
          isFetching: false
        }
      });

    case RECEIVE_METHODS_ERROR:
      return Object.assign({}, state, {
        methods: {
          isFetching: false
        }
      });

    case REQUEST_SIGNATURE:
      return Object.assign({}, state, {
        credentials: {
          isFetching: true
        }
      });

    case RECEIVE_SIGNATURE:
      return Object.assign({}, state, {
        credentials: {
          data: {
            pageId: action.pageId,
            signature: action.signature,
            token: action.token,
            key: action.key,
            tenantId: action.tenantId
          },
          isFetching: false
        }
      });

    case SELECT_METHOD:
      return Object.assign({}, state, {
        selectedMethod: action.methodId
      });

    case RESET_CREDENTIALS:
      return Object.assign({}, state, {
        credentials: {
          data: undefined,
          isFetching: false
        }
      });

    default:
      return state;
  }
}
