import expect from 'expect'

import {
  SET_APP_STATE, SELECT_PLAN
} from '../actions/actionTypes';

import { appState, appStateInit }
  from './appState';

describe('appState reducer', () => {
  it('should return the initial state', () => {
    expect(
      appState(undefined, {})
    ).toEqual(appStateInit);
  });

  it('should handle SET_APP_STATE', () => {
    expect(
      appState({}, {
        type: SET_APP_STATE,
        appState: {
          title: 'Hello World',
          language: 'en'
        }
      })
    ).toEqual({
      title: 'Hello World',
      language: 'en'
    });
  });

  it('should handle SELECT_PLAN', () => {
    expect(
      appState({}, {
        type: SELECT_PLAN,
        planId: 'my-plan-id',
        discountId: 'my-discount-id',
        period:6,
        planTitleKey:'six months'

      })
    ).toEqual({
      shoppingCart: {
        plan: 'my-plan-id',
        discount: 'my-discount-id',
        period:6,
        planTitleKey:'six months'
      }
    });
  });
});
