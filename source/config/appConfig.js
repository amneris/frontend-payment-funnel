/**
 * provided configuration for entire app.
 * global variable as defined by webpack plugin
 */
export default class AppConfig {
  // TODO: this is not working for server side, because the process.env will always be DEV because we dont rebuild server
  // that means we will see proper appConfig in window but default value will take dev values and pixels in server side
  // will not have pro values
  static getValue(key, defaultValue){
    let returnValue;

    if (typeof window === 'object'){
      returnValue =  window.appConfig[key] || defaultValue;
    } else{
      returnValue = defaultValue
    }

    return returnValue
  }


  static getNodeEnvironment() {

    return this.getValue('NODE_ENV', process.env.NODE_ENV);
  }

  static getShouldPersistData() {

    return this.getValue('DATA_PERSIST', process.env.DATA_PERSIST);
  }

  static getCampusUrl() {

    return this.getValue('CAMPUS_URL', process.env.CAMPUS_URL);
  }

  static getAbaZuoraApiUrl() {

    return this.getValue('ABA_ZUORA_API_URL', process.env.ABA_ZUORA_API_URL);
  }

  static getZuoraApiUrl() {

    return this.getValue('ZUORA_API_URL', process.env.ZUORA_API_URL);
  }

  static getWebSocketsUrl() {

    return this.getValue('WEBSOCKETS_URL', process.env.WEBSOCKETS_URL);
  }

  static getPaypalActionUrl(){

    return this.getValue('PAYPAL_URL', process.env.PAYPAL_URL);
  }

  static getPaypalId(){

    return this.getValue('PAYPAL_MERCHANT_ID', process.env.PAYPAL_MERCHANT_ID);
  }

  static getITunesUrl(){

    return 'https://app.adjust.com/lcqz5t';
  }

  static getGooglePlayUrl(){

    return 'https://app.adjust.com/iizdny';
  }

  static getTransactionInfoRetryInterval(){

    return this.getValue('TRANSACTION_INFO_RETRY_INTERVAL', 30000);
  }

  static getTransactionInfoMaxRetries(){

    return this.getValue('TRANSACTION_INFO_MAX_RETRIES', 3);
  }
}
