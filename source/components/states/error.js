import React from 'react';
import {Component, PropTypes} from 'react';

export default class Error extends Component {
  constructor(props) {
    super(props);

    this.retryFunction =  props.hasOwnProperty('retryFunction') ? props.retryFunction : 'return false;';

    this.state = {
      isRetryPressed: false
    };
  }

  retry() {
    this.setState({
      isRetryPressed: true
    });

    this.retryFunction();
  }

  render() {
    return (
      <div className="aba-error">
        <div className="error-container">
          <div className="error-icon icon-cloud-cross"></div>
          <div className="error-text">{this.context.localeManager.getMessage('label error thrown')}</div>
          <button className="error-button" type="button"
                  onClick={this.retry.bind(this)}
          >
            {this.context.localeManager.getMessage('label button try again')}
          </button>
        </div>
      </div>
    );
  }
}

Error.contextTypes = {
  localeManager: PropTypes.object
}
