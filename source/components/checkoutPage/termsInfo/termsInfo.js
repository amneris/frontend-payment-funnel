import React from 'react';
import { Component, PropTypes } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { Overlay, Popover } from 'react-bootstrap';
import Loading from '../../states/loading';
import Link from '../../common/link';
import { callActionFetchTermsAndConditions, callActionFetchPrivacyPolicy } from '../../../actions/asynch';

class TermsInfo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      target: null,
      show: false
    };
  }

  handleShowPopover(event) {
    var target = null;
    var show = null;

    if (event.target.id == 'popover-close') {
      target = this.state.target;
      show = false;
    } else {
      target = event.target;
      show = this.state.target !== target ? true : !this.state.show;
    }

    if (show) {
      let popoverContainer = document.querySelector('.popover-content-body');
      if (popoverContainer) {
        this.loadingTimeout = setTimeout(() => {
          render(<Loading />, popoverContainer);
        }, 300);
      }

      switch (target.id) {
        case 'popover-terms':
          this.props.callActionFetchTermsAndConditions()
            .then(response => {
              this.updateContent(show, target, response);
            });
          break;

        case 'popover-privacy':
          this.props.callActionFetchPrivacyPolicy()
            .then(response => {
              this.updateContent(show, target, response);
            });
          break;
      }
    }
    else {
      this.setState({
        target: target,
        show: show
      });
    }
  }

  updateContent(show, target, response) {
    clearTimeout(this.loadingTimeout);

    this.setState({
      target: target,
      show: show
    });

    document.querySelector('.popover-content-body').innerHTML = response;
  }

  componentDidMount() {
    this.context.localeManager.translateContainer('.terms-info-container');

    let element1 = document.querySelector('.terms-info-container [data-link="terms and conditions"]');
    render(<Link id="popover-terms" text={element1.innerHTML} onClick={this.handleShowPopover.bind(this)} />, element1);

    let element2 = document.querySelector('.terms-info-container [data-link="privacy policy"]');
    render(<Link id="popover-privacy" text={element2.innerHTML} onClick={this.handleShowPopover.bind(this)} />, element2);
  }

  render() {

    return (
      <div className="terms-info-container">

        <div className="legal-info-container" data-translate="legal text links"></div>

        <Overlay
          show={this.state.show}
          target={this.state.target}
          placement="bottom"
          container={this}
        >
          <Popover id="popover-container">

            <div id="popover-close" className="popover-close icon-close" onClick={this.handleShowPopover.bind(this)}></div>

            <div className="popover-content-body">
              <Loading />
            </div>

          </Popover>
        </Overlay>

      </div>
    );

  }
}

TermsInfo.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  return {}
}

export default connect(
  mapStateToProps,
  { callActionFetchTermsAndConditions, callActionFetchPrivacyPolicy }
)(TermsInfo);
