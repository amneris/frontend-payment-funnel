import React from 'react';
import {Component, PropTypes} from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { getHelpDeskLinkWithEmail } from '../../../reducers/utils';
import Link from '../../common/link';

class CancellationInfo extends Component {

  componentDidMount() {
    this.context.localeManager.translateContainer('.cancellation-info-container');

    let element = document.querySelector('.cancellation-info-container [data-link="help desk name"]');
    render(<Link text={element.innerHTML} href={getHelpDeskLinkWithEmail(this.props.language,this.props.user.email)} />, element);
  }

  render() {

    return (
      <div className="cancellation-info-container">
        <div data-translate="help desk text"></div>
      </div>
    );
  }
}

CancellationInfo.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  const { language } = state.appState;
  const { name, email } = state.userSession;

  return {
    language: language,
    user: {
      name: name || undefined,
      email: email || undefined
    }
  };
}

export default connect(
  mapStateToProps
)(CancellationInfo);
