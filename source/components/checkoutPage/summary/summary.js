import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import 'date-utils';
import { priceWithCurrency } from '../../../reducers/utils';


class Summary extends Component {
  /*
    Props from parent:
    - plan
    - renewalDate
   */
  constructor(props) {
    super(props);
  }



  setRenewalDate(date) {
    if (date) {
      return new Date(date);
    }
    else {
      return Date.today().add({months: this.props.plan.period});
    }
  }

  formatRenewalDate(date) {
    return date.toFormat('DD-MM-YYYY');
  }

  render() {
    let promo = this.props.plan.prices.promo || undefined;
    let originalPrice = this.props.plan.prices.base;
    let renewalDate = this.formatRenewalDate(this.setRenewalDate(this.props.renewalDate));

    return (

        <div className="aba-checkout-summary">
          <div className="section-title">{this.context.localeManager.getMessage('title summary')}</div>

          <div className="hidden-xs">
            <ul>
              <li>
                <div>
                  <span className="summary-label">{this.context.localeManager.getMessage('label student name')}</span>
                  <span className="summary-info" data-user-name={this.props.user.name}>{this.props.user.name}</span>
                </div>
              </li>
              <li>
                <div className="summary-section-row">
                  <span className="summary-label">{this.context.localeManager.getMessage('label student email')}</span>
                  <span className="summary-info" data-user-email={this.props.user.email}>{this.props.user.email}</span>
                </div>
              </li>
            </ul>

            <hr/>
          </div>

          <ul>
            <li>
              <span className="summary-label">{this.context.localeManager.getMessage('title subscription')}</span>
              <span className="summary-info-big">{this.context.localeManager.getMessage(this.props.plan.titleKey)}</span>
              <span className="summary-info-addition" data-period={this.props.plan.period}>({this.props.plan.period} {this.context.localeManager.getMessage('label months')})</span>
            </li>
            <li>
              {(() => {
                switch (this.props.selectedMethod) {
                  default:


                    return (
                      <div className="summary-section-row">
                        <span className="summary-label">{this.context.localeManager.getMessage('label renewal date')}</span>
                        <span className="summary-info" data-renewal-date={renewalDate}>{renewalDate}</span>
                      </div>
                    );
                }
              })()}
            </li>
          </ul>

          <div className="summary-price">

            <ul>
              <li>
                <span className="summary-price-label">{this.context.localeManager.getMessage('title price')}</span>
                {(() => {
                  if (promo) {
                    return (
                      <div>
                        <span className="summary-price-old">{priceWithCurrency(originalPrice, this.props.currency)}</span>
                        <span className="summary-info-big" data-price-total={promo.discountBase}>{priceWithCurrency(promo.discountBase, this.props.currency)}</span>
                        <span className="summary-info-addition">/ Saving: {priceWithCurrency(originalPrice - promo.discountBase, this.props.currency)}</span>
                      </div>
                    );
                  } else {
                    return (
                      <div>
                        <span className="summary-info-big" data-price-total={originalPrice}>{priceWithCurrency(originalPrice, this.props.currency)}</span>
                      </div>
                    );
                  }
                })()}
              </li>
            </ul>

          </div>

        </div>
    );
  }
}

Summary.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  const { name, email, currency } = state.userSession;
  const { selectedMethod } = state.checkout;

  return {
    user: {
      name: name || undefined,
      email: email || undefined
    },
    currency: currency,
    selectedMethod: selectedMethod
  };
}

export default connect(
  mapStateToProps
)(Summary);
