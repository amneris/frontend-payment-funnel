import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

import AppConfig from '../../../config/appConfig';
import Loading from '../../states/loading';
import Error from '../../states/error';
import TermsInfo from '../termsInfo/termsInfo';
import { callActionFetchZuoraSignature } from '../../../actions/asynch';
import { callActionResetCredentials } from '../../../actions/synch';
import { callActionTrackStartPaymentProcess } from '../../../actions/synch';

const zuoraContainerId = 'zuora_payment';

class HostedPage extends Component {

  constructor(props) {
    super(props);

    this.hostedPageId = props.id;
    this.onSuccess = props.onSuccess;

    this.state = {
      status: undefined
    };
  }

  resetState() {
    this.setState({
      status: undefined
    });
  }

  componentDidMount() {
    this.getZuoraAuthentication();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.credentials) {
      if (prevProps.credentials !== this.props.credentials || prevState.status === 'error') {
        if (this.props.credentials.pageId === this.hostedPageId) {
          this.renderZuoraContainer(this.props.credentials);
        }
      }
    }
  }

  getZuoraAuthentication() {
    if (!this.props.isFetching) {
      this.props.callActionFetchZuoraSignature(this.hostedPageId);
    }
  }

  componentWillUnmount() {
    this.props.callActionResetCredentials();
  }

  renderZuoraContainer(credentials) {

    if (this.zuoraContainerBeforeRender()) {
      Z.renderWithErrorHandler(
        {
          url: AppConfig.getZuoraApiUrl() + '/apps/PublicHostedPageLite.do',
          tenantId: credentials.tenantId,
          id: credentials.pageId,
          token: credentials.token,
          signature: credentials.signature,
          style: 'inline',
          key: credentials.key,
          submitEnabled: 'true',
          locale: this.props.language
        },
        {},
        this.hostedPageCallback.bind(this),
        this.hostedPagesErrorHandler.bind(this)
      );

      this.zuoraContainerAfterRender();
    }
  }

  resetZuoraContainer() {
    clearTimeout(this.renderTimeout);

    let zuoraContainer = document.getElementById(zuoraContainerId);

    if (zuoraContainer) {
      zuoraContainer.removeAttribute('id');
      zuoraContainer.removeAttribute('class');
      zuoraContainer.innerHTML = '';
    }
  }

  zuoraContainerBeforeRender() {
    let newZuoraContainer = document.getElementsByName(`methodContainer:${this.hostedPageId}`)[0];

    if (newZuoraContainer) {
      newZuoraContainer.setAttribute('id', zuoraContainerId);
    }

    return newZuoraContainer;
  }

  zuoraContainerAfterRender() {
    let zuoraContainer = document.getElementById(zuoraContainerId);

    if (zuoraContainer) {
      let iframe = zuoraContainer.getElementsByTagName('iframe')[0];

      if (iframe) {
        iframe.onload = () => {
          this.renderTimeout = setTimeout(() => {
            this.setState({ status: 'ok' });
            // this class shows the iFrame in the proper position
            if (zuoraContainer.id = zuoraContainerId) {
              zuoraContainer.className = 'iframe-container-rendered';
            }
          }, 200);
        }
      }
    }
  }

  hostedPageCallback(response) {
    // this callback is called after submitting the form
    //console.log('Subscription response', response);

    this.props.callActionTrackStartPaymentProcess();

    this.resetZuoraContainer();

    if (response.success === 'true') {
      this.resetState();
      this.handleSuccess(response.refId);
    } else {
       this.setState({
        status: 'error'
      });
    }
  }

  handleSuccess(zuoraCallbackId) {
    this.onSuccess(zuoraCallbackId);
  }

  hostedPagesErrorHandler(key, code, message) {

    if (key === 'error') {
      console.error(key, code, message);
      message = this.context.localeManager.getMessage('payment gateway error');
    }

    // Zuora docs: https://knowledgecenter.zuora.com/CA_Commerce/T_Hosted_Commerce_Pages/B_Payment_Pages_2.0/N_Error_Handling_for_Payment_Pages_2.0/Customize_Error_Messages_for_Payment_Pages_2.0#Invoke_the_Alternate_Render_Function_for_Custom_Error_Handling
    Z.sendErrorMessageToHpm(key, message);
  }

  handleError() {
    this.resetState();
    this.getZuoraAuthentication();
  }

  render() {

    return (
      <div>
        <div name={'methodContainer:' + this.hostedPageId}></div>

        {(() => {
          switch (this.state.status) {
            case 'ok':
              return <TermsInfo />;

            case 'error':
              return <Error retryFunction={this.handleError.bind(this)} />;

            default:
              return <Loading />;
          }
        })()}
      </div>
    );
  }
}

HostedPage.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  const { language } = state.appState;
  const { checkout } = state;

  return {
    language: language,
    credentials: checkout.credentials.data,
    isFetching: checkout.credentials.isFetching
  };
}

export default connect(
  mapStateToProps,
  {
    callActionFetchZuoraSignature, callActionResetCredentials, callActionTrackStartPaymentProcess
  }
)(HostedPage);
