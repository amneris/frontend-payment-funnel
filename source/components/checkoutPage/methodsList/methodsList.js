import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import Scroll  from 'react-scroll';
import Panel from 'react-bootstrap/lib/Panel';

import Loading from '../../states/loading';
import Error from '../../states/error';
import HostedPage from './hostedPage';
import Paypal from './paypal';
import { callActionFetchMethodsIfNeeded } from '../../../actions/asynch';
import { callActionSelectMethod, callActionGoToConfirmation } from '../../../actions/synch';

import CancellationInfo from '../cancellationInfo/cancellationInfo';

class MethodsList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      status: undefined,
      blocked: false
    };
  }

  resetState() {
    this.setState({
      status: undefined,
      blocked: false
    });
  }

  componentWillMount() {
    if (this.props.methods)  {
      this.setState({
        status: 'ok'
      })
    }

    this.props.callActionFetchMethodsIfNeeded();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.methods && nextProps.methods) {
      this.setState({
        status: 'ok'
      });
    }
  }

  componentDidMount() {
    this.handleSelectMethod();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.methods && this.props.methods) {
      this.handleSelectMethod();
    }

    if (prevProps.selectedMethod !== this.props.selectedMethod) {
      Scroll.scroller.scrollTo('method_' + this.props.selectedMethod + '_container', {
        duration: 1500,
        delay: 100,
        offset: -15,
        smooth: true
      });
    }
  }

  componentWillUnmount() {
    this.resetState();
  }

  handleSelectMethod() {
    // set default selected method
    //TODO get from same place css rule get value from
    let screenMaxSizeMobile = 767;

    if (this.props.methods && this.props.windowWidth > screenMaxSizeMobile) {
      let selectedMethod = this.props.selectedMethod
        || parseInt(this.props.urlParams.method)
        || this.props.methods[0].methodId;

      this.selectMethod(selectedMethod);
    }
  }

  selectMethod(methodId) {
    if (methodId !== this.props.selectedMethod) {
      this.props.callActionSelectMethod(methodId);
    }
  }

  onClickSelectMethod(key) {
    if (!this.state.blocked) {
      this.selectMethod(key);
    }
  }

  resetMethod(methodId) {
    this.setState({
      status: 'ok'
    });

    this.props.callActionSelectMethod(methodId);
  }

  goToConfirmationPage(zuoraRefId) {
    let selectedMethod = this.props.methods.find(
      selectedMethod => selectedMethod.methodId === this.props.selectedMethod
    );

    this.props.callActionGoToConfirmation(zuoraRefId, selectedMethod.gatewayName, selectedMethod.methodName);
  }

  render() {

    return (
      <div className="aba-checkout-methodsList col-sm-7">
        {(() => {
          if (this.props.isFetching) {
            return <Loading />;
          }
          else if (this.props.methods) {

            var methodsList = this.props.methods.map(method => {

              let expandPage = method.methodId === this.props.selectedMethod;

              let bullet = expandPage ?
                <div className="payment-method-toggler icon-check"/> : <div className="payment-method-toggler"/>;

              return (
                <div className="payment-method-container"
                     id={'method_' + method.methodId + '_container'}
                     key={'method_' + method.methodId + '_container'}>

                  <Panel key={method.pageId + '_header'}
                         onClick={ this.onClickSelectMethod.bind(this, method.methodId)}
                         className={`payment-method-header-container selected-${expandPage} blocked-${this.state.blocked}`}>

                    <div className="payment-method-header">
                      <div className="payment-method-toggler-container">{bullet}</div>

                      <div className="payment-method-info">
                        <div className="payment-method-info-header">
                          <div className="payment-method-title">
                            {this.context.localeManager.getMessage('title ' + method.methodName)}
                          </div>

                          <div className="payment-method-description hidden-xs hidden-sm">
                            {this.context.localeManager.getMessage('description ' + method.methodName, undefined, "")}
                          </div>
                        </div>
                      </div>

                    </div>

                  </Panel>
                  <div className={`panel-show-${expandPage}`}>
                    <Panel key={'method_' + method.methodId} eventKey={'method_' + method.methodId} collapsible
                           expanded={expandPage} className="panel-method">

                      {(() => {
                        if (expandPage) {
                          switch (this.state.status) {
                            case 'error':
                              return <Error retryFunction={this.resetMethod.bind(this, method.methodId)}/>

                            case 'ok':
                              switch (method.providerName) {
                                case 'Zuora':
                                  return (
                                    <HostedPage id={method.pageId} onSuccess={this.goToConfirmationPage.bind(this)}/>
                                  );
                                  break;

                                case 'PayPal':
                                  return <Paypal />
                                  break;
                              }

                              break;

                            case 'waiting':

                              //todo remove case

                              break;

                            default:
                              return <Loading />
                          }
                        }
                      })()}

                    </Panel>
                  </div>
                </div>
              );
            });

            return (
              <div>
                <div className="section-title">{this.context.localeManager.getMessage('title payment method')}</div>
                { methodsList }
                <CancellationInfo />
              </div>
            );
          }
          else {
            return <Error retryFunction={this.props.callActionFetchMethodsIfNeeded}/>;
          }
        })()}
      </div>
    );
  }
}

MethodsList.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  const { checkout } = state;
  const { transaction } = state;
  const { urlParams } = state.appState;

  return {
    methods: checkout.methods.items,
    isFetching: checkout.methods.isFetching,
    selectedMethod: checkout.selectedMethod,
    windowWidth: window.innerWidth,
    transaction,
    urlParams
  };
}

export default connect(
  mapStateToProps,
  {
    callActionSelectMethod,
    callActionFetchMethodsIfNeeded,
    callActionGoToConfirmation
  }
)(MethodsList);
