import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

import Loading from '../../states/loading';
import Error from '../../states/error';
import AppConfig from '../../../config/appConfig';
import { findPlanById, getConfirmationPageUrl, appendParamsToUrl } from '../../../actions/utils';
import { callActionStartPaypalTransaction } from '../../../actions/asynch';
import { callActionTrackStartPaymentProcess } from '../../../actions/synch';
import TermsInfo from '../termsInfo/termsInfo';

export const ITEM_NAME_PREFIX = 'ABA English ';
export const ITEM_NAME_SUFFIX = ' subscription';

class Paypal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      status: undefined
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.transaction.isFetching && !this.props.transaction.isFetching) {
      if (this.props.transaction.paypalToken) {
        this.goToPaypal(this.props.transaction.paypalToken);
      }
      else {
        this.setState({
          status: 'error'
        });
      }
    }
  }

  startTransaction() {

    this.props.callActionTrackStartPaymentProcess();

    if (this.state.status !== 'loading') {

      if (this.props.urlParams.hasOwnProperty('token')) {
        this.goToPaypal(this.props.urlParams['token']);
      }
      else {
        this.props.callActionStartPaypalTransaction(
          this.getSuccessUrl(),
          this.getCancelUrl(),
          this.props.userEmail
        );
      }

      this.setState({
        status: 'loading'
      });
    }
  }

  getSuccessUrl() {
    let urlReturn = getConfirmationPageUrl();

    let returnParams = this.props.urlParams;
    returnParams.method = this.props.selectedMethod.methodId;
    returnParams.methodName = encodeURIComponent(this.props.selectedMethod.methodName);
    returnParams.gatewayName = encodeURIComponent(this.props.selectedMethod.gatewayName);
    if (this.props.shoppingCart.discount) returnParams.discount = encodeURIComponent(this.props.shoppingCart.discount);
    returnParams.period = encodeURIComponent(this.props.shoppingCart.period);
    returnParams.planId = encodeURIComponent(this.props.shoppingCart.plan);
    returnParams.planTitleKey = encodeURIComponent(this.props.shoppingCart.planTitleKey);

    return appendParamsToUrl(urlReturn, returnParams);
  }

  getCancelUrl() {
    return window.location.href;
  }

  goToPaypal(token) {
    let url = AppConfig.getPaypalActionUrl();

    window.location.href = appendParamsToUrl(url, {
        cmd: '_express-checkout',
        token: token
      }
    );
  }

  render() {
    return (
      <div className="aba-paypal-container">
        <div className="paypal-container-inner">
          {(() => {
            switch (this.state.status) {

              case 'loading':

                return (
                  <div>
                    <div className={'paypal-redirect-container'}>
                      <span className="paypal-redirect-text">{this.context.localeManager.getMessage('text paypal redirect')}</span>
                      <Loading scaleFactor={0.12} />
                    </div>
                  </div>
              );

              case 'error':
                return <Error retryFunction={this.startTransaction.bind(this)}/>

              default:

                return (
                  <div>
                    <div className={'paypal-button ' + this.state.status} onClick={this.startTransaction.bind(this)}>
                      <div className="sp-logo sp-logo__paypal"></div>
                    </div>
                    <TermsInfo />
                  </div>
                );
            }
          })()}
        </div>
      </div>
    );
  }
}

Paypal.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  const { urlParams, shoppingCart } = state.appState;
  const { id, language,  email } = state.userSession;
  const { currency } = state.plans;
  const { methods, selectedMethod } = state.checkout;
  const { transaction } = state;

  let selectedPlanItem;
  if (shoppingCart && shoppingCart.plan) {
      selectedPlanItem = findPlanById(shoppingCart.plan, state.plans.items);
  }

  let selectedMethodItem = methods.items.find(
    method => method.methodId === selectedMethod
  );

  return {
    urlParams: urlParams,
    userId: id,
    userEmail : email,
    userLanguage: language,
    currency: currency,
    selectedMethod: selectedMethodItem,
    selectedPlan: selectedPlanItem || undefined,
    transaction,
    shoppingCart: shoppingCart
  };
}

export default connect(
  mapStateToProps,
  { callActionStartPaypalTransaction, callActionTrackStartPaymentProcess }
)(Paypal);
