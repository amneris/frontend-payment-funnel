import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

class SupportInfo extends Component {

  render() {

    //TODO do we want to link to help centre? & get info from backend
    return (
      <div className="support-info-container col-sm-5">
        <div className="section-title">{this.context.localeManager.getMessage('title doubts')}</div>
        <p className="support-opening-hours-info">{this.context.localeManager.getMessage('label helpcenter hours')}</p>
        <ul className="contact-info">
          <li className="contact-info-element">
            <div className="icon icon-telephone"/>
            <div>{this.props.phoneNumber}</div>
          </li>
          <li className="contact-info-element">
            <div className="icon icon-envelope"/>
            <div>{this.props.emailContact}</div>
          </li>
        </ul>
      </div>
    );
  }
}

SupportInfo.contextTypes = {
  localeManager: PropTypes.object
}

SupportInfo.propTypes = {
  phoneNumber: PropTypes.string.isRequired,
  emailContact: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {}
}

export default connect(
  mapStateToProps
)(SupportInfo);
