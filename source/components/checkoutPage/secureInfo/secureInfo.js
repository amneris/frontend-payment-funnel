import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

class SecureInfo extends Component {

  render() {
    return (
      <div className="secure-info-container">
        <p>{this.context.localeManager.getMessage('text transaction info')}</p>
        <div className="secure-icons-list">
          <div className="secure-logo">
            <div className="secure-logo-container">
              <div className="sp-logo sp-logo__comodo_secure"></div>
            </div>
          </div>
          <div className="secure-logo">
            <div className="secure-logo-container">
              <div className="sp-logo sp-logo__paypal_secure"></div>
            </div>
          </div>
          <div className="secure-logo">
            <div className="secure-logo-container">
              <div className="sp-logo sp-logo__payment_secure"></div>
              <div className="secure-logo-text">{this.context.localeManager.getMessage('label payment secure')}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SecureInfo.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  return {}
}

export default connect(
  mapStateToProps
)(SecureInfo);
