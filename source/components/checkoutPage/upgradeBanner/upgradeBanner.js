import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

class UpgradeBanner extends Component {

  render() {
    return (
      <div className="upgrade-banner-container">
        <div className="banner-header">Upgrade Now</div>
        <div className="banner-info-container">
          <div className="banner-info">
            GET ONE YEAR EXTRA
          </div>
        </div>
      </div>
    );
  }
}

UpgradeBanner.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  return {}
}

export default connect(
  mapStateToProps
)(UpgradeBanner);
