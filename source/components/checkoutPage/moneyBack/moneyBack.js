import React from 'react';
import {Component, PropTypes} from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { GUARANTEE_DAYS, getHelpDeskLinkWithEmail } from '../../../reducers/utils';
import Link from '../../common/link';

class MoneyBack extends Component {

  componentDidMount() {
    this.context.localeManager.translateContainer('.money-back-container');

    let element = document.querySelector('.money-back-container [data-link="help desk name"]');
    render(<Link text={element.innerHTML} href={getHelpDeskLinkWithEmail(this.props.language,this.props.user.email)} />, element);
  }

  render() {
    return (
      <div className="money-back-container">
        <div className="section-title">{this.context.localeManager.getMessage('title money back')}</div>
        <div className="money-back-info" data-translate="text money back" data-days={GUARANTEE_DAYS}></div>
      </div>
    );
  }
}

MoneyBack.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  const { language } = state.appState;
  const { name, email } = state.userSession;

  return {
    language: language,
    user: {
      name: name || undefined,
      email: email || undefined
    }
  };
}

export default connect(
  mapStateToProps
)(MoneyBack);
