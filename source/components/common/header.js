import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { callActionExitFunnel } from '../../actions/synch';

class Header extends Component {

  goHome() {
    this.props.callActionExitFunnel();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.appState.title) {
      document.title = 'ABA English | ' + nextProps.appState.title;
    }
  }

  render() {

    return (
      <section className="aba-plans-header">
        <div className="header-logo">
          <div className="sp-logo sp-logo__abaenglish" onClick={this.goHome.bind(this)}></div>
        </div>
        <div className="header-title">
          {this.props.appState.title}

          {(() => {
            if (this.props.appState.subtitle) {
              return (
                <span className="header-subtitle">
                  {this.props.appState.subtitle}
                </span>
              );
            }
          })()}
        </div>
      </section>

    );
  }
}

//map state of app (located in redux store) to this component's properties
function mapStateToProps(state) {
  return {
    appState :
    state.appState || {
      title: null,
      subtitle: null
    }
  };
}

export default connect(
  mapStateToProps,
  { callActionExitFunnel }
)(Header);
