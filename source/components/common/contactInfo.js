import React from 'react';
import { PropTypes, Component } from 'react';

export default class ContactInfo extends Component {

  render() {
    return (<div className="footer-text">
      {'ABA english ' + this.props.phoneNumber} {this.props.contactHours}
    </div>)
  }
}

ContactInfo.propTypes = {
  phoneNumber: PropTypes.string.isRequired,
  contactHours: PropTypes.string.isRequired
};
