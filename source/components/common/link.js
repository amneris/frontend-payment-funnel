import React from 'react';
import { Component, PropTypes } from 'react';

export default class Link extends Component {
  constructor(props) {
    super(props);

    this.id = props.id;
    this.className = props.className || 'link';
    this.text = props.text;
    this.isExternal = props.hasOwnProperty('href');
    this.onClick = this.isExternal ? props.href : props.onClick;
  }

  render() {
    if (this.isExternal) {
      return (
        <a id={this.id} className={this.className} href={this.onClick} target="_blank">{this.text}</a>
      );
    }
    else {
      return (
        <span id={this.id} className={this.className} onClick={this.onClick}>{this.text}</span>
      );
    }
  }
}

Link.contextTypes = {
  localeManager: PropTypes.object
}
