import React from 'react';
import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getDefaultContactPhone } from '../../reducers/utils';
import ContactInfo from './contactInfo';

export default class Footer extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className="aba-plans-footer col-xs-12">
        <div className="footer-container">
          <div className="footer-icon">
            <div className="icon-telephone"></div>
          </div>
          <ContactInfo phoneNumber={getDefaultContactPhone()}
                       contactHours={this.context.localeManager.getMessage('label helpcenter hours')}/>
        </div>
      </section>
    );
  }
}

// define properties of app context that this component needs to access
Footer.contextTypes = {
  localeManager: PropTypes.object
};
