import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import Link from '../../common/link';
import {
  getSocialLinkedinLink, getSocialFacebookLink, getSocialTwitterLink,
  getSocialBlogLink, getSocialSoundcloudLink, getSocialYoutubeLink
} from '../../../reducers/utils';

class SocialNetwork extends Component {

  componentDidMount() {
    this.context.localeManager.translateContainer('.social-network-container');
  }

  render() {
    return (
      <div className="social-network-container">
        <div className="social-network-text" data-translate="social network links"></div>

        <div className="social-network-icons">
          <Link className="social-link" text={<div className="icon-rss"></div>} href={getSocialBlogLink(this.props.language)} />
          <Link className="social-link" text={<div className="icon-facebook"></div>} href={getSocialFacebookLink()} />
          <Link className="social-link" text={<div className="icon-linkedin2"></div>} href={getSocialLinkedinLink()} />
          <Link className="social-link" text={<div className="icon-twitter"></div>} href={getSocialTwitterLink()} />
          <Link className="social-link" text={<div className="icon-youtube"></div>} href={getSocialYoutubeLink()} />
          <Link className="social-link" text={<div className="icon-soundcloud"></div>} href={getSocialSoundcloudLink()} />
        </div>
      </div>
    );

  }
}

SocialNetwork.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {

  const { language } = state.appState;
  const { email } = state.userSession;

  return {
    language: language,
    userEmail: email
  };
}

export default connect(
  mapStateToProps
)(SocialNetwork);
