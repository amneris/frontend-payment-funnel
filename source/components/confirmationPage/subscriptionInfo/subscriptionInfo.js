import React from 'react';
import {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import Link from '../../common/link';
import { getHelpDeskLinkWithEmail } from '../../../reducers/utils';

class SubscriptionInfo extends Component {

  componentDidMount() {
    this.context.localeManager.translateContainer('.subscription-info-container');

    let element = document.querySelector('.subscription-info-container [data-link="help desk name"]');
    ReactDOM.render(<Link text={element.innerHTML} href={getHelpDeskLinkWithEmail(this.props.language, this.props.userEmail)} />, element);
  }

  render() {
    return (
      <div className="subscription-info-container">
        <div className="subscription-info-text" data-translate="subscription info text 1"></div>
        <div className="subscription-info-text" data-translate="subscription info text 2"></div>
        <div className="subscription-info-text" data-translate="subscription info text 3"></div>
        <div className="subscription-info-text" data-translate="subscription info text 4"></div>
      </div>
    );
  }
}

SubscriptionInfo.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {

  const { language } = state.appState;
  const { email } = state.userSession;

  return {
    language: language,
    userEmail: email
  };
}

export default connect(
  mapStateToProps
)(SubscriptionInfo);
