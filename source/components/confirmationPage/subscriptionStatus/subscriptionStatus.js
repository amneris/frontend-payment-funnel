import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

import Loading from '../../states/loading';
import { callActionGoToCourse } from '../../../actions/synch';

class SubscriptionStatus extends Component {

  render() {

    return (
      <div className="confirmation-user-status">
        <div className="user-status-container">

          {(() => {

            if (this.props.userIsPremium || this.props.subscriptionId) {

              return (
                <div>
                  <div className="success-icon icon-check2"></div>
                  <div className="user-status-text">{this.context.localeManager.getMessage('confirmation premium text')}</div>

                  <div className={'subscription-id-text has-subscription-id-' + Boolean(this.props.subscriptionId)}>
                    <span>{this.context.localeManager.getMessage('subscription id label')}</span>
                    <span className="subscription-id-value">{this.props.subscriptionId}</span>
                  </div>

                  <button className={'goToCampus-button user-premium-' + this.props.userIsPremium} type="button"
                          onClick={this.props.callActionGoToCourse}
                  >
                    {this.context.localeManager.getMessage('start course label')}
                  </button>

                </div>
              );

            } else if (!this.props.userIsPremium) {

              return (
                <div className="processing-payment-container">
                  <span className="processing-payment-text">{this.context.localeManager.getMessage('text processing payment')}</span>
                  <Loading scaleFactor={0.12} />
                </div>
              );
            }
            else {

              return <Loading />
            }

          })()}

        </div>
      </div>
    );
  }
}

SubscriptionStatus.contextTypes = {
  localeManager: PropTypes.object
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  { callActionGoToCourse }
)(SubscriptionStatus);
