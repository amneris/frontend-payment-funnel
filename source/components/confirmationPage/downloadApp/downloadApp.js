import React from 'react';
import {Component, PropTypes} from 'react';

import AppConfig from '../../../config/appConfig';
import Link from '../../common/link';

export default class DownloadApp extends Component {

  constructor(props) {
    super(props);

    this.isSticky = props.isSticky || false;
    this.alignCenter = props.alignCenter || false;
  }

  render() {
    let linkToITunes = (
      <div>
        <div className="sp-logo sp-logo__itunes"></div>
        <div className="app-button-text">
          <div className="app-button-store-text">{this.context.localeManager.getMessage('label get the app on')}</div>
          <div className="app-button-store-name">iTunes</div>
        </div>
      </div>
    );

    let linkToGooglePlay = (
      <div>
        <div className="sp-logo sp-logo__google_play"></div>
        <div className="app-button-text">
          <div className="app-button-store-text">{this.context.localeManager.getMessage('label get the app on')}</div>
          <div className="app-button-store-name">Google Play</div>
        </div>
      </div>
    );

    return (
      <div className={"download-app-container sticky-" + this.isSticky + ' align-center-' + this.alignCenter}>
        <div className="download-app-container-inner">
          <div className="download-app-info-container">

            <div className="download-app-info-container-inner">
              <div className="download-app-info col-xs-6 col-sm-offset-1 col-md-offset-0">

                <div className="download-app-title">{this.context.localeManager.getMessage('title learn english everywhere')}</div>

                <div className="download-app-buttons-container">
                  <div className="download-app-subtitle">{this.context.localeManager.getMessage('title download the app')}</div>

                  <Link text={linkToITunes} href={AppConfig.getITunesUrl()} className="download-app-button itunes" />
                  <Link text={linkToGooglePlay} href={AppConfig.getGooglePlayUrl()} className="download-app-button google-play"/>

                </div>

              </div>
            </div>

          </div>

          <div className="download-app-info-image col-xs-offset-6 col-xs-6 col-sm-5">
            <div className="sp-image sp-image__mobile_devices"></div>
          </div>

        </div>
      </div>
    );
  }
}

DownloadApp.contextTypes = {
  localeManager: PropTypes.object
}
