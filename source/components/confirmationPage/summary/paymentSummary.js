import React from 'react';
import {Component, PropTypes} from 'react';
import {render} from 'react-dom';
import Summary from '../../checkoutPage/summary/summary';
import DownloadApp from '../downloadApp/downloadApp';
import SubscriptionInfo from '../subscriptionInfo/subscriptionInfo';


export default class PaymentSummary extends Component {

  render() {

    return (
      <div className="payment-summary-and-info-container">
        <div className="payment-summary col-sm-6">
          <Summary plan={this.props.plan} renewalDate={this.props.renewalDate} />
        </div>

        {(() => {
          if (!this.props.isMobileDevice) {
            return (
              <div className="col-md-5">
                <DownloadApp isSticky={false}/>
              </div>
            );
          }
        })()}

        <div className="col-sm-6 col-md-12">
          <SubscriptionInfo />
        </div>
      </div>
    );
  }
}
