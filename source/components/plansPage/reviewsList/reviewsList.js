import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import Review from './review';
import dataSources from '../../../dataSources/plansPage/data';
import Carousel from 'react-bootstrap/lib/Carousel';
import CarouselItem from 'react-bootstrap/lib/CarouselItem';
import { callActionSwipeReviewCarousel, callActionSelectReviewCarousel } from '../../../actions/synch';


class ReviewsList extends Component {

  componentWillMount() {

    let numberOfReviews = dataSources['reviews'].length;

    this.reviews = [];
    for (var i = 0; i < dataSources['reviews'].length; i++) {
      var image = dataSources['reviews'][i].image;
      var text = this.context.localeManager.getMessage(dataSources['reviews'][i].text);
      var reviewer = this.context.localeManager.getMessage(dataSources['reviews'][i].name);
      this.reviews.push(<Review image={image} text={text} reviewer={reviewer} numberOfReviews={numberOfReviews} />);
    }
  }

  carouselSwipe(direction){

    let numberOfReviews = this.reviews.length;
    this.props.callActionSwipeReviewCarousel(direction, numberOfReviews)
  }

  render() {
    var reviews = this.reviews;

    return (
      <section className="aba-plans-reviews sp-image">

        <div
          className="section-title title-testimonies">{this.context.localeManager.getMessage('title testimonies')}</div>
        <div className="aba-plans-reviews-inner">

          <div className="pagination outer left">
            <div className="line-arrow left" onClick={this.carouselSwipe.bind(this, 'prev')}></div>
          </div>

          <div className="carousel-and-pagination-container">

            <Carousel className="carousel slide"
                      controls={false}
                      activeIndex={this.props.activeReviewIndex}
                      onSelect={this.props.callActionSelectReviewCarousel}>
              {(() => {
                var output = [];
                reviews.forEach((review, i) => {
                  output.push(
                    <CarouselItem key={'review:' + i} active={i == this.props.activeReviewIndex}>
                      <div className="">
                        {review}
                      </div>
                    </CarouselItem>
                  );
                });

                return (output);
              })()}
            </Carousel>

          </div>

          <div className="pagination outer right">
            <div className="line-arrow right" onClick={this.carouselSwipe.bind(this, 'next')}></div>
          </div>
        </div>
      </section>
    );
  }
}

ReviewsList.propTypes = {
  reviews: PropTypes.arrayOf(Review)
}

ReviewsList.contextTypes = {
  localeManager: PropTypes.object
}


function mapStateToProps(state) {

  return {
    direction: state.review.direction,
    activeReviewIndex: state.review.activeIndex
  };
}

export default connect(
  mapStateToProps,
  { callActionSwipeReviewCarousel, callActionSelectReviewCarousel }
)(ReviewsList);
