import React from 'react';
import { Component, PropTypes}  from 'react';
import { connect } from 'react-redux';
import { callActionSwipeReviewCarousel } from '../../../actions/synch';

class Review extends Component {

  componentWillMount() {
    this.image = this.props.image;
    this.text = this.props.text;
    this.reviewer = this.props.reviewer;
  }


  carouselSwipe(direction){

    let numberOfReviews = this.props.numberOfReviews;
    this.props.callActionSwipeReviewCarousel(direction, numberOfReviews)
  }

  render() {
    return (
      <div className="review-container-inner">
        <div className="review-container-header">
          <div className="pagination inner left">

            <div className="line-arrow left" onClick={this.carouselSwipe.bind(this, 'prev')}></div>
          </div>
          <div className="review-image">
            <div className={this.image}></div>
          </div>
          <div className="pagination inner right">

            <div className="line-arrow right" onClick={this.carouselSwipe.bind(this, 'next')}></div>
          </div>
        </div>
        <div className="review-content">
          <div className="review-content-text">"{this.text}"</div>
          <div className="review-content-name">{this.reviewer}</div>
        </div>
      </div>
    );
  }
}

Review.propTypes = {
  image: PropTypes.string,
  text: PropTypes.string,
  reviewer: PropTypes.string
}

function mapStateToProps(state) {

  return {
    direction: state.review.direction,
    activeReviewIndex: state.review.activeIndex
  };
}

export default connect(
  mapStateToProps,
  { callActionSwipeReviewCarousel }
)(Review);
