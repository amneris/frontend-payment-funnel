import React from 'react';
import {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import Price from './price';
import Promo from './promo';
import PreviousPrice from './previousPrice';
import { callActionGoToCheckout, callActionClickSelectPlan } from '../../../actions/synch';
import { priceToString, priceWithCurrency, GUARANTEE_DAYS } from '../../../reducers/utils';

class Plan extends Component {

  componentWillMount() {
    this.plan = this.props.data;
    this.promo = this.plan.prices.hasOwnProperty('promo') ? this.plan.prices.promo : null;
    this.currency = this.props.currency;

    this.trending = this.plan.featured;

    this.state = {
      isSelectedPlan: false,
      containerMouseState: ''
    };
  }


  handleSelectedPlan() {
    this.props.callActionClickSelectPlan(this.plan);
    this.props.callActionGoToCheckout(this.plan);
  }

  render() {

    var containerClasses = 'plan-container';

    if (this.trending) containerClasses += ' trending';


    let trendingElement = this.trending ? <div
      className="plan-trending-label">{this.context.localeManager.getMessage('label plan trending')}</div> : null;

    let promoDetailsElement, previousPriceElement, monthlyPrice, totalPrice = null;

    if (this.promo) {
      promoDetailsElement = <Promo data={this.promo}/>
      let previousPriceTotal = priceWithCurrency(this.plan.prices.monthlyBase, this.currency);
      previousPriceElement = <PreviousPrice previousPriceTotal={previousPriceTotal}/>;
      monthlyPrice = this.promo.discountMonthly;
      totalPrice = this.plan.prices.promo.discountBase;
    } else {
      monthlyPrice = this.plan.prices.monthlyBase;
      totalPrice = this.plan.prices.base;
    }
    let formattedMonthlyPrice = priceToString(monthlyPrice, this.currency.decimalSeparator);
    let formattedTotalPrice = priceWithCurrency(totalPrice, this.currency);

    let priceElement = <Price value={monthlyPrice} strValue={formattedMonthlyPrice} currency={this.currency}/>;

    return (
      <div id={'idLi' + this.plan.titleKey} className={containerClasses + ' ' + this.state.containerMouseState}>
        {trendingElement}
        <div className="plan-container-inner">
          {promoDetailsElement}
          <div className="plan-title">
            {this.context.localeManager.getMessage(this.plan.titleKey)}
          </div>
          <div className="plan-price">
            {previousPriceElement}
            <div className="plan-price-current">
              {priceElement}
            </div>
            {(() => {
              if (this.plan.period > 1) {
                return (
                  <div className="plan-price-total" data-price-total={totalPrice}>
                    <span>{this.context.localeManager.getMessage('label for just', {price: formattedTotalPrice})}</span>
                  </div>
                );
              }
            })()}
          </div>
          <div className="plan-select">
            <div className="plan-select-button"
                    onClick={this.handleSelectedPlan.bind(this)}
            >
              <span className="hidden-xs">{this.context.localeManager.getMessage('label select')}</span>
              <span className="visible-xs icon-chevron-right"></span>
            </div>
            <div className="plan-select-warranty">
              {this.context.localeManager.getMessage('title guarantee', { days: GUARANTEE_DAYS })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Plan.propTypes = {
  id: PropTypes.number,
  key: PropTypes.string,
  period: PropTypes.object,
  name: PropTypes.string,
  price: PropTypes.object,
  currency: PropTypes.object,
  trending: PropTypes.bool
};

Plan.contextTypes = {
  localeManager: PropTypes.object,
  queryParams: PropTypes.object
};

function mapStateToProps(state) {
  const { segmentId } = state.appState;

  return {
    segmentId: segmentId
  };
}

export default connect(
  mapStateToProps,
  { callActionGoToCheckout, callActionClickSelectPlan }
)(Plan);
