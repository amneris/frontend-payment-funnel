import React from 'react';
import {Component, PropTypes} from 'react';

export default class PreviousPrice extends Component{


  render() {

    let previousPriceTotal = this.props.previousPriceTotal;

    return (
      <div className="plan-price-old">
        <span>&nbsp;{previousPriceTotal}&nbsp;</span>
      </div>
    );
  }
}

PreviousPrice.propTypes = {
  previousPriceTotal: PropTypes.string
};

PreviousPrice.contextTypes = {
  localeManager: PropTypes.object
};
