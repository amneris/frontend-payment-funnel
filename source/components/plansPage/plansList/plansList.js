import React from 'react';
import {Component, PropTypes} from 'react';
import Scroll  from 'react-scroll';
import Plan from './plan';

export default class PlansList extends Component {

  constructor(props) {
    super(props);

    if (props.hasOwnProperty('data')) this.handleFetchedPlans(props.data);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.data && nextProps.data) {
      this.handleFetchedPlans(nextProps.data);
    }
  }

  componentDidMount() {
    if (this.props.hasOwnProperty('scrollTo')) {
      this.scrollToElement(this.props.scrollTo);
    }
  }

  scrollToElement(id) {

    if (window && window.innerWidth < 992 && document.getElementById(id)) {
      Scroll.scroller.scrollTo(id, {
        duration: 1500,
        delay: 100,
        offset: -15,
        smooth: true
      });
    }

  }

  handleFetchedPlans(availablePlans) {

    if (availablePlans.items && availablePlans.currency) {

      let currency = availablePlans.currency;

      let self = this;

      var planComponents = availablePlans.items.filter(::this.isPlanToBeDisplayed).map(plan => {

        return <Plan data={plan} currency={currency}/>;

      });

      this.planComponents = planComponents;
      this.currency = currency;
    }
  }

  /**
   * we can hide plans by passing a comma-separated string of plan periods to hide is a queryString name 'hp'.
   * also, family plan is hidden
   * @param plan
   * @returns {boolean}
   */
  isPlanToBeDisplayed(plan) {

      if (plan.familyPlan)
        return false;

      let plansToHide = []

      if (this.props.urlParams && this.props.urlParams.hp) {
        plansToHide = this.props.urlParams.hp.split(',');
        return !plansToHide.includes(plan.period.toString())
      }

      return true;
  }

  render() {

    return (
      <div className="aba-plans-list-wrapper">
        {(() => {
          var output = [];


          if (this.planComponents && this.planComponents.length > 0) {
            var maxInList = 4;
            var numLists = Math.ceil(this.planComponents.length / maxInList);
            var planIndex = 0;

            for (var i = 0; i < numLists; i++) {
              var listElements = [];

              for (var j = planIndex; j - planIndex < maxInList && j < this.planComponents.length; j++) {

                listElements.push(
                  <li key={'plan' + j} className="plan-list-element" data-plan-id={'plansList' + i + ':plan' + j}>
                    {this.planComponents[j]}
                  </li>
                );
              }

              planIndex = j;

              output.push(
                <ul key={'plansList' + i} className="aba-plans-list-container">
                  {listElements}
                </ul>
              );
            }
          }

          return output;
        })()}
      </div>
    );
  }
}

PlansList.propTypes = {
  planComponents: PropTypes.arrayOf(Plan),
  currency: PropTypes.object
}

PlansList.contextTypes = {
  localeManager: PropTypes.object
}
