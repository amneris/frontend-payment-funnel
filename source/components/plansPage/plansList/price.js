import React from 'react';
import {Component, PropTypes} from 'react';

export default class Price extends Component{

  componentWillMount() {
    var strPrice = this.props.strValue;
    var currency = this.props.currency;
    var priceParts = strPrice.split(currency.decimalSeparator);

    this.strInt = priceParts[0];

    this.strCents = currency.decimalSeparator + priceParts[1];
    if (!currency.symbolShowBeforePrice) {
      this.strCents = this.strCents + currency.symbol;
    }

    this.value = this.props.value;

    this.currency = currency;
  }

  render() {


    return (
      <dl data-price-monthly={this.value}>
        <dt className="plan-price-current-element">
          {(() => {
            if (this.currency.symbolShowBeforePrice) {
              return (
                <div>
                  <span className="plan-price-current-small">{this.currency.symbol}</span>
                  <span className="plan-price-current-big">{this.strInt}</span>
                </div>
              );
            }
            else {
              return (<span className="plan-price-current-big">{this.strInt}</span>);
            }
          })()}
        </dt>
        <dd className="plan-price-current-element">
          <span className="plan-price-current-small two-lines">{this.strCents}</span>
          <span className="plan-price-current-small two-lines">{this.context.localeManager.getMessage('label per month')}</span>
        </dd>
      </dl>
    );
  }
}

Price.propTypes = {
  strInt: PropTypes.string,
  strCents: PropTypes.string,
  currency: PropTypes.object
};

Price.contextTypes = {
  localeManager: PropTypes.object
};
