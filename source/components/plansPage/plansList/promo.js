import React from 'react';
import {Component, PropTypes} from 'react';

export default class Promo extends Component {

  componentWillMount() {
    var data = this.props.data;
    this.value = data.discount;
  }

  render() {


    return (
      <div className="plan-promo-container">
        {'-' + this.value + '%'}
      </div>
    );

  }
}

Promo.propTypes = {
  value: PropTypes.number
};

Promo.contextTypes = {
  localeManager: PropTypes.object
};
