import React from 'react';
import {Component, PropTypes} from 'react';
import Feature from './feature';
// todo move this import after AB test complete
import FeatureABTestVariationB from './ABTestVariant/feature-abtest-variation-b';
import dataSources from '../../../dataSources/plansPage/data';
import { GUARANTEE_DAYS } from '../../../reducers/utils';

export default class FeaturesList extends Component {

  getLocaleMessage(message) {
    // todo AB : refactor title and text to accept params dynamically

    return this.context.localeManager.getMessage(message, { days: GUARANTEE_DAYS });
  }

  componentWillMount() {

    this.features = dataSources['features'].map(feature => {

      let featureTitle = this.getLocaleMessage(feature.title);
      let featureText = this.getLocaleMessage(feature.text);
      return (<Feature title={featureTitle} text={featureText} icon={feature.icon}/>);
    });
  }

  //todo AB : move this method after AB test complete
  getFeatureListVariantB(){

    this.featuresVariantB = dataSources['features-abtest-variation-b'].map(featureVariantB => {

      let featureTitle = this.getLocaleMessage(featureVariantB.title);
      let featureText = this.getLocaleMessage(featureVariantB.text);
      return (<FeatureABTestVariationB title={featureTitle} text={featureText} icon={featureVariantB.icon}/>);
    });

    return <ul className="features-list features-list-variantB">
      {
        this.featuresVariantB.map(featureVariantB =>
          <li key={Math.random()} className="feature-container col-xs-12 col-sm-6 col-md-4">
            {featureVariantB}
          </li>)}
    </ul>
  }

  render() {
    return (
      <section className="aba-plans-features">
        <div className="section-title title-premium-advantages">{this.getLocaleMessage('title premium advantages')}</div>
        <ul className="features-list">
          {
            this.features.map(feature =>
              <li key={Math.random()} className="feature-container col-sm-6 col-xs-12">
                {feature}
              </li>)}
        </ul>


        {
          /* todo AB : move this line after AB test complete */
          this.getFeatureListVariantB()
        }

      </section>
    );
  }
}

FeaturesList.propTypes = {
  features: PropTypes.arrayOf(Feature)
}

FeaturesList.contextTypes = {
  localeManager: PropTypes.object
}
