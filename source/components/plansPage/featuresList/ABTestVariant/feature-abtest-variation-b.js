import React from 'react';
import {Component, PropTypes} from 'react';

export default class FeatureABTestVariationB extends Component {

  componentWillMount() {
    this.title = this.props.title;
    this.text = this.props.text;
    this.icon = this.props.icon;
  }

  render() {

    // title consists of two parts, title header & title content whereby first
    // word of title is the header, and the remaining text is the content
    // e.g. for the title 'Unlimited messages to your teacher'
    // title header = 'Unlimited', title contnet = 'messages to your teacher'
    let titleHeader = this.title.split(' ')[0];
    let titleContent = this.title.substring(this.title.indexOf(' '), this.title.length);

    return (
      <div className="feature-container-inner feature-container-inner-variantB">
        <dl>
          <div className={'nc-icon-outline nucleo-feature-icon ' + this.icon}></div>
          <dt className="feature-title"><span
            className="title-header">{titleHeader}</span>{titleContent}
          </dt>
          <dd className="feature-content">
            <div className="feature-text">{this.text}</div>
          </dd>
        </dl>
      </div>
    );
  }
}

FeatureABTestVariationB.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.string
}
