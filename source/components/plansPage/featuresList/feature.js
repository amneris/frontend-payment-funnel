import React from 'react';
import {Component, PropTypes} from 'react';

export default class Feature extends Component {

  componentWillMount() {
    this.title = this.props.title;
    this.text = this.props.text;
    this.icon = this.props.icon;
  }

  render() {
    return (
      /* todo AB : remove class feature-container-inner-variantA after AB test finished */
      <div className="feature-container-inner feature-container-inner-variantA">
        <dl>
          <dt className="feature-title">{this.title}</dt>
          <dd className="feature-content">
            <div className={'feature-icon ' + this.icon}></div>
            <div className="feature-text">{this.text}</div>
          </dd>
        </dl>
      </div>
    );
  }
}

Feature.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.string
}
