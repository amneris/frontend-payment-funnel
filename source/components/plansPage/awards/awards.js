import React from 'react';
import {Component} from 'react';

export default class Awards extends Component {

  render() {
    return (
      <section className="aba-plans-awards">
        <div className="awards-logo sp-logo sp-logo__prize_app"></div>
        <span>Best Educational App 2015</span>
      </section>
    );
  }
}
