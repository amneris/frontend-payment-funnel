import React from 'react';
import { Route, IndexRedirect, Redirect } from 'react-router';
import App from './containers/app';
import PlansPage from './containers/plansPage';
import CheckoutPage from './containers/checkoutPage';
import ConfirmationPage from './containers/confirmationPage';

export const PAGES_ROUTES = {
  PLANS_PAGE : '/plans',
  CHECKOUT_PAGE : '/checkout',
  CONFIRMATION_PAGE : '/confirmation'
};

export const DEFAULT_PAGE = PAGES_ROUTES.PLANS_PAGE;
export const DEFAULT_LOCALE = 'en';

export default (
  <div>
    <Route component={App}>
      <IndexRedirect to={'/:lang' + PAGES_ROUTES.PLANS_PAGE}/>
      <Route path={'/:lang' + PAGES_ROUTES.PLANS_PAGE} component={PlansPage}/>
      <Route path={'/:lang' + PAGES_ROUTES.CHECKOUT_PAGE} component={CheckoutPage}/>
      <Route path={'/:lang' + PAGES_ROUTES.CONFIRMATION_PAGE} component={ConfirmationPage}/>
    </Route>
    <Redirect from="*" to={DEFAULT_LOCALE + '/' + DEFAULT_PAGE}/>
  </div>
);
