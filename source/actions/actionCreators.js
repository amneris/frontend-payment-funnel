/**
 * It is another common convention that, instead of creating action objects inline in the places where you dispatch
 * the actions, you would create functions generating them.
 */
import * as actionTypes from './actionTypes';

export function viewPlansPage() {
  return {
    type: actionTypes.VIEW_PLANS_PAGE
  };
}

export function viewCheckoutPage() {
  return {
    type: actionTypes.VIEW_CHECKOUT_PAGE
  };
}

export function viewConfirmationPage() {
  return {
    type: actionTypes.VIEW_CONFIRMATION_PAGE
  };
}

export function forbidAccess() {
  return {
    type: actionTypes.FORBID_ACCESS
  };
}

export function exitFunnel() {
  return {
    type: actionTypes.EXIT_FUNNEL
  };
}

export function setAppState(appState) {
  return {
    type: actionTypes.SET_APP_STATE,
    appState
  };
}

export function setUrlParams(urlParams) {
  return {
    type: actionTypes.SET_URL_PARAMS,
    urlParams
  };
}

export function requestPlans() {
  return {
    type: actionTypes.REQUEST_PLANS
  };
}

export function requestUserSession() {
  return {
    type: actionTypes.REQUEST_USER_SESSION
  };
}

export function receivePlans(json) {

  return {
    type: actionTypes.RECEIVE_PLANS,
    items: json.plans.map(child => child),
    currency: json.currency
  };
}

export function setReceivePlansError() {
  return {
    type: actionTypes.RECEIVE_PLANS_ERROR,
  };
}

export function receiveUserSession(json) {

  return {
    type: actionTypes.RECEIVE_USER_SESSION,
    userSession: json,
    receivedAt: Date.now()
  };
}

export function requestMethods() {
  return {
    type: actionTypes.REQUEST_METHODS
  };
}

export function receiveMethods(methods) {
  return {
    type: actionTypes.RECEIVE_METHODS,
    methods: methods
  };
}

export function setReceiveMethodsError() {
  return {
    type: actionTypes.RECEIVE_METHODS_ERROR
  };
}

export function requestPaypalToken() {
  return {
    type: actionTypes.REQUEST_PAYPAL_TOKEN
  };
}

export function receivePaypalToken(paypalToken) {
  return {
    type: actionTypes.RECEIVE_PAYPAL_TOKEN,
    paypalToken
  };
}

export function startTransaction() {
  return {
    type: actionTypes.START_TRANSACTION
  };
}

export function requestTransactionInfo() {
  return {
    type: actionTypes.REQUEST_TRANSACTION_INFO
  };
}

export function queueRequestTransactionInfo() {
  return {
    type: actionTypes.QUEUE_REQUEST_TRANSACTION_INFO
  };
}

export function receiveTransactionInfo(transactionInfo) {

  //alert(JSON.stringify(transactionInfo))
  return {
    type: actionTypes.RECEIVE_TRANSACTION_INFO,
    transactionInfo
  }
}

export function setTransactionError(errorType) {
  return {
    type: actionTypes.SET_TRANSACTION_ERROR,
    errorType: errorType
  };
}

export function requestZuoraSignature() {
  return {
    type: actionTypes.REQUEST_SIGNATURE
  };
}

export function receiveZuoraSignature(pageId, json) {
  return {
    type: actionTypes.RECEIVE_SIGNATURE,
    pageId: pageId,
    signature: json.signature,
    token: json.token,
    key: json.key,
    tenantId: json.tenantId
  };
}

export function clickSelectPlan(plan) {
  return {
    type: actionTypes.CLICK_SELECT_PLAN,
    plan
  }
}

export function trackCheckoutCart() {
  return {
    type: actionTypes.CHECKOUT_CART_LOADED
  }
}

export function trackStartPaymentProcess() {
  return {
    type: actionTypes.START_PAYMENT_PROCESS
  }
}

export function selectPlan(planId, discountId, period, planTitleKey) {
  return {
    type: actionTypes.SELECT_PLAN,
    planId: planId,
    discountId: discountId,
    period: period,
    planTitleKey: planTitleKey
  };
}

export function selectMethod(methodId) {
  return {
    type: actionTypes.SELECT_METHOD,
    methodId
  };
}

export function resetCredentials() {
  return {
    type: actionTypes.RESET_CREDENTIALS
  };
}

export function goToCheckout(plan) {
  return {
    type: actionTypes.GO_TO_CHECKOUT,
    plan
  };
}

export function goToPlans() {
  return {
    type: actionTypes.GO_TO_PLANS
  };
}

export function goToConfirmation(token, gatewayName, methodName) {
  return {
    type: actionTypes.GO_TO_CONFIRMATION,
    token,
    gatewayName,
    methodName
  };
}

export function goToCampus() {
  return {
    type: actionTypes.GO_TO_CAMPUS
  };
}

export function goToCourse() {
  return {
    type: actionTypes.GO_TO_COURSE
  };
}

export function trackPaymentSuccess(transactionData) {
  return {
    type: actionTypes.PAYMENT_SUCCESS,
    transactionData
  };
}

export function convertUserToPremium() {
  return {
    type: actionTypes.CONVERT_USER_TO_PREMIUM
  };
}

export function swipeReviewCarousel(direction, numberOfReviews){
  return {
    type: actionTypes.SWIPE_REWIEW_CAROUSEL,
    direction,
    numberOfReviews
  };
}

export function selectReviewCarousel(index) {
  return {
    type: actionTypes.SELECT_REWIEW_CAROUSEL,
    index
  };
}

export function requestTermsAndConditions() {
  return {
    type: actionTypes.REQUEST_TERMS_AND_CONDITIONS
  };
}

export function receiveTermsAndConditions() {
  return {
    type: actionTypes.RECEIVE_TERMS_AND_CONDITIONS
  };
}

export function requestPrivacyPolicy() {
  return {
    type: actionTypes.REQUEST_PRIVACY_POLICY
  };
}

export function receivePrivacyPolicy() {
  return {
    type: actionTypes.RECEIVE_PRIVACY_POLICY
  };
}
