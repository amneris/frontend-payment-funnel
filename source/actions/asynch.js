/**
 * Redux Thunk middleware allows you to write action creators that return a function instead of an action.
 * The thunk can be used to delay the dispatch of an action, or to dispatch only if a certain condition is met.
 * The inner function receives the store methods dispatch and getState as parameters.
 */
import AppConfig from '../config/appConfig';
import * as ErrorTypes from './errorTypes';

import {
  setAppState, requestPlans, requestUserSession,
  receivePlans, setReceivePlansError, receiveUserSession,
  requestMethods, receiveMethods, setReceiveMethodsError, requestZuoraSignature, receiveZuoraSignature,
  requestPaypalToken, receivePaypalToken, startTransaction, requestTransactionInfo, queueRequestTransactionInfo,
  receiveTransactionInfo, setTransactionError, forbidAccess, requestTermsAndConditions, receiveTermsAndConditions,
  requestPrivacyPolicy, receivePrivacyPolicy
} from './actionCreators';

import {
  shouldFetchUserSession, shouldFetchPlans, shouldFetchMethods, appendParamsToUrl,
  appendAutoLoginParamToUrl
} from './utils';

import { apiRequest, staticGet } from '../services/api';

import { getLegalTermsAndConditionsUrl, getLegalPrivacyPolicyUrl } from '../reducers/utils';

function fetchUserSession(currentParams = {}) {
  return dispatch => {
    var url = AppConfig.getCampusUrl() + '/payments/wsGetUserData';
    url = appendAutoLoginParamToUrl(url, currentParams.autol);

    // dispatch action so that store's state accounts for fact that user session is being fetched
    dispatch(requestUserSession());

    // now go and fetch user session
    return apiRequest(url, undefined, undefined, true)
      .then(userSessionJson => {
        dispatch(receiveUserSession(userSessionJson));
        dispatch(setAppState({startApp: true}));
      })
      .catch(function (err) {
        switch (err.status) {
          case 401:
            dispatch(forbidAccess());

          default:
            // that means we have an execution error of our code
            console.error('ERROR OCCURRED - ' + err.name + ': ' + err.message);
            break;
        }
      });
  };
}

function fetchPlans(userId, segmentId) {

  return (dispatch) => {

    if (!userId) {
      dispatch(setReceivePlansError());
    } else {
      var url = AppConfig.getAbaZuoraApiUrl() + '/plans';
      var params = {user: userId};

      if (segmentId) Object.assign(params, {segment: segmentId});

      url = appendParamsToUrl(url, params);

      dispatch(requestPlans());
      // now go and fetch plans
      return apiRequest(url)
        .then(plansJson => {
          if (plansJson.plans.length > 0) {
            dispatch(receivePlans(plansJson))
          }
          else {
            throw new Error();
          }
        })
        .catch(function (err) {
          console.error(err);
          dispatch(setReceivePlansError());
        });
    }
  };

}

function fetchMethods() {
  return (dispatch, getState) => {
    const state = getState();

    var url = AppConfig.getAbaZuoraApiUrl() + '/paymentforms';
    var params = {
      user: state.userSession.id
    };

    url = appendParamsToUrl(url, params);

    dispatch(requestMethods());

    // now go and fetch hosted pages
    return apiRequest(url)
      .then(methodsArray => {
        if (methodsArray.length > 0) {
          dispatch(receiveMethods(methodsArray))
        }
        else {
          throw new Error();
        }
      })
      .catch(() => {
        dispatch(setReceiveMethodsError());
      });
  };
}

/* public methods */

export function callActionFetchUserSessionIfNeeded(currentParams) {
  return (dispatch, getState) => {
    if (shouldFetchUserSession(getState())) {
      return dispatch(fetchUserSession(currentParams));
    }
    else {
      return dispatch(setAppState({startApp: true}));
    }
  };
}


export function callActionFetchPlansIfNeeded() {

  return (dispatch, getState) => {

    const state = getState();

    const segmentId = state.appState.segmentId;
    const userId = state.userSession.id;

    if (shouldFetchPlans(state)) {
      return dispatch(fetchPlans(userId, segmentId));
    }
  };
}

export function callActionFetchMethodsIfNeeded() {

  return (dispatch, getState) => {

    const state = getState();

    if (shouldFetchMethods(state)) {
      return dispatch(fetchMethods());
    }
  };
}

export function callActionFetchZuoraSignature(pageId) {

  return dispatch => {

    var url = AppConfig.getAbaZuoraApiUrl() + '/signatures';
    url = appendParamsToUrl(url, { page : pageId });

    dispatch(requestZuoraSignature());

    // now go and fetch hosted pages
    return apiRequest(url)
      .then(signatureJson => dispatch(receiveZuoraSignature(pageId, signatureJson)))
      .catch(function (err) {
        //console.log('ERROR GETTING SIGNATURE - ' + err);
      });
  };
}

export function callActionStartPaypalTransaction(urlSuccess, urlCancel, userEmail) {
  return (dispatch) => {

    let url = AppConfig.getAbaZuoraApiUrl() + '/subscriptions/paypal/token';

    let params = {
      urlSuccess : urlSuccess,
      urlCancel : urlCancel,
      emailBuyer : userEmail
    };

    dispatch(requestPaypalToken());

    return apiRequest(url, 'post', params)
      .then(subscriptionJson => {
        if (subscriptionJson['paypalToken']) {
          dispatch(receivePaypalToken(subscriptionJson['paypalToken']));
        }
        else {
          throw new Error();
        }
      })
      .catch(() => dispatch(callActionSetTransactionError(ErrorTypes.PAYPAL_TOKEN)));
  };
}

export function callActionStartTransaction(gatewayName, methodName, plan, token) {

  return (dispatch, getState) => {

    const state = getState();

    let url = AppConfig.getAbaZuoraApiUrl() + '/transactions';
    let ratePlans = [plan.id];

    if (plan.prices.promo) {
      ratePlans.push(plan.prices.promo.discountId);
    }

    let params = {
      userId: parseInt(state.userSession.id),
      gatewayName: gatewayName,
      type: methodName,
      ratePlans : ratePlans,
      period: plan.period
    };

    if (methodName === 'PayPal') {
      params['paypalToken'] = token;
    }
    else {
      params['zuoraRefId'] = token;
    }

    dispatch(startTransaction());

    return apiRequest(url, 'post', params)
      .then(transactionResponse => {

        if (transactionResponse['id'] && transactionResponse['state'] === 'OPEN') {
          dispatch(receiveTransactionInfo(transactionResponse));
        }
        else {
          throw new Error();
        }
      })
      .catch((err) => dispatch(callActionSetTransactionError(ErrorTypes.HTTP_ERROR, err)));
  };
}

export function callActionFetchTransactionInfo(transactionId) {


  return (dispatch, getState) => {

    const state = getState();

    let { isFetching } = state.transaction;

    if (isFetching) {
      dispatch(queueRequestTransactionInfo());

      console.debug('Already fetching transaction info, so return')
      return undefined;
    }
    else {
      dispatch(requestTransactionInfo());

      let url = AppConfig.getAbaZuoraApiUrl() + '/transactions/' + transactionId + '?expand=invoice,dueDate';

      return apiRequest(url)
        .then(transactionResponse => {

          console.debug("getTransactionInfo response : " + JSON.stringify(transactionResponse))

          if (transactionResponse['state'] !== 'ERROR') {

            dispatch(receiveTransactionInfo(transactionResponse));
          }
          else {
            throw new Error();
          }

          if (getState().transaction.haveQueuedRequest) {
            dispatch(callActionFetchTransactionInfo(transactionId));
          }

          return transactionResponse;

        })
        .catch((err) => dispatch(callActionSetTransactionError(ErrorTypes.HTTP_ERROR, err)));
    }
  }
}

export function callActionSetTransactionError(type, errorDetails) {

  if (!errorDetails) errorDetails = '';

  return (dispatch) => {
    console.error('Payment transaction error occurred of type : ' + type + '. Error details : ' + errorDetails);
    dispatch(setTransactionError(type));
  }
}

export function callActionFetchTermsAndConditions() {
  return (dispatch, getState) => {

    const state = getState();

    if (!state.externalContent.isFetchingTermsAndConditions) {

      let url = getLegalTermsAndConditionsUrl(state.appState.language);

      dispatch(requestTermsAndConditions());

      return callActionFetchExternalContent(url)
        .then(text => {
          dispatch(receiveTermsAndConditions());

          return text;
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };
}

export function callActionFetchPrivacyPolicy() {
  return (dispatch, getState) => {

    const state = getState();

    if (!state.externalContent.isFetchingPrivacyPolicy) {

      let url = getLegalPrivacyPolicyUrl(state.appState.language);

      dispatch(requestPrivacyPolicy());

      return callActionFetchExternalContent(url)
        .then(text => {
          dispatch(receivePrivacyPolicy());

          return text;
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };
}

export function callActionFetchExternalContent(url) {
  return staticGet(url);
}
