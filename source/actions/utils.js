import QueryString from 'query-string';

import { PAGES_ROUTES } from '../routes';
import { getLanguageFromUrl } from '../reducers/utils';

export function shouldFetchUserSession(state) {
  const userSession = state.userSession;

  if (!userSession || !userSession.id) {
    return true;
  }

  if (userSession.isFetching) {
    return false;
  }

  return false;
}

export function shouldFetchPlans(state) {
  const plans = state.plans;

  if (plans.isFetching) {
    return false;
  } else if (!plans.items || plans.items.length < 1) {
    return true;
  }

  return false;
}

export function shouldFetchMethods(state) {
  const methods = state.checkout.methods;

  if (methods.isFetching) {
    return false;
  } else if (!methods.items) {
    return true;
  }

  return false;
}

export function getCurrentUrl() {
  if (typeof window === 'object') {
    return window.location.origin + window.location.pathname;
  }

  return '';
}

export function getConfirmationPageUrl() {
  if (typeof window === 'object') {
    let language = getLanguageFromUrl(window.location.pathname);

    return window.location.origin + '/' + language + PAGES_ROUTES.CONFIRMATION_PAGE;
  }

  return '';
}

export function appendParamsToUrl(url, params) {
  if (params instanceof Object) {
    url += '?' + QueryString.stringify(params);
  }

  return url;
}

export function appendAutoLoginParamToUrl(url, autoLoginKey) {

  if (autoLoginKey) {
    url += '?' + QueryString.stringify({ autol: autoLoginKey });
  }

  return url;
}

export function findPlanById(planId, plans) {
  if (planId && plans && plans.length > 1) {
    return plans.find(
      plan => plan.id === planId
    );
  }

  return false;
}
