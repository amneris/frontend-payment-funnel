import {
  setAppState, setUrlParams, forbidAccess, exitFunnel, clickSelectPlan, selectPlan, selectMethod, resetCredentials,
  goToCheckout, goToPlans, goToConfirmation, viewPlansPage, viewCheckoutPage, viewConfirmationPage,
  swipeReviewCarousel, selectReviewCarousel, trackCheckoutCart, trackPaymentSuccess, goToCampus, goToCourse, convertUserToPremium, trackStartPaymentProcess
} from './actionCreators';

import { findPlanById } from './utils';

export function callActionSetAppState(appState) {
  return setAppState(appState);
}

export function callActionSetUrlParams(urlParams) {
  return setUrlParams(urlParams);
}

export function callActionExitFunnel() {
  return exitFunnel();
}

export function callActionForbidAccess() {
  return forbidAccess();
}

export function callActionViewPlansPage() {
  return viewPlansPage();
}

export function callActionViewCheckoutPage() {
  return viewCheckoutPage();
}

export function callActionViewConfirmationPage() {
  return viewConfirmationPage();
}

export function callActionClickSelectPlan(plan) {
  return clickSelectPlan(plan);
}

export function callActionTrackCheckoutCart() {
  return trackCheckoutCart();
}

export function callActionTrackStartPaymentProcess() {
  return trackStartPaymentProcess();
}

export function callActionSelectPlan(planId) {
  return (dispatch, getState) => {
    const state = getState();

    if (state.plans.items && state.plans.items.length > 1) {
      const selectedPlan = state.appState.shoppingCart ? state.appState.shoppingCart.plan : undefined;
      const newSelectedPlan = findPlanById(planId, state.plans.items);

      if (newSelectedPlan && newSelectedPlan !== selectedPlan) {
        const selectedPromo = newSelectedPlan.prices.promo;
        var discountId = selectedPromo ? selectedPromo.discountId : undefined;
        var period = newSelectedPlan.period;
        var planTitleKey = newSelectedPlan.titleKey;

        dispatch(selectPlan(planId, discountId, period, planTitleKey));
      }
    }
  };
}

export function callActionSelectMethod(methodId) {
   return selectMethod(methodId);
}

export function callActionResetCredentials() {
   return resetCredentials();
}

export function callActionGoToCheckout(plan) {
  return goToCheckout(plan);
}

export function callActionGoToPlans() {
  return goToPlans();
}

export function callActionGoToConfirmation(token, gatewayName, methodName) {
  return goToConfirmation(token, gatewayName, methodName);
}

export function callActionGoToCampus() {
  return goToCampus();
}

export function callActionGoToCourse() {
  return goToCourse();
}

export function callActionTrackPaymentSuccess(paymentData) {
  return trackPaymentSuccess(paymentData);
}

export function callActionConvertUserToPremium() {
  return convertUserToPremium();
}

export function callActionSwipeReviewCarousel(direction, numberOfReviews) {
  return swipeReviewCarousel(direction, numberOfReviews);
}

export function callActionSelectReviewCarousel(index) {
  return selectReviewCarousel(index);
}
