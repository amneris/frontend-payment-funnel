import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import Asynch, {
  callActionFetchUserSessionIfNeeded,
  callActionFetchMethodsIfNeeded,
  callActionFetchPlansIfNeeded,
  callActionFetchZuoraSignature
} from './asynch';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const CAMPUS_MOCKED_URL = 'http://campus-mocked-url';
const ZUORA_API_MOCKED_URL = 'http://api-mocked-url';


const mockedUserSessionData = {
  id: -1,
  type: 'free',
  language: 'es',
  name: 'Student 83',
  email: 'email@abaenglish.com',
  country: {
    id: -1,
    iso: 'ES'
  },
  isFetching: false
};

const mockedPlansData = {
  plans: [{
    id: '-1',
    titleKey: '1_month',
    featured: false,
    period: 1,
    prices: { monthlyBase: 19.99, base: 19.99, promo: null }
  }, {
    id: '-2',
    titleKey: '6_months',
    featured: false,
    period: 6,
    prices: { monthlyBase: 10, base: 59.99, promo: null }
  }],
  currency: { symbol: '€', iso: 'EUR', symbolShowBeforePrice: false, decimalSeparator: '.' }
};

const mockedMethodsData = {
  isFetching: false,
  items: [
    { defaultOption: true,
      methodName: 'CREDIT_CARD',
      pageId: '2c92c0f854a35e960154a965550b6ece'
    }, {
      defaultOption: false,
      methodName: 'BANK_TRANSFER',
      pageId: '2c92c0f854a35e960154a9668350793a'
    }
  ]
};

const mockedSignatureData = {
  isFetching: false,
  data: {
    signature: 'mocked_signature',
    success: true
  }
};

describe('async actions', () => {

  // mock services url
  Asynch.__Rewire__('AppConfig', {
    getAbaZuoraApiUrl: () => ZUORA_API_MOCKED_URL,
    getCampusUrl: () => CAMPUS_MOCKED_URL
  });

  afterEach(() => {
    nock.cleanAll();
  });

  /* ********************************************************************************************************* */
  /* **** USER SESSION *************************************************************************************** */
  /* ********************************************************************************************************* */

  describe('get user session', () => {

    it('creates RECEIVE_USER_SESSION when fetching user session has been done', () => {

      nock(CAMPUS_MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply(200, mockedUserSessionData);

      const expectedActions = [
        {type: 'REQUEST_USER_SESSION'},
        {
          type: 'RECEIVE_USER_SESSION',
          userSession: mockedUserSessionData
        }];

      const store = mockStore({ appState: { startApp: false } });

      return store.dispatch(callActionFetchUserSessionIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('creates RECEIVE_USER_SESSION when fetching user session has been done with auto login key', () => {

      nock(CAMPUS_MOCKED_URL)
        .get('/payments/wsGetUserData?autol=some_auto_login_key')
        .reply(200, mockedUserSessionData);

      const expectedActions = [
        {type: 'REQUEST_USER_SESSION'},
        {
          type: 'RECEIVE_USER_SESSION',
          userSession: mockedUserSessionData
        }];

      const store = mockStore({ appState: { startApp: false } });

      return store.dispatch(callActionFetchUserSessionIfNeeded({ autol: 'some_auto_login_key' }))
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('creates FORBIDDEN_ACCESS when fetching user session has failed because unauthorized', () => {

      nock(CAMPUS_MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply(401);

      const expectedActions = [
        { type: 'REQUEST_USER_SESSION' },
        { type: 'FORBIDDEN_ACCESS' }
      ];

      const store = mockStore({ appState: { startApp: false } });

      return store.dispatch(callActionFetchUserSessionIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('do not reach RECEIVE_USER_SESSION when fetching user session has failed', () => {

      nock(CAMPUS_MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply();

      const expectedActions = [
        { type: 'REQUEST_USER_SESSION' }
      ];

      const store = mockStore({ appState: { startApp: false } });

      return store.dispatch(callActionFetchUserSessionIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('return existing user session if data has already been fetched', () => {

      const store = mockStore({ userSession: mockedUserSessionData });

      const expectedActions = [
        { type: 'SET_APP_STATE', appState: { startApp: true } }
      ];

      store.dispatch(callActionFetchUserSessionIfNeeded());

      return expect(store.getActions()).toEqual(expectedActions);
    });

    it('return existing user session if data is being fetched', () => {

      const store = mockStore({ userSession: Object.assign(mockedUserSessionData, { isFetching: true }) });

      const expectedActions = [
        { type: 'SET_APP_STATE', appState: { startApp: true } }
      ];

      store.dispatch(callActionFetchUserSessionIfNeeded());

      return expect(store.getActions()).toEqual(expectedActions);
    });

  });

  /* ********************************************************************************************************* */
  /* **** PLANS ********************************************************************************************** */
  /* ********************************************************************************************************* */

  describe('get plans', () => {

    it('creates RECEIVE_PLANS when fetching plans has been done', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/plans?segment=-1&user=-1')
        .reply(200, mockedPlansData);

      const expectedActions = [
        {type: 'REQUEST_PLANS'},
        {
          type: 'RECEIVE_PLANS',
          items: mockedPlansData.plans,
          user: undefined,
          currency: {'symbol': '€', 'iso': 'EUR', 'symbolShowBeforePrice': false, 'decimalSeparator': '.'}
        }];

      const store = mockStore({
        appState: { segmentId: -1 },
        userSession: { id: -1 },
        plans: { isFetching: false }
      });

      return store.dispatch(callActionFetchPlansIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('creates RECEIVE_PLANS when fetching plans has been done with no segment', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/plans?user=-1')
        .reply(200, mockedPlansData);

      const expectedActions = [
        {type: 'REQUEST_PLANS'},
        {
          type: 'RECEIVE_PLANS',
          items: mockedPlansData.plans,
          user: undefined,
          currency: {'symbol': '€', 'iso': 'EUR', 'symbolShowBeforePrice': false, 'decimalSeparator': '.'}
        }];

      const store = mockStore({
        appState: { segmentId: undefined },
        userSession: { id: -1 },
        plans: { isFetching: false }
      });

      return store.dispatch(callActionFetchPlansIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('creates RECEIVE_PLANS if we have empty plans list loaded previously', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/plans?segment=-1&user=-1')
        .reply(200, mockedPlansData);

      const expectedActions = [
        {type: 'REQUEST_PLANS'},
        {
          type: 'RECEIVE_PLANS',
          items: mockedPlansData.plans,
          user: undefined,
          currency: {'symbol': '€', 'iso': 'EUR', 'symbolShowBeforePrice': false, 'decimalSeparator': '.'}
        }];

      const store = mockStore({
        appState: { segmentId: -1 },
        userSession: { id: -1 },
        plans: { items: [], isFetching: false }
      });

      return store.dispatch(callActionFetchPlansIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('creates RECEIVE_PLANS_ERROR when fetching plans has been failed', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/plans?segment=-1&user=-1')
        .reply(500);

      const expectedActions = [
        {type: 'REQUEST_PLANS'},
        {type: 'RECEIVE_PLANS_ERROR'}
      ];

      const store = mockStore({appState: {segmentId: -1}, userSession: {id: -1}, plans: {isFetching: false}});

      return store.dispatch(callActionFetchPlansIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('creates RECEIVE_PLANS_ERROR when user is not defined', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/plans?segment=-1')
        .reply(200);

      const expectedActions = [
        {type: 'RECEIVE_PLANS_ERROR'}
      ];

      const store = mockStore({appState: {segmentId: -1}, userSession: {id: null}, plans: {isFetching: false}});
      store.dispatch(callActionFetchPlansIfNeeded());
      return expect(store.getActions()).toEqual(expectedActions);
    });

    it('return no actions if data is being fetched', () => {

      const store = mockStore({
        appState: { segmentId: -1} ,
        userSession: { id: -1 },
        plans: { isFetching: true }
      });

      const expectedActions = [];

      store.dispatch(callActionFetchPlansIfNeeded());

      return expect(store.getActions()).toEqual(expectedActions);
    });

    it('return no actions if data has already been fetched', () => {

      const store = mockStore(Object.assign(
        { appState: { segmentId: -1 }, userSession: mockedUserSessionData },
        { plans: { items: mockedPlansData.plans } }
      ));

      const expectedActions = [];

      store.dispatch(callActionFetchPlansIfNeeded());

      return expect(store.getActions()).toEqual(expectedActions);
    });
  });

  /* ********************************************************************************************************* */
  /* **** HOSTED PAGES *************************************************************************************** */
  /* ********************************************************************************************************* */

  describe('get Zuora hosted pages', () => {

    it('creates RECEIVE_METHODS when fetching hosted pages has been done', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/paymentforms?user=-1')
        .reply(200, mockedMethodsData.items);

      const expectedActions = [
        {type: 'REQUEST_METHODS'},
        {
          type: 'RECEIVE_METHODS',
          methods: mockedMethodsData
        }];

      const store = mockStore({ userSession: {id: -1}, checkout: { methods: {} } });

      return store.dispatch(callActionFetchMethodsIfNeeded())
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('don\'t reach RECEIVE_METHODS when fetching hosted pages has failed', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/paymentforms?user=-1')
        .reply(500);

      const expectedActions = [
        {type: 'REQUEST_METHODS'}
      ];

      const store = mockStore({ userSession: {id: -1}, checkout: { methods: {} } });

      store.dispatch(callActionFetchMethodsIfNeeded())

      return expect(store.getActions()).toEqual(expectedActions);
    });

    it('return no actions if data is being fetched', () => {

      const store = mockStore({
        appState: { segmentId: -1} ,
        userSession: { id: -1 },
        checkout: {
          methods: {
            isFetching: true
          }
        }
      });

      const expectedActions = [];

      store.dispatch(callActionFetchMethodsIfNeeded());

      return expect(store.getActions()).toEqual(expectedActions);
    });

    it('return no actions if data has already been fetched', () => {

      const store = mockStore(Object.assign(
        { appState: { segmentId: -1 }, userSession: mockedUserSessionData },
        { checkout: { methods: mockedMethodsData } }
      ));

      const expectedActions = [];

      store.dispatch(callActionFetchMethodsIfNeeded());

      return expect(store.getActions()).toEqual(expectedActions);
    });
  });

  /* ********************************************************************************************************* */
  /* **** ZUORA SIGNATURE ************************************************************************************ */
  /* ********************************************************************************************************* */

  describe('get Zuora signature for a hosted page', () => {

    it('creates RECEIVE_SIGNATURE when fetching hosted pages has been done', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/signatures?page=-1')
        .reply(200, mockedSignatureData);

      const expectedActions = [
        {type: 'REQUEST_SIGNATURE'},
        {
          type: 'RECEIVE_SIGNATURE',
          methods: mockedSignatureData
        }];

      const store = mockStore({ userSession: {id: -1}, checkout: {} });

      return store.dispatch(callActionFetchZuoraSignature(-1))
        .then(() => { // return of async actions 
          expect(store.getActions()).toEqual(expectedActions)
        });
    });

    it('don\'t reach RECEIVE_METHODS when fetching hosted pages has failed', () => {

      nock(ZUORA_API_MOCKED_URL)
        .get('/signatures?page=-1')
        .reply(500);

      const expectedActions = [
        {type: 'REQUEST_SIGNATURE'}
      ];

      const store = mockStore({ userSession: {id: -1}, checkout: {} });

      store.dispatch(callActionFetchZuoraSignature())

      return expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
