import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Synch, {
  callActionSetAppState, callActionExitFunnel, callActionForbidAccess, callActionViewPlansPage,
  callActionSelectPlan, callActionSelectMethod, callActionGoToCheckout, callActionGoToPlans
} from './synch';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const mockedPlansData = {
  items: [{
    id: 'some_plan',
    titleKey: '1_month',
    featured: false,
    period: 1,
    prices: { monthlyBase: 19.99, base: 19.99, promo: null }
  }, {
    id: 'some_plan_with_promo',
    titleKey: '6_months',
    featured: false,
    period: 6,
    prices: {
      monthlyBase: 10, base: 89.99,
      promo: {
        discountId: 'some_promo',
        discount: 20,
        discountBase: 71.99,
        discountMonthly: 12
      }
    }
  }],
  currency: { symbol: '€', iso: 'EUR', symbolShowBeforePrice: false, decimalSeparator: '.' }
};

const mockedMethodsData = [
  { defaultOption: true,
    methodName: 'CREDIT_CARD',
    pageId: '2c92c0f854a35e960154a965550b6ece'
  }, {
    defaultOption: false,
    methodName: 'BANK_TRANSFER',
    pageId: '2c92c0f854a35e960154a9668350793a'
  }
];

describe('sync actions', () => {

  describe('set app state', () => {
    it('should create SET_APP_STATE when the action is called', () => {

      const actionData = {title: 'some_title', language: 'some_language'};
      const expectedActions = [
        {
          type: 'SET_APP_STATE',
          appState: actionData
        }
      ];

      const store = mockStore({});

      store.dispatch(callActionSetAppState(actionData));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('exit funnel', () => {
    it('should create EXIT_FUNNEL when the action is called', () => {

      const expectedActions = [
        {
          type: 'EXIT_FUNNEL'
        }
      ];

      const store = mockStore({});

      store.dispatch(callActionExitFunnel());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('fobid access', () => {
    it('should create FORBID_ACCESS when the action is called', () => {

      const expectedActions = [
        {
          type: 'FORBID_ACCESS'
        }
      ];

      const store = mockStore({});

      store.dispatch(callActionForbidAccess());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('plans page laoded', () => {
    it('should create VIEW_PLANS_PAGE when the action is called', () => {

      const expectedActions = [
        {
          type: 'VIEW_PLANS_PAGE'
        }
      ];

      const store = mockStore({});

      store.dispatch(callActionViewPlansPage());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('select plan', () => {
    it('should create SELECT_PLAN when the select plan exists', () => {

      const expectedActions = [
        {
          type: 'SELECT_PLAN',
          planId: 'some_plan',
          discountId: undefined,
          period:1,
          planTitleKey : '1_month'
        }
      ];

      const store = mockStore({ appState: {}, plans: mockedPlansData });

      store.dispatch(callActionSelectPlan('some_plan'));
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('should create SELECT_PLAN when the select plan exists with promo', () => {

      const expectedActions = [
        {
          type: 'SELECT_PLAN',
          planId: 'some_plan_with_promo',
          discountId: 'some_promo',
          period:6,
          planTitleKey : '6_months'
        }
      ];

      const store = mockStore({  appState: {}, plans: mockedPlansData });

      store.dispatch(callActionSelectPlan('some_plan_with_promo'));
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('should create no action when the select plan does not exist', () => {

      const expectedActions = [];

      const store = mockStore({  appState: {}, plans: mockedPlansData });

      store.dispatch(callActionSelectPlan('something_wrong'));
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('should create no action when there is no select plan', () => {

      const expectedActions = [];

      const store = mockStore({  appState: {}, plans: mockedPlansData });

      store.dispatch(callActionSelectPlan());
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('should create no action when there is no plans', () => {

      const expectedActions = [];

      const store = mockStore({ plans: { items: [] } });

      store.dispatch(callActionSelectPlan('some_plan'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('select method', () => {
    it('should create SELECT_METHOD when the action is called', () => {

      const actionData = '2c92c0f854a35e960154a965550b6ece';
      const expectedActions = [
        {
          type: 'SELECT_METHOD',
          methodId: actionData
        }
      ];

      const store = mockStore({ checkout: { methods: mockedMethodsData } });

      store.dispatch(callActionSelectMethod(actionData));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('go to checkout', () => {
    it('should create GO_TO_CHECKOUT when the action is called', () => {

      const expectedActions = [
        {
          type: 'GO_TO_CHECKOUT',
          plan: 'some_plan'
        }
      ];

      const store = mockStore({ appState: {} });

      store.dispatch(callActionGoToCheckout('some_plan'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('go to plans', () => {
    it('should create GO_TO_PLANS when the action is called', () => {

      const expectedActions = [
        {
          type: 'GO_TO_PLANS'
        }
      ];

      const store = mockStore({ appState: {} });

      store.dispatch(callActionGoToPlans());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

});
