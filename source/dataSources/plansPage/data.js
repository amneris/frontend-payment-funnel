/**
 * populate an array which contains hardcoded user features and reviews
 * @type {Array}
 */

var dataSources = []
dataSources['features'] = require('./features.json');
dataSources['features-abtest-variation-b'] = require('./features-abtest-variation-b.json');
dataSources['reviews'] = require('./reviews.json');

module.exports = dataSources;
