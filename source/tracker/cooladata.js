import {
  CLICK_SELECT_PLAN,
  VIEW_PLANS_PAGE,
  VIEW_CHECKOUT_PAGE,
  VIEW_CONFIRMATION_PAGE,
  PAYMENT_SUCCESS,
  START_PAYMENT_PROCESS
} from '../actions/actionTypes';


function trackEvent(eventName, eventInfo) {
  if (window.cooladata !== undefined) {
    window.cooladata.trackEvent(eventName, eventInfo);
    //console.debug('Sent event to Cooladata : ', eventName, eventInfo);
  }
}


export default class CoolaData {
  constructor(trackerSubscriber) {
    this.trackerSubscriber = trackerSubscriber;
    this.name = 'Cooladata Event Tracking';
  }

  setKey(key) {
    this.key = key;
  }

  getInjectableCode() {
    return `(function(d,a){if(!a.__SV){var b,c,g,e;window.cooladata=a;a._i=[];a.init=function(b,c,f){function d(a,b){var c=b.split(".");2==c.length&&(a=a[c[0]],b=c[1]);a[b]=function(){a.push([b].concat(Array.prototype.slice.call(arguments,0)))}}var h=a;"undefined"!==typeof f?h=a[f]=[]:f="cooladata";g=["trackEvent","trackEventLater","trackPageload","flush","setConfig"];for(e=0;e<g.length;e++)d(h,g[e]);a._i.push([b,c,f])};a.__SV=1.2;b=d.createElement("script");b.type="text/javascript";b.async=!0;b.src="//cdn.cooladata.com/tracking/cooladata-2.1.12.min.js";c=d.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.cooladata||[]);
    cooladata.init({
        "app_key": '${this.key}',
        "track_pageload": false,
        "img_src_get_request": true
    });`;
  }

  injectIntoHead() {
    this.trackerSubscriber.injectIntoHead(this.getInjectableCode(), this.name);
  }


  handleEvent(action, state) {

    let eventInfo = {
      idSite: 1,
      event_timestamp_epoch: new Date().getTime(),
      user_id: state.userSession.id,
      new_platform: 1
    };

    switch (action.type) {

      case VIEW_PLANS_PAGE:

        trackEvent('OPENED_PRICES', eventInfo);
        break;

      case START_PAYMENT_PROCESS:
        trackEvent('START_PAYMENT_PROCESS', eventInfo);
        break;

      case CLICK_SELECT_PLAN:

        // todo: in what case would we not have a plan?
        if (action.plan) {
          let eventName = 'SELECTED_PRICE_PLAN';

          eventInfo = Object.assign(eventInfo, {
            selected_plan: action.plan.period * 30 // days
          });

          // send event to cooladata
          trackEvent(eventName, eventInfo);
        }

        break;

      case VIEW_CHECKOUT_PAGE:

        trackEvent('OPENED_CHECKOUT_PAGE', eventInfo);

        break;

      case VIEW_CONFIRMATION_PAGE:

        trackEvent('OPENED_CONFIRMATION_PAGE', eventInfo);

        break;

      case PAYMENT_SUCCESS:

        let transactionData = action.transactionData;

        if (transactionData.period && transactionData.subscriptionId && transactionData.invoice) {

          let price = transactionData.invoice.finalPrice;
          let discount = transactionData.invoice.discount ? Math.abs(transactionData.invoice.discount.discountPrice) : 0;

          eventInfo = Object.assign(eventInfo, {
            price_paid: price,
            discount_applied: discount,
            transaction_currency: state.userSession.currency.iso,
            product_bought: transactionData.period * 30 // days
          });

          // send event to cooladata
          trackEvent('PAID', eventInfo);
        }

        break;
    }
  }
}
