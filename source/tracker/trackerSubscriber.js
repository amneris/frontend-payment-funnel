import VWO from './vwo';
import GTM from './gtm';
import GA from './ga';
import Cooladata from './cooladata';
import NewRelic from './newRelic';

export default class TrackerSubscriber {

  constructor() {

    this.trackerHandlers = []; // observers

    this.gtm = new GTM(this);
    this.vwo = new VWO(this);
    this.ga = new GA(this);
    this.cooladata = new Cooladata(this);
    this.newRelic = new NewRelic(this);

    this.subscribe(this.gtm.handleEvent.bind(this.gtm));
    this.subscribe(this.ga.handleEvent.bind(this.ga));
    this.subscribe(this.cooladata.handleEvent.bind(this.cooladata));
    this.subscribe(this.vwo.handleEvent.bind(this.vwo));

  }

  // subscribe observers to events received by this tracker
  subscribe(fn) {

    if (fn) {
      this.trackerHandlers.push(fn);
    }
  }

  // send event to all observers
  emitEvent(action, state) {

    //let scope = window

    this.trackerHandlers.map(tracker => {
      tracker.call(this, action, state)
    })
  }


  injectIntoHead(code, description) {
    var children = this.getInjectableCode(code, description);

    for (var i = 0; i < children.length; i ++) {
      document.getElementsByTagName('head')[0].appendChild(children[i]);
    }
  }

  getInjectableCode(code, description) {
    var startDescription = document.createComment('Start: ' + description);
    var endDescription = document.createComment('End: ' + description);

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.text = code;

    return [startDescription, script, endDescription];
  }
}
