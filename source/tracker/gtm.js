import { RECEIVE_USER_SESSION, VIEW_PLANS_PAGE, VIEW_CHECKOUT_PAGE, CLICK_SELECT_PLAN, CHECKOUT_CART_LOADED, PAYMENT_SUCCESS } from '../actions/actionTypes';
import { findPlanById } from '../actions/utils';
import { priceWithCurrency } from '../reducers/utils';

export default class GTM {
  constructor(trackerSubscriber) {
    this.trackerSubscriber = trackerSubscriber;
    this.name = 'Google Tag Manager';
  }

  setId(id) {
    this.id = id;
  }

  getInjectableCode() {
    return `(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '${this.id}');`;
  }

  setDataLayer(params) {
    /*
     Params used by GTM:
     userId, partnerSource, email, device, paymentValue, promoCode, discount, currency, idProduct,
     paySuppExtId, idPayment, quantity, productPlan, idSite
     */

    if (window.dataLayer) {
      window.dataLayer.push(params);
    }
  }

  triggerEvent(event) {
    window.dataLayer.push({event: event});
  }

  handleEvent(action, state) {
    switch (action.type) {

      case RECEIVE_USER_SESSION:
        const { userSession } = action;

        if (userSession) {
          this.setDataLayer({
            userId: userSession.id,
            userEmail: userSession.email,
            userLanguage: userSession.language
          });
        }

        break;

      case VIEW_PLANS_PAGE:
        this.triggerEvent('view_plans_page');

        break;

      case VIEW_CHECKOUT_PAGE:
        this.triggerEvent('view_checkout_page');

        break;

      case CLICK_SELECT_PLAN:

        if (action.plan) {
          let selectedPlan = action.plan;
          let periodDays = selectedPlan.period * 30;
          let price = selectedPlan.prices.promo ? selectedPlan.prices.promo.discountBase : selectedPlan.prices.base;
          let savingPrice = selectedPlan.prices.promo ? selectedPlan.prices.base - selectedPlan.prices.promo.discountBase : 0;

          this.setDataLayer({
            productDiscountWithCurrency: priceWithCurrency(savingPrice, state.plans.currency),
            productId: state.userSession.country.id + '_' + periodDays, // productId: country id (in abawebapps) + period in days
            productPrice: price,
            productQuantity: 1,
            subscriptionPeriodDays: periodDays
          });
          this.triggerEvent('select_plan');
        }

        break;

      case CHECKOUT_CART_LOADED:

        let selectedPlan = undefined;
        let price = undefined;

        if (state.appState.shoppingCart && state.plans) {
          selectedPlan = findPlanById(state.appState.shoppingCart.plan, state.plans.items);
          price = selectedPlan.prices.promo ? selectedPlan.prices.promo.discountBase : selectedPlan.prices.base;
        }

        this.setDataLayer({
          currency: state.userSession.currency.iso,
          productPrice: price,
        });

        this.triggerEvent('checkout_cart_loaded');

        break;

      case PAYMENT_SUCCESS:

        let transactionData = action.transactionData;

        if (transactionData.period && transactionData.subscriptionId && transactionData.invoice) {
          let periodDays = transactionData.period * 30;
          let price = transactionData.invoice.finalPrice;
          let savingPrice = transactionData.invoice.discount ? Math.abs(transactionData.invoice.discount.discountPrice) : 0;

          this.setDataLayer({
            currency: state.userSession.currency.iso,
            paymentId: transactionData.subscriptionId,
            paymentMethodId: state.checkout.selectedMethod,
            productDiscountWithCurrency: priceWithCurrency(savingPrice, state.userSession.currency),
            productId: state.userSession.country.id + '_' + periodDays, // productId: country id (in abawebapps) + period in days
            productPrice: price,
            productQuantity: 1,
            subscriptionPeriodDays: periodDays
          });

          this.triggerEvent('payment_success');
        }

        break;
    }
  }
}
