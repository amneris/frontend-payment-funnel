import {
  RECEIVE_USER_SESSION, CLICK_SELECT_PLAN, PAYMENT_SUCCESS, EXIT_FUNNEL,
  VIEW_PLANS_PAGE, VIEW_CHECKOUT_PAGE, VIEW_CONFIRMATION_PAGE, CHECKOUT_CART_LOADED
} from '../actions/actionTypes';

import { findPlanById } from '../actions/utils';

export default class GA {

  constructor(trackerSubscriber) {
    this.trackerSubscriber = trackerSubscriber;
    this.language = '';
    this.name = 'Google Analytics';
    this.userId = 0;
  }

  setId(id) {
    this.id = id;
  }

  setLanguage(language) {
    this.language = language;
  }

  setCookieDomain(cookieDomain) {
    this.cookieDomain = cookieDomain;
  }

  getInjectableCode() {
    var script = `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');`;
    var init = `ga('create', '${this.id}', '${this.cookieDomain}');`;
    var pageView = `ga('send', 'pageview');`;
    var setLanguage = `window.ga('set', '&ul', '${this.language}');`;

    return script + init + pageView + setLanguage;
  }

  injectIntoHead() {
    this.trackerSubscriber.injectIntoHead(this.getInjectableCode(), this.name);
  }

  sendParams(params) {
    this.userId = parseInt(params.userId);

    if (params.hasOwnProperty('userId')) {
      window.ga('set', '&uid', this.userId);
    }
  }

  sendSelectPlanEvent(label) {
    if (label) { // only send the event if the label is filled (so the plan is valid)
      window.ga('send', 'event', 'Plans', 'Select', label);
    }
  }

  sendViewCheckoutEvent(label = 'error') {
    // send the event even when there is no valid plan, then we send "error"
    window.ga('send', 'event', 'Checkout', 'View', label);
  }

  sendExitEvent() {
    window.ga('send', 'event', 'Plans', 'Exit', 'Logo');
  }

  sendPageView() {
    // As a SPA, page view events have to be sent manually
    // MORE INFO: https://developers.google.com/analytics/devguides/collection/analyticsjs/single-page-applications?hl=es
    window.ga('set', 'page', window.location.pathname);
    window.ga('send', 'pageview');
  }

  getPlanLabel(plan, userCountryIso) {
    let label = undefined;

    if (plan) {
      label = userCountryIso.toUpperCase() + '_' + plan.period * 30;
    }

    return label;
  }

  handleEvent(action, state) {

    switch (action.type) {

      case RECEIVE_USER_SESSION:

        const { userSession } = action;

        if (userSession) {
          this.sendParams({
            userId: userSession.id
          });
        }

        break;

      case CLICK_SELECT_PLAN:
        const { plan } = action;

        this.sendSelectPlanEvent(this.getPlanLabel(plan, state.userSession.country.iso));

        break;

      case CHECKOUT_CART_LOADED:

        let selectedPlan = undefined;

        if (state.appState.shoppingCart && state.plans) {
          selectedPlan = findPlanById(state.appState.shoppingCart.plan, state.plans.items);
        }

        this.sendViewCheckoutEvent(this.getPlanLabel(selectedPlan, state.userSession.country.iso));

        break;

      case PAYMENT_SUCCESS:

        let transactionData = action.transactionData;

        if (transactionData.period && transactionData.subscriptionId && transactionData.invoice) {

          let periodDays = transactionData.period * 30;
          let price = transactionData.invoice.finalPrice;
          let productNames = {
            1: 'Monthly',
            6: 'Six-monthly',
            12: 'Annual',
            24: 'Two years'
          };

          ga('require', 'ecommerce');

          ga('ecommerce:addTransaction', {
            id: transactionData.subscriptionId,
            affiliation: '', // TODO: send the parnter id of the campaign, now in abawebapps this data is wrong in payments table
            revenue: price,
            currency: state.userSession.currency.iso,
            shipping: 0,
            tax: 0
          });

          ga('ecommerce:addItem', {
            id: transactionData.subscriptionId,
            name: productNames[transactionData.period],
            sku: state.userSession.country.id + '_' + periodDays, // productId: country id (in abawebapps) + period in days
            category: state.appState.segmentId || '',
            price: price,
            currency: state.userSession.currency.iso,
            quantity: 1
          });

          ga('ecommerce:send');
        }

        break;

      case EXIT_FUNNEL:
        this.sendExitEvent();

        break;

      case VIEW_PLANS_PAGE:
      case VIEW_CHECKOUT_PAGE:
      case VIEW_CONFIRMATION_PAGE:

        this.sendPageView();

        break;
    }
  }
}
