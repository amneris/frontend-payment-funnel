import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';

import LocaleManager from './i18n/localeManager';
import configureStore from './store/configureStore';
import AppRoot from './containers/appRoot';
import AppConfig from './config/appConfig';
import { appStateInit } from './reducers/appState';


const localeManager = new LocaleManager();
const store = configureStore();
const appRoot = (<AppRoot store={store} history={browserHistory} localeManager={localeManager}/>);
const target = document.getElementById('content');

const shouldPersistData = AppConfig.getShouldPersistData();

if (shouldPersistData) {
  const persistor = persistStore(store, {}, () => {
    // set the initial state
    // to avoid not matching server rendered html, we need to reset what server is also rendering
    persistor.rehydrate( { appState: appStateInit });

    // dispatch first render
    render(appRoot, target);
  });
}
else {
  render(appRoot, target);
}

// add redux dev tools if app is in development mode
if (process.env.NODE_ENV === 'development' && !window.devToolsExtension) {
  // Enable Redux dev tools in DEBUG mode
  const DevToolsView = require('./containers/reduxDevTools').default;
  const devNode = (
    <Provider store={store}>
      <DevToolsView/>
    </Provider>
  );
  const devTarget = document.createElement('div');
  target.parentNode.insertBefore(devTarget, target.nextSibling);
  render(devNode, devTarget);
};
