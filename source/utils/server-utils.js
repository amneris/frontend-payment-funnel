import React from 'react';

/**
 * Created by kbarz on 03/08/2016.
 */
export function createPage(content, language, trackerSubscriber, appConfig) {

  trackerSubscriber.newRelic.setId(appConfig['NEW_RELIC_APP_ID']);
  trackerSubscriber.gtm.setId(appConfig['GOOGLE_TAG_MANAGER_ID']);
  trackerSubscriber.ga.setId(appConfig['GOOGLE_ANALYTICS_ID']);
  trackerSubscriber.ga.setLanguage(language);
  trackerSubscriber.ga.setCookieDomain(appConfig['GOOGLE_ANALYTICS_COOKIE_DOMAIN']);
  trackerSubscriber.cooladata.setKey(appConfig['COOLADATA_APP_KEY']);

  return `
    <!DOCTYPE html>
    <html>
    <head>
      <!-- application version: %%GULP_INJECT_VERSION%% -->
      <meta charset="utf-8">
      <meta name="language" content="${language}">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <meta name="robots" content="noindex, nofollow, noarchive">
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Expires" content="0" />
      <title>ABA English</title>
      <link rel="icon" href="/assets/img/favicon.ico">

      <script type="text/javascript">
        var appConfig = ${JSON.stringify(appConfig)}
      </script>

      <script type="text/javascript" src="/assets/scripts/config.min.js"/></script>

      <!-- Start: New Relic -->
      <script type="text/javascript">
        ${trackerSubscriber.newRelic.getInjectableCode()}
      </script>
      <!-- End: New Relic -->

      <!-- Start: Google Analytics -->
      <script type="text/javascript">
        ${trackerSubscriber.ga.getInjectableCode()}
      </script>
      <!-- End: Google Analytics -->

      <!-- Start: CoolaData -->
      <script type="text/javascript">
        ${trackerSubscriber.cooladata.getInjectableCode()}
      </script>
      <!-- End: CoolaData -->

      <link rel="stylesheet" href="/assets/stylesheets/vendors.css">
      <link rel="stylesheet" href="/assets/stylesheets/app.css">

      <!-- Zuora Public javascript library -->
      <script type="text/javascript" src="https://static.zuora.com/Resources/libs/hosted/1.3.0/zuora-min.js"/></script>
      
      <!-- Start: Visual Web Optimizer -->
      <script type="text/javascript">
        ${trackerSubscriber.vwo.getInjectableCode()}
      </script>
      <!-- End: Visual Web Optimizer -->
      
      
    </head>
    <body class="app-background">
      <div id="content">${content}</div>
      <script src="/assets/scripts/vendor.min.js"></script>
      <script src="/assets/scripts/build.min.js"></script>

      <!-- Start: Google Tag Manager -->
      <script type="text/javascript">
        ${trackerSubscriber.gtm.getInjectableCode()}
      </script>
      <!-- End: Google Tag Manager -->      
      
    </body>
    </html>
  `;
}
