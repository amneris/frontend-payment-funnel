import { appendParamsToUrl } from '../actions/utils';

export function goToOldCheckout(AppConfig, oldPlanId, params) {

  // TODO: remove when using Zuora checkout all the time (when split test is over)

  // submit data to old campus
  var submitUrl = AppConfig.getCampusUrl() + '/payments/paymentmethod';
  submitUrl = appendParamsToUrl(submitUrl, params);

  var form = document.createElement('form');
  form.setAttribute('id', 'selectPlanForm');
  form.setAttribute('method', 'POST');
  form.setAttribute('action', submitUrl);

  var planKeyField = document.createElement('input');
  planKeyField.setAttribute('type', 'hidden');
  planKeyField.setAttribute('name', 'productPricePKSelected');
  planKeyField.setAttribute('value', oldPlanId);

  form.appendChild(planKeyField);

  document.body.appendChild(form);

  //console.debug("ABOUT TO SUBIT FORM")

  form.submit();
  document.body.removeChild(form);
}
