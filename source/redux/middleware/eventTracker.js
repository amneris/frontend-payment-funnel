/**
 *
 * @param trackerSubscriber class that trackers (observers) subscribe to
 * @returns {function(*=): function(*): function(*=)}
 */
function eventTracker(trackerSubscriber) {

  // the sequence of functions below (store, next, and action) are curried
  // using es6 arrow funcions to make it easier on the eye.
  // see http://redux.js.org/docs/advanced/Middleware.html for more info
  const eventTrackerChain = store => next => action => {

    let result = next(action);

    // send action to tracker subscriber
    trackerSubscriber.emitEvent(action, store.getState());

    return result;
  }

  return eventTrackerChain;
}

export default eventTracker;
