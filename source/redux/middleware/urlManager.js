import { browserHistory } from 'react-router';
import QueryString from 'query-string';

import { PAGES_ROUTES } from '../../routes';
import AppConfig from '../../config/appConfig';
import * as actionTypes from '../../actions/actionTypes';
import { appendParamsToUrl } from '../../actions/utils';
import { goToOldCheckout } from '../../splitTests/oldAndNewCheckout';

const urlManager = store => next => action => {

  const state = store.getState();

  switch (action.type) {
    case actionTypes.GO_TO_CAMPUS:
    case actionTypes.EXIT_FUNNEL:
      goToCampus();
      break;

    case actionTypes.FORBID_ACCESS:
      goToCampusLogin();
      break;

    case actionTypes.GO_TO_COURSE:
      goToCourse(state);
      break;

    case actionTypes.GO_TO_CHECKOUT:
      SplitTestCheckoutPage(action, state);

      break;

    case actionTypes.GO_TO_PLANS:
      goToPlans(state);
      break;

    case actionTypes.GO_TO_CONFIRMATION:
      goToConfirmation(action, state);
      break;

    case actionTypes.SELECT_METHOD:
      addMethodToUrl(action.methodId, state);
      break;

    case actionTypes.RECEIVE_TRANSACTION_INFO:
      changeConfirmationPageParams(action.transactionInfo, state);
      break;
  }

  return next(action);
}

function SplitTestCheckoutPage(action, state){
  //TODO: remove this split test when it's over
  // property oc of state.appState.urlParam is placehoder for whether 'old checkout' should be used instead of new one
  // oc isn't a great property name but it looks less clumsy when displayed as part of the url in the user's browser
  if (state.appState.urlParams.oc &&  state.appState.urlParams.oc==="1"){
    //console.debug("going to old checkout")
    goToOldCheckout(AppConfig, action.plan.idProduct, state.appState.urlParams)
  }else{
    //console.debug("going to new checkout")
    let planId = action.plan ? action.plan.id : undefined;

    goToCheckout(planId, state);
  }
}

function goToCampus() {
  window.location = AppConfig.getCampusUrl();
}

function goToCampusLogin() {
  let url = AppConfig.getCampusUrl() + '/login';
  let params = {
    'returnTo' : encodeURIComponent(window.location.href)
  };

  if (Object.keys(params).length > 0) url += '?' + QueryString.stringify(params);

  window.location = url;
}

function goToCourse(state) {
  window.location = AppConfig.getCampusUrl() + '/' + state.appState.language + '/course/index';
}

function goToCheckout(planId, state) {
  let uri = '/' + state.appState.language + PAGES_ROUTES.CHECKOUT_PAGE;
  let params = state.appState.urlParams;

  if (planId) params.plan = planId;

  if (params.token) delete params.token;
  if (params.gatewayName) delete params.gatewayName;
  if (params.methodName) delete params.methodName;
  if (params.transactionId) delete params.transactionId;

  if (Object.keys(params).length > 0) uri += '?' + QueryString.stringify(params);

  browserHistory.push(uri);
}

function goToPlans(state) {
  let uri = '/' + state.appState.language + PAGES_ROUTES.PLANS_PAGE;
  let params = state.appState.urlParams;

  if (params.plan) delete params.plan;
  if (params.transactionId) delete params.transactionId;

  if (Object.keys(params).length > 0) uri += '?' + QueryString.stringify(params);

  browserHistory.push(uri);
}

function goToConfirmation(paramsToAppend, state) {
  let url = '/' + state.appState.language + PAGES_ROUTES.CONFIRMATION_PAGE;
  let params = state.appState.urlParams;
  let shoppingCart = state.appState.shoppingCart;

  if (paramsToAppend.token) params.token = paramsToAppend.token;
  if (paramsToAppend.gatewayName) params.gatewayName = paramsToAppend.gatewayName;
  if (paramsToAppend.methodName) params.methodName = paramsToAppend.methodName;

  if (shoppingCart) {
    if (shoppingCart.period) params.period = shoppingCart.period;
    if (shoppingCart.planTitleKey) params.planTitleKey = shoppingCart.planTitleKey;
    if (shoppingCart.discount) params.discount = shoppingCart.discount;
    if (shoppingCart.plan) params.plan = shoppingCart.plan;
  }

  if (Object.keys(params).length > 0) {
    url = appendParamsToUrl(url, params);
  }

  browserHistory.push(url);
}

function addMethodToUrl(methodId, state) {
  let params = state.appState.urlParams;
  params.method = methodId;

  let url = window.location.pathname;
  url += '?' + QueryString.stringify(params);

  browserHistory.replace(url);
}

function changeConfirmationPageParams(transactionInfo, state) {
  // changing url params
  let params = state.appState.urlParams;
  let url = window.location.pathname;

  if (params.token) delete params.token;
  if (params.gatewayName) delete params.gatewayName;
  if (params.methodName) delete params.methodName;

  if (transactionInfo && transactionInfo.id) {
    params.transactionId = transactionInfo.id;
  }

  url += '?' + QueryString.stringify(params);

  browserHistory.replace(url);
}

export default urlManager;
