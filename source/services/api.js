require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch';

export function checkStatus(response) {

  let status = response.status;

  switch (true) {
    case (status >= 200 && status < 300):
      return response;
    default:
      var error = new Error(response.statusText);
      error.status = status;
      error.response = response;
      throw error;
  }
}

/**
 *
 * @param url
 * @param method (get or post)
 * @param params (when method is post)
 * @param includeCredentials (true when api is abawebapps)
 * @returns {Promise.<T>}
 */
export function apiRequest(url, method = 'get', params = {}, includeCredentials = false) {

  var fetchParams = {
    mode: 'cors',
    cache: 'no-cache',
    method: method,
  };

  if (method === 'post') {
    fetchParams = Object.assign(fetchParams, {
      headers: { 'Content-Type' : 'application/json;charset=UTF-8' },
      body: JSON.stringify(params)
    });
  }

  if (includeCredentials === true) {
    fetchParams = Object.assign(fetchParams, {
      credentials: 'include'
    });
  }

  return fetch(url, fetchParams)
    .then(checkStatus)
    .then(apiResponse => apiResponse.json());
}

export function staticGet(url) {

  var fetchParams = {
    mode: 'cors',
    cache: 'default'
  };

  return fetch(url, fetchParams)
    .then(checkStatus)
    .then(response => response.text());
}
