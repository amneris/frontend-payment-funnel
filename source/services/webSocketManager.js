import socketIO from 'socket.io-client';
import QueryString from 'query-string';
import AppConfig from '../config/appConfig';
import * as ErrorTypes from '../actions/errorTypes';

let instance = null;

export default class WebSocketManager {
  constructor() {

    if (!instance) {
      instance = this;
      instance.socket = null;
    }

    return instance;
  }

  disconnect(){
    // About to try to disconnect client
    if (this.socket) {
      this.socket.disconnect();
    }
  }

  connect(transactionId, eventReceivedCallback, disconnectCallback, errorCallback) {

    if (this.socket != null && this.socket.connected){
      //Client already has connected socket so exit
      return;
    }else{
      // Client has no socket open
    }

    let params = {
      transactionId: transactionId
    };

    this.socket = socketIO.connect(AppConfig.getWebSocketsUrl(), {
      query: QueryString.stringify(params),
      'multiplex': false,
      reconnection: false
    });

    let self = this;

    this.socket.on('connect', function (msg) {
      // Connected to socket
      //let socketid = self.socket.io.engine.id;
      console.debug("---------- WEBSOCKET CONNECTED -------------")
    });

    this.socket.on('disconnect', function (reason) {

      console.debug("---------- WEBSOCKET DISCONNECTED -------------")

      // websocket has disconnected

      if (reason === ErrorTypes.WEBSOCKET_SERVER_UNEXPECTED_DISCONNECT){
        errorCallback("websocket has disconnected unexpectedly");
      }else{
        disconnectCallback()
      }
    });

    this.socket.on('error', function (errorData) {
      console.debug("websocket connection has errored");
      errorCallback(errorData);
    });

    this.socket.on('mq_event_match', function (msg) {
      // Message recieved
      // possible states: PAYPAL_VERIFIED && ZUORA_CONFIRMED && ABA_CONFIRMED, ERROR
      try{
        let jsonMessage = JSON.parse(msg);
        eventReceivedCallback(jsonMessage);
      }catch(Exception){
        errorCallback("error occured after receiving event from websocket server: " + Exception.toString());
      }

    })
  }
}
