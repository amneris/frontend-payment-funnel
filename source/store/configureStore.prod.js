import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';
import urlManager from '../redux/middleware/urlManager';
import eventTracker from '../redux/middleware/eventTracker';
import TrackerSubscriber from '../tracker/trackerSubscriber';

export default function configureStore(initialState) {

  const trackerSubscriber = new TrackerSubscriber();

  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunkMiddleware, urlManager, eventTracker(trackerSubscriber))
  );
}
