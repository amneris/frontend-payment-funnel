import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
//import { autoRehydrate } from 'redux-persist';

import rootReducer from '../reducers';
import DevTools from '../containers/reduxDevTools';
import urlManager from '../redux/middleware/urlManager';
import eventTracker from '../redux/middleware/eventTracker'
import TrackerSubscriber from '../tracker/trackerSubscriber';


export default function configureStore(initialState) {

  const trackerSubscriber = new TrackerSubscriber();

  let middleware = [thunkMiddleware, urlManager, eventTracker(trackerSubscriber)];

  if (typeof(window) !== 'undefined') {
    middleware = [ ...middleware, createLogger() ]
  }

  return createStore(
    rootReducer,
    initialState, // initial state only works this way when not using store persist
    compose(
      //autoRehydrate(), // to sync the local storage with the app state
      applyMiddleware(...middleware),
      DevTools.instrument())
  );
}
