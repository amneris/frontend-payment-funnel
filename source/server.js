var fs = require('fs');
var actuator = require('express-actuator');
const rootPath = process.env.ROOT_PATH || '';
const appConfig = JSON.parse(fs.readFileSync(path.join(rootPath, 'config.json'), 'utf8'));

if (appConfig.NEW_RELIC_ENABLED) {
  require('newrelic');
}


import React from 'react';
import express from 'express';
import path from 'path';
import { renderToString } from 'react-dom/server';
import TrackerSubscriber from './tracker/trackerSubscriber';
import LocaleManager from './i18n/localeManager';
import Provider from './containers/provider';
import App from './containers/app';
import configureStore from './store/configureStore';
import { appStateInit } from './reducers/appState';
import { getDefaultLanguage, getLanguageFromUrl } from './reducers/utils';
import { createPage } from './utils/server-utils'

var compression = require('compression');

const app = express();
app.use(actuator('/management'));
app.use(compression());
const port = process.env.PORT || 3000;

const trackerSubscriber = new TrackerSubscriber();
const localeManager = new LocaleManager();

let language = getDefaultLanguage();

app.use('/assets', express.static(path.join(rootPath, 'assets')));

// This is fired every time the server side receives a request
app.get('*', loadApp);

app.listen(port, function () {
  console.log('Payment Funnel Webapp listening on port ' + port);
});

function loadApp(req, res) {

  language = getLanguageFromUrl(req.url);

  const store = configureStore({
    appState: Object.assign({}, appStateInit, { language })
  });

  let renderedApp = renderToString(
    <Provider store={store} trackerSubscriber={trackerSubscriber} localeManager={localeManager}>
      <App />
    </Provider>
  )

  res.send(createPage(renderedApp, language, trackerSubscriber, appConfig));
}
