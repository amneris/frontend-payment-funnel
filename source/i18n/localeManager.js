import { getDefaultLocale } from '../reducers/utils';

const pageTitles = {
  PlansPage: 'Plans page',
  CheckoutPage: 'Checkout page',
  ConfirmationPage: 'Confirmation page'
};

/**
 * class responsible for getting application strings according to user's locale
 */
export default class LocaleManager {
  constructor(locale) {

    this.messages = [];

    if (!locale) {
      locale = getDefaultLocale();
    }

    if (locale) {
      this.setMessages(locale)
    };
  }

  /**
   * method that converts a locales messages specified in PO to the format that
   * the FormatJS library expects
   * @param locale
   */
  setMessages(locale) {

    var localeMessages = require('./phrases/' + locale + '.json');

    this.messages = localeMessages;
  }

  /**
   * returns a message for a given key
   * @param key, values
   * @returns {message for key, or key if value not found}
   */
  getMessage(key, values, defaultValue) {
    if (this.messages[key] === undefined) {
      return defaultValue == undefined ?  key :defaultValue;
    }
    else if (values !== undefined) {
      var message = this.messages[key];
      for (var prop in values) {
        if (values.hasOwnProperty(prop)) {
          message = message.replace('{' + prop + '}', values[prop]);
        }
      }

      return message;
    }

    return this.messages[key];
  }

  getTranslation(dataset) {
    let key = dataset.translate;
    delete(dataset.translate);

    let message = this.messages[key];

    if (!message) {
      return key;
    }

    // search the markup pattern {{something}} inside the message to apply different cases
    // case 1: {{var:variable name}}
    // case 2: {{link:key to be translated}}
    message = message.replace(/{{(.*?)}}/g, (str, p1) => {
      let stringParts = p1.split(':');

      if (stringParts.length == 2) {
        let type = stringParts[0];
        let value = stringParts[1];
        let content = '';

        switch (type) {
          case 'var':
            // the output will be the value in the dataset that matches the message key
            // example: {{var:days}} requires an input of <span data-days={value}></span>
            content = dataset[value];
            break;

          case 'link':
            // the output will be the translated value inside the pattern {{link:key to be translated}}
            content = this.messages[value];
            break;

        }

        return `<span data-${type}="${value}">${content}</span>`;
      }
    });

    return message;
  }

  // receives a css selector and loop to translate all the elements that has an attribute data-translate
  translateContainer(selector) {
    let elements = document.querySelectorAll(`${selector} [data-translate]`);

    for (let element of elements) {
      element.innerHTML = this.getTranslation(element.dataset);
    }
  }
}
