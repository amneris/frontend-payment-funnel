import React from 'react';
import {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Scroll  from 'react-scroll';
import AppConfig from '../config/appConfig';
import {
  callActionGoToPlans, callActionViewConfirmationPage,
  callActionSelectMethod, callActionSetAppState, callActionGoToCheckout,
  callActionTrackPaymentSuccess, callActionConvertUserToPremium
} from '../actions/synch';
import {
  callActionStartTransaction,
  callActionFetchTransactionInfo,
  callActionSetTransactionError
} from '../actions/asynch';
import WebSocketManager from '../services/webSocketManager';
import * as ErrorTypes from '../actions/errorTypes';

import Error from '../components/states/error';

import SubscriptionStatus from '../components/confirmationPage/subscriptionStatus/subscriptionStatus';
import DownloadApp from '../components/confirmationPage/downloadApp/downloadApp';
import SocialNetwork from '../components/confirmationPage/socialNetwork/socialNetwork';
import PaymentSummary from '../components/confirmationPage/summary/paymentSummary';

class ConfirmationPage extends Component {

  constructor(props) {
    super(props);

    // because using bind creates a new function reference, remove event listener fails unless we create
    // a variable that contains these bind functions
    this.onResize = this.handleResize.bind(this);
    this.onScroll = this.reCalculateStickyPosition.bind(this);
    this.webSocketManager = new WebSocketManager();

    this.state = {
      isMobileDevice: this.checkIfIsMobileDevice(),
      status: undefined,
      executingFetchTransactionInfoWithRetries : false,
      isPaymentSuccessTracked : false,
      timerFetchTransactionInfo : undefined
    };
  }

  resetState() {
    this.setState({status: undefined});
  }

  componentWillMount() {

    Scroll.animateScroll.scrollToTop();

    if (this.props.userIsPremium || this._haveTransactionInfoInUrl()) {
      //have all transaction info necessary in url
      // action to track confirmation page traffic
      this.props.callActionViewConfirmationPage();

      // change page title
      this.props.callActionSetAppState({
        title: this.context.localeManager.getMessage('title plans page'),
        subtitle: ''
      });
    } else {

      //console.debug("transaction info missing");
      this.props.callActionSetTransactionError(ErrorTypes.TRANSACTION_URL_PARAMS_MISSING)
    }
  }

  componentDidMount() {

    if (this.props.selectedPlan && this.props.urlParams.token) {
      this._startTransaction();
    }

    if (this.props.urlParams.transactionId) {
      this.props.callActionFetchTransactionInfo(this.props.urlParams.transactionId);
    }

    if (!this.props.selectedMethodId && this.props.urlParams.hasOwnProperty('method')) {
      this.props.callActionSelectMethod(parseInt(this.props.urlParams['method']));
    }

    this.reCalculateStickyPosition();

    window.addEventListener('resize', this.onResize);
    window.addEventListener('scroll', this.onScroll);
  }

  /***
   * method that will fetch transaction info every x seconds. this acts as a backup in case there is a problem
   * with the websocket server and it doesn't return any transaction info
   * @private
   */
  _makeFetchTransactionInfoWithRetries() {

    //console.debug('Recursivly call action to fetch transaction info');
    var self = this;
    return function () {

      if (!self.state.executingFetchTransactionInfoWithRetries) {

        self.setState({executingFetchTransactionInfoWithRetries: true});// set true so that we only enter recursive loop onces
        var numberOfRetriesMade = 0;
        var retryInterval = AppConfig.getTransactionInfoRetryInterval();
        //console.debug('Retry interval for fetching transaction info : ' + retryInterval);
        self.setState({timerFetchTransactionInfo: setTimeout(fetchTransactionInfo, retryInterval)});  // call fetchTransactionInfo after a specified number of milliseconds
        function fetchTransactionInfo() {
          //console.debug('Calling action to fetch transaction info. Retry number  #' + numberOfRetriesMade + 1);
          let maxRetries = AppConfig.getTransactionInfoMaxRetries();
          //console.debug("Max retries allowed : " + maxRetries);

          if (numberOfRetriesMade < maxRetries && self.props.transactionError==undefined && !self.props.userIsPremium) {
            //console.debug('BANG with trans id ' + self.props.urlParams.transactionId);
            self.props.callActionFetchTransactionInfo(self.props.urlParams.transactionId);
            self.setState({timerFetchTransactionInfo: setTimeout(fetchTransactionInfo, retryInterval)});
            numberOfRetriesMade++;
          } else {
            //console.debug('Max retries reached or payment process is complete.')
            //console.debug('Number of retries made : ' + numberOfRetriesMade + '. Payment complete ? ' + self._transactionHasInvoice(self.props));
          }
        }
      }
    }
  }


  _transactionHasInvoice(props){

    if (props.transactionInfo && props.transactionInfo.invoice){
      return true;
    }

    return false;
  }

  componentWillReceiveProps(nextProps) {

    if (this.isUserSubscriptionInProgress(nextProps)) {


      this.webSocketManager.connect(
        nextProps.transactionId,
        ::this.websocketEventReceived,
        ::this.websocketDisconnected,
        ::this.websocketError);

      // as a backup in case websockets don't work, recursivly call method for getting transacion info
      var recursivlyCallActionFetchTransactionInfo = this._makeFetchTransactionInfoWithRetries();
      recursivlyCallActionFetchTransactionInfo();
    }else{
      this.stopGettingTransactionInfo();
    }

    if (nextProps.transactionError) {
      this.stopGettingTransactionInfo()
    }

    if (this._transactionHasInvoice(nextProps) && !this.state.isPaymentSuccessTracked){
      //console.debug('Track payment success : ' + JSON.stringify(nextProps.transactionInfo));
      this.props.callActionTrackPaymentSuccess(nextProps.transactionInfo);
      this.setState({isPaymentSuccessTracked: true});
    }else{
      //console.debug('payment is not a success or has already been tracked. Payment tracked? : ' + this.state.isPaymentSuccessTracked + '. payment success : ' + this._transactionHasInvoice(nextProps))
    }
  }

  stopGettingTransactionInfo(){
    //console.debug('Stopping all future requests for getting transaction info');
    this.webSocketManager.disconnect();
    this.stopTimer();
  }

  stopTimer(){
    // stop retries
    var timerFetchTransactionInfo = this.state.timerFetchTransactionInfo;
    if (timerFetchTransactionInfo){
      clearTimeout(timerFetchTransactionInfo)
    }
  }

  websocketEventReceived(payload) {

    switch (payload.routingKey) {
      case 'subscription-service.transaction-zuora-confirmed':
        // fetch transaction info to get subscription id and invoice
        // after that we track the payment event if we receive the proper data
        //console.debug('Event ZUORA_CONFIRMED received from websocket server')
        this.props.callActionFetchTransactionInfo(this.props.transactionId)
        break;

      case 'subscription-service.transaction-aba-confirmed':
        // user is premium
        this.props.callActionConvertUserToPremium(); // update state of user session to say that user is of type premium
        this.stopGettingTransactionInfo();

        break;

      default:
        let errorPattern = new RegExp('\.(deadletters)$');

        if (errorPattern.test(payload.routingKey)) {
          let subscriptionId = this.props.transactionInfo ? this.props.transactionInfo.subscriptionId : undefined;

          // UX feat: we will not change ui upon arrival of errors that occure after we have a subscription id.
          // if we have a subscription id then we will already have shown the user their subscription id and the payment
          // will have been processed (although the user may not yet been converted to premium in abawebapps. however.
          // if we have no subscription id, the payment most likely failed and so we should show an error message
          if (!subscriptionId) {
            this.props.callActionSetTransactionError(ErrorTypes.TRANSACTION_EVENT_QUEUE);
          }

          this.stopGettingTransactionInfo()
        }

        break;
    }
  }

  /**
   * method to deal with case when websocket is disconnected (remember, this could be fired in the case when the server falls-over)
   */
  websocketDisconnected() {
    // Websocket disconnected
    //todo stop retries
    //console.debug('Websocket disconnected');
  }

  /**
   * fired upon connection error
   * @param errorData
   */
  websocketError(errorData) {
    //console.error('Websocket error : ' + errorData)
    this.props.callActionSetTransactionError(ErrorTypes.WEBSOCKET_CONNECTION);
  }

  componentWillUnmount() {
    this.resetState();

    window.removeEventListener('resize', this.onResize);
    window.removeEventListener('scroll', this.onScroll);
  }

  _haveTransactionInfoInUrl() {
    // user can access this page when:
    // 1 - is premium
    // 2 - is free and have token in url
    // 3 - is free and have transactionId in url

    if (this._haveUrlParametersForStartingTransaction(this.props.urlParams)
      || this._haveUrlParametersForGettingTransactionInfo(this.props.urlParams)) {

      return true;
    }

    return false;
  }

  _haveUrlParametersForStartingTransaction(urlParams) {

    if (urlParams.hasOwnProperty('token')
      && urlParams.hasOwnProperty('plan')
      && urlParams.hasOwnProperty('period')
      && urlParams.hasOwnProperty('methodName')
      && urlParams.hasOwnProperty('gatewayName')) {
      return true;
    }

    return false;

  }

  _haveUrlParametersForDisplayingPaymentSummary(urlParams) {

    if (urlParams.hasOwnProperty('planTitleKey')
      && urlParams.hasOwnProperty('period')) {
      return true;
    }

    return false;

  }

  _haveUrlParametersForGettingTransactionInfo(urlParams) {

    if (urlParams.hasOwnProperty('transactionId')) {
      return true;
    }

    return false;

  }

  _startTransaction() {

    this.props.callActionStartTransaction(
      decodeURIComponent(this.props.urlParams.gatewayName),
      decodeURIComponent(this.props.urlParams.methodName),
      this.props.selectedPlan,
      this.props.urlParams.token
    );
  }

  isUserSubscriptionInProgress(nextProps) {

    return !nextProps.userIsPremium
      && nextProps.transactionInfo
      && nextProps.transactionId
      && nextProps.transactionState !== 'CLOSED';
  }

  selectPlan() {
    const planId = this.props.urlParams.plan;
    this.props.callActionSelectPlan(planId);
  }

  checkIfIsMobileDevice() {
    return window.innerWidth < 992;
  }

  handleResize(e) {
    this.reCalculateStickyPosition();

    this.setState({
      isMobileDevice: this.checkIfIsMobileDevice()
    });
  }

  reCalculateStickyPosition() {
    let tricky = document.getElementById('sticky-tricky');

    if (this.checkIfIsMobileDevice()) {
      let wrapper = document.getElementById('sticky-wrapper');

      if (wrapper && tricky) {
        tricky.style.paddingBottom = wrapper.offsetHeight + 'px';
        wrapper.style.position = 'absolute';

        let wrapperOverflow =
          wrapper.offsetTop + wrapper.offsetHeight - document.body.scrollTop + wrapper.offsetHeight;

        if (wrapperOverflow > window.innerHeight) {
          wrapper.style.position = 'fixed';
        }
      }
    }
    else if (!this.checkIfIsMobileDevice() && tricky) {
      tricky.style.paddingBottom = '0px';
    }
  }

  /**
   * checks that plan has all available information (at the moment, this information comes from two sources, one beiung
   * the url params and the other being the result of calling subscription-service getTransactionInfo
   * @returns {boolean}
   * @private
   */
  _haveAllPlanInformation() {

    if (this._haveUrlParametersForDisplayingPaymentSummary(this.props.urlParams) && this.props.selectedPlan && this.props.selectedPlan.prices.base) {
      return true;
    }

    return false;
  }

  render() {

    return (
      <div className="page-container confirmation-container">
        <div className="page-inner-container">

          {(() => {

            let subscriptionId = this.props.transactionInfo ? this.props.transactionInfo.subscriptionId : undefined;

            let output = (
              <div>

                <div id="sticky-tricky">

                  {(() => {

                    if (this.props.transactionError && !this.props.userIsPremium) {

                      let retryFunction = this.props.transactionError == ErrorTypes.TRANSACTION_URL_PARAMS_MISSING
                        ? this.props.callActionGoToPlans : this.props.callActionGoToCheckout;

                      return <Error retryFunction={retryFunction}/>
                    } else {

                      return (
                        <div className="confirmation-status-container">
                          <SubscriptionStatus subscriptionId={subscriptionId} userIsPremium={this.props.userIsPremium}/>
                        </div>
                      );
                    }

                  })()}

                  {(() => {
                    if (this._haveAllPlanInformation()) {

                      return (
                        <div>
                          <PaymentSummary plan={this.props.selectedPlan}
                                          renewalDate={this.props.transactionInfo.dueDate}
                                          isMobileDevice={this.state.isMobileDevice}/>

                          {(() => {

                            if (this.state.isMobileDevice) {

                              // TODO: waiting react-sticky to use sticky bottom
                              // https://github.com/captivationsoftware/react-sticky/commit/ff37020d0c1f79e918e164a126ca2e6a05b800bc#comments
                              // this should change to <Sticky position="bottom">
                              return (
                                <div id="sticky-wrapper">
                                  <DownloadApp isSticky={true}/>
                                </div>
                              );

                            }

                          })()}

                          <hr/>
                          <SocialNetwork />
                        </div>
                      );
                    }
                  })()}

                </div>

              </div>
            );

            // TODO: waiting react-sticky to use sticky bottom
            /*return this.state.isMobileDevice
             ? <StickyContainer>{output}</StickyContainer>
             : output;*/
            return output;

          })()}

        </div>
      </div>
    );
  }
}

ConfirmationPage.contextTypes = {
  localeManager: PropTypes.object
};

function mapStateToProps(state) {
  const {urlParams} = state.appState;
  const {type} = state.userSession;
  const {selectedMethod} = state.checkout;
  const {transactionInfo} = state.transaction;
  const {transactionError} = state.transaction;
  let transactionState;
  let transactionId;

  let selectedPlanItem = {
    id: urlParams.plan,
    period: parseInt(urlParams.period),
    titleKey: urlParams.planTitleKey,
    prices: {
      base: null,
      promo: null
    }
  };

  if (urlParams.discount) {
    selectedPlanItem.prices.promo = {
      discountId: urlParams.discount,
      discountBase: null
    }
  }

  // adding transaction info to selected plan object
  if (transactionInfo) {
    transactionId = transactionInfo.id;
    transactionState = transactionInfo.state;
    let invoice = transactionInfo.invoice;

    if (invoice) {
      if (invoice.discountPrice) {
        selectedPlanItem.prices.base = invoice.basePrice;
        selectedPlanItem.prices.promo.discountBase = invoice.finalPrice;
      }
      else {
        selectedPlanItem.prices.base = invoice.finalPrice;
      }
    }
  }

  return {
    urlParams: urlParams || {},
    selectedPlan: selectedPlanItem || undefined,
    selectedMethodId: selectedMethod || undefined,
    userIsPremium: type == 'premium',
    transactionIsFetching: state.transaction.isFetching,
    transactionInfo,
    transactionState,
    transactionId,
    transactionError
  };
}

export default connect(
  mapStateToProps,
  {
    callActionGoToPlans, callActionViewConfirmationPage,
    callActionSetAppState, callActionSetTransactionError, callActionTrackPaymentSuccess,
    callActionStartTransaction, callActionFetchTransactionInfo, callActionSelectMethod, callActionGoToCheckout,
    callActionConvertUserToPremium
  }
)(ConfirmationPage);
