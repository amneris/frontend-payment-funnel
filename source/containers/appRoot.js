import React from 'react';
import { Component, PropTypes } from 'react';
import routes from '../routes';
import { Router } from 'react-router';
import Provider from './provider';

export default class AppRoot extends Component {
  render() {
    const { store, history, trackerSubscriber, localeManager } = this.props;
    return (
      <Provider store={store} localeManager={localeManager}>
        <Router history={history} routes={routes}/>
      </Provider>
    );
  }
}

// define the properties that this component expects to receive
AppRoot.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  localeManager: PropTypes.object.isRequired
};
