require('array.prototype.find').shim();

import React from 'react';
import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Header from '../components/common/header';
import Footer from '../components/common/footer';
import Loading from '../components/states/loading';
import { getDefaultContactPhone, getDefaultLanguage, convertLanguageToLocale } from '../reducers/utils';
import { getCurrentUrl, getConfirmationPageUrl } from '../actions/utils';
import { callActionFetchUserSessionIfNeeded } from '../actions/asynch';
import { callActionSetAppState, callActionSetUrlParams, callActionGoToConfirmation } from '../actions/synch';

class App extends Component {

  componentWillReceiveProps(nextProps) {
    this._checkUserType(nextProps);
  }

  componentWillMount() {
    this._setAppLanguage();
  }

  componentDidMount() {
    this.props.callActionSetUrlParams(this.props.appState.urlParams);
    this.props.callActionFetchUserSessionIfNeeded(this.props.appState.urlParams);

    this._checkUserType(this.props);
  }

  _checkUserType(props) {
    if (props.userSession.type === 'premium' && getCurrentUrl() !== getConfirmationPageUrl()) {
      this.props.callActionGoToConfirmation();

      // TODO improve that kick to confirmation page because is tracking also view_plans_page or view_checkout_page before redirecting
    }
  }

  _setAppLanguage() {
    let language = this.props.params ? this.props.params.lang :
      this.props.appState.language ||
      getDefaultLanguage();

    let localeCountry = convertLanguageToLocale(language);

    this.props.callActionSetAppState({ language: language, startApp: false });

    this.context.localeManager.setMessages(localeCountry);
  }

  render() {

    return (
      <div className="app-container">

        <Header />

        {(() => {

          let { startApp } = this.props.appState;

          if (startApp === true) {
            // to avoid page flickering during first redirects
            document.body.className = 'app-background';

            return this.props.children;
          }
          else {
            return (
              <Loading />
            );
          }

        })()}

        <Footer phoneNumber={getDefaultContactPhone()}/>

      </div>

    );
  }
}

// define properties of app context that this component needs to access
App.contextTypes = {
  localeManager: PropTypes.object
};

function mapStateToProps(state, ownProps) {

  let urlParams = ownProps.location ? ownProps.location.query : undefined;
  let segmentId = ownProps.location ? ownProps.location.query.segm_id : undefined;

  let currentAppState = state.appState || {
      language: null,
      urlParams: null,
      startApp: false,
      title: null,
      selectedPlan: null,
      segmentId: null
    };

  Object.assign(currentAppState, {urlParams: urlParams}, {segmentId: segmentId});

  return {
    appState: currentAppState,
    userSession : state.userSession || {
      fetchResult: null,
      id: null,
      language: null,
      type: null,
      userLocale: null
    }
  };
}

export default connect(
  mapStateToProps,
  { callActionSetAppState, callActionSetUrlParams,
    callActionFetchUserSessionIfNeeded, callActionGoToConfirmation
  }
)(App);
