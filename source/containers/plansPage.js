import React from 'react';
import {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import 'date-utils';
import PlansList from '../components/plansPage/plansList/plansList';
import FeaturesList from '../components/plansPage/featuresList/featuresList';
import Awards from '../components/plansPage/awards/awards';
import ReviewsList from '../components/plansPage/reviewsList/reviewsList';
import Loading from '../components/states/loading';
import Error from '../components/states/error';
import {callActionFetchPlansIfNeeded} from '../actions/asynch';
import {callActionViewPlansPage, callActionSetAppState} from '../actions/synch';

class PlansPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      plansListState: 'loading',
      plansListData: null,
      autoScroll: null
    };
  }

  componentWillMount() {
    this.props.callActionSetAppState({
      title: this.context.localeManager.getMessage('title plans page'),
      subtitle: this.context.localeManager.getMessage('subtitle plans page')
    });

    this.props.callActionViewPlansPage();
  }

  componentDidMount() {
    let urlParams = this.props.appState.urlParams;

    if (urlParams && urlParams.show) {
      this.setState({ autoScroll: urlParams.show });
    }

    this.props.callActionFetchPlansIfNeeded();
  }

  promoExists(plans) {

    let numberOfPlans = plans.items ? plans.items.length : 0;

    for (var i = 0; i < numberOfPlans; i++) {

      let obj = plans.items[i].prices;

      if (obj.promo != undefined) {
        return true;
      }
    }
    return false;
  }

  render() {

    let showSpecialOfferWelcome = this.promoExists(this.props.plans);

    var tomorrow = Date.tomorrow().toFormat('DD-MM-YYYY');

    return (
      <div className="page-container">

        <div className="plans-list-container">
          <div className="section-title">{this.context.localeManager.getMessage('title page')}</div>

          <div className={"hidden-xs label-welcome-offer " + `panel-show-${showSpecialOfferWelcome}`}>
            {this.context.localeManager.getMessage('label welcome offer')}
          </div>

          <section className="aba-plans-list col-xs-12">
            {(() => {

              if (this.props.plans.items) {
                return <PlansList data={this.props.plans} scrollTo={this.state.autoScroll} urlParams={this.props.appState.urlParams}/>;
              }
              else if (this.props.plans.isFetching) {
                return <Loading />;
              }
              else {
                return <Error retryFunction={ showError => this.props.callActionFetchPlansIfNeeded() }/>;
              }

            })()}
          </section>

          <div className={"promo-expiration-date " + `panel-show-${showSpecialOfferWelcome}`}>
            {this.context.localeManager.getMessage('label promotion expiration') + " " + tomorrow}
          </div>

        </div>

        <FeaturesList />
        <Awards />
        <ReviewsList />
      </div>
    );
  }
}

PlansPage.contextTypes = {
  localeManager: PropTypes.object
};

function mapStateToProps(state) {

  return {
    plans: state.plans,
    appState: state.appState || {
      title: '',
      urlParams: null
    },
    locale: state.userSession.locale
  };
}

export default connect(
  mapStateToProps,
  {callActionFetchPlansIfNeeded, callActionViewPlansPage, callActionSetAppState}
)(PlansPage);
