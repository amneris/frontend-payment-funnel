import React from 'react';
import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { callActionFetchPlansIfNeeded } from '../actions/asynch';
import { findPlanById } from '../actions/utils';
import {
  callActionSetAppState, callActionSelectPlan, callActionGoToPlans,
  callActionViewCheckoutPage, callActionTrackCheckoutCart
} from '../actions/synch';

import Loading from '../components/states/loading';
import Error from '../components/states/error';
import MethodsList from '../components/checkoutPage/methodsList/methodsList';
import Summary from '../components/checkoutPage/summary/summary';
import MoneyBack from '../components/checkoutPage/moneyBack/moneyBack';
import SecureInfo from '../components/checkoutPage/secureInfo/secureInfo';
import SupportInfo from '../components/checkoutPage/supportInfo/supportInfo';

class CheckoutPage extends Component {

  componentWillMount() {
    this.props.callActionSetAppState({
      title: this.context.localeManager.getMessage('title plans page'),
      subtitle: this.context.localeManager.getMessage('subtitle plans page')
    });

    this.props.callActionViewCheckoutPage();
  }

  componentDidMount() {
    this.props.callActionFetchPlansIfNeeded();

    if (this.props.plans) {
      this.selectPlan();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.plans && nextProps.plans) {
      this.selectPlan();
    }
  }

  selectPlan() {
    const planId = this.props.urlParams.plan;

    this.props.callActionSelectPlan(planId);
    // after select plan, shopping cart should be filled in the state
    this.props.callActionTrackCheckoutCart();
  }

  render() {
    return (
      <div className="page-container checkout-container">
        <div className="page-inner-container">

          {(() => {
            if (this.props.isFetching) {
              return <Loading />;
            }
            else if (this.props.plans && this.props.selectedPlan) {
              return (
                <div className="payment-summary-and-methods-container">
                  <div className="checkout-summary-and-upgrade-container col-sm-5">
                    <Summary plan={this.props.selectedPlan} />
                  </div>

                  <hr className="checkout-summary-separator"/>

                  <MethodsList plan={this.props.selectedPlan} />
                </div>
              );
            }
            else {
              let onError = this.props.plans
                ? this.props.callActionGoToPlans
                : this.props.callActionFetchPlansIfNeeded;

              return <Error retryFunction={onError}/>;
            }
          })()}

          <hr/>

          <div className="money-back-and-support-info-container">
            <div className="money-back-and-transaction-info-container col-sm-7">
              <MoneyBack />
              <SecureInfo />
            </div>
            <SupportInfo phoneNumber={this.props.phoneNumber} emailContact={this.props.emailContact} />
          </div>

        </div>
      </div>
    );
  }
}

CheckoutPage.contextTypes = {
  localeManager: PropTypes.object
};

function mapStateToProps(state) {
  const { urlParams, shoppingCart } = state.appState;

  let selectedPlanItem;

  if (shoppingCart && shoppingCart.plan) {
    selectedPlanItem = findPlanById(shoppingCart.plan, state.plans.items);
  }

  return {
    urlParams: urlParams || {},
    plans: state.plans.items,
    isFetching: state.plans.isFetching,
    selectedPlan: selectedPlanItem || undefined,
    phoneNumber: state.appState.phoneNumber,
    emailContact: state.appState.emailContact
  };
}

export default connect(
  mapStateToProps,
  {
    callActionSelectPlan, callActionFetchPlansIfNeeded,
    callActionGoToPlans, callActionViewCheckoutPage, callActionSetAppState, callActionTrackCheckoutCart
  }
)(CheckoutPage);
