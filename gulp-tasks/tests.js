var gulp = require('gulp');
var istanbul = require('gulp-istanbul');
var isparta = require('isparta');
var del = require('del');
var mocha = require('gulp-mocha-co');
var coverageEnforcer = require('gulp-istanbul-enforcer');
var sourcemaps = require('gulp-sourcemaps');
var replace = require('gulp-replace');
var remapIstanbul = require('remap-istanbul/lib/gulpRemapIstanbul');

var requiredCoverage = 40;
var includeUntestedFiles = true;

var paths = {
  server: {
    scripts:  ['source/**/*.js'],
    tests:    ['source/**/*-test.js'],
    coverage: 'reports/coverage'
  }
};

module.exports = function() {

  del(['reports']);

  generateSourceMaps();
};

function generateSourceMaps() {
  console.log('### Generating sourceMaps ###');

  return gulp.src('source/**/*.js')
    // optionally load existing source maps
    .pipe(sourcemaps.init())
    // Covering files
    .pipe(istanbul({ // Covering files
      instrumenter: isparta.Instrumenter,
      includeUntested: includeUntestedFiles
    }))
    .pipe(sourcemaps.write('.'))
    // Write the covered files to a temporary directory
    .pipe(gulp.dest('tests-tmp/'))
    .on('finish', preProcessCode);
}

function linkSourceMaps() {
  // TODO: this should be executed after generating source maps but there is a bug in the lib that appends a new line every time...
  return gulp.src('source/**/*.js')
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('../../../tests-tmp'))
    .pipe(gulp.dest('source'));
}

function preProcessCode() {
  return gulp.src(paths.server.scripts)
    .pipe(istanbul({ // Covering files
      instrumenter: isparta.Instrumenter,
      includeUntested: includeUntestedFiles
    }))
    .pipe(istanbul.hookRequire())
    .on('finish', runTests);
}

function runTests() {
  console.log('### Running tests ###');

  return gulp.src(paths.server.tests, {read: false})
    .pipe(mocha({reporter: 'spec'}))
    .pipe(istanbul.writeReports({
      dir: paths.server.coverage,
      reportOpts: {dir: paths.server.coverage},
      reporters: ['text', 'text-summary', 'json', 'html']
    }))
    .pipe(coverageEnforcer({
      thresholds: {
        statements: requiredCoverage,
        branches: requiredCoverage,
        lines: requiredCoverage,
        functions: requiredCoverage
      },
      coverageDirectory: paths.server.coverage,
      rootDirectory: ''
    }))
    .on('finish', generateReport);
}

function generateReport() {
  console.log('### Generating report ###');

  return gulp.src(paths.server.coverage + '/coverage-final.json')
    .pipe(remapIstanbul({
      reports: {
        'lcovonly': 'reports/coverage/lcov.info'
      }
    }))
    .on('finish', fixReportPaths);
}

function fixReportPaths() {
  console.log('### Preparing report ###');

  return gulp.src(paths.server.coverage + '/lcov.info')
    .pipe(replace('/source', 'source'))
    .pipe(gulp.dest(paths.server.coverage));
}
