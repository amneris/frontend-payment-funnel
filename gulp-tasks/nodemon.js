var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var gutil = require('gulp-util');

module.exports = function () {
  return nodemon({
    script: 'dist/server.js',
    stdout: true,
    watch: ['dist/**/*.js']
  }).on('readable', function () {
      gulp.src('dist/server.js')
        .pipe(gutil.log('Reloading page, please wait...'));
    })
    .on('start', function () {
      gutil.log('----    Nodemon started    -----');
    });
};
