var gulp = require('gulp');

module.exports = function() {

  return gulp.src(['source/assets/fonts/*.*', '!source/assets/fonts/*.json', '!source/assets/fonts/*.css'])
    .pipe(gulp.dest('dist/assets/fonts'));
};
