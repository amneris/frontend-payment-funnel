var gulp = require('gulp');
var revReplace = require('gulp-rev-replace');
var debug = require('gulp-debug');
var gulpif = require('gulp-if');

module.exports = function() {

  var manifest = gulp.src('dist/rev-manifest.json');

  return gulp.src('dist/assets/stylesheets/sprite-*.css')
    .pipe(gulpif(!this.opts.isLocal, revReplace({manifest: manifest})))
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist/assets/stylesheets/')));
};
