var gulp = require('gulp');
var gulpif = require('gulp-if');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var merge = require('merge-stream');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var livereload = require('gulp-livereload');

module.exports = function() {

  var vendors = gulp.src([
      'source/assets/vendor/bootstrap/bootstrap.min.css'
    ])
    .pipe(concat('vendors.css'))
    .pipe(gulpif(!this.opts.isLocal, cssnano()))
    .pipe(gulpif(!this.opts.isLocal, rev()))
    .pipe(gulp.dest('dist/assets/stylesheets'))
    .pipe(gulpif(!this.opts.isLocal, rev.manifest('dist/rev-manifest.json', {base: process.cwd()+'/dist', merge: true})))
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist')));

  var manifest = gulp.src('dist/rev-manifest.json');

  var styles = gulp.src(['source/**/**/*.scss', 'source/**/**/*.css', '!source/assets/vendor/**/*'])
    .pipe(concat('app.scss'))
    .pipe(sass({includePaths: ['source/assets']}))
    .pipe(postcss([require('autoprefixer')]))
    .pipe(gulpif(!this.opts.isLocal, cssnano()))
    .pipe(gulpif(!this.opts.isLocal, rev()))
    .pipe(gulpif(!this.opts.isLocal, revReplace({manifest: manifest})))
    .pipe(gulp.dest('dist/assets/stylesheets'))
    .pipe(gulpif(!this.opts.isLocal, rev.manifest('dist/rev-manifest.json', {base: process.cwd()+'/dist', merge: true})))
    .pipe(livereload())
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist')));

  return merge(vendors, styles);
};
