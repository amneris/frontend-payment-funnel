		var initOrChangeState = function(countryId,selectType,stateName,stateId){
			showCountryState(countryId,selectType,stateName,stateId);
			$(countryId).addEvent("change",function(){
				showCountryState(this,selectType,stateName,stateId);
				var country = this.value;
				if(country != '' && country != '224' && country != '36'){
					$(stateId).value='';
				}
			})
		}
		
		var showCountryState = function(countryId,selectType,stateName,stateId){
			var temp_Country = $(countryId).value;
			if(temp_Country == ''){
				changeState(selectType + "_state", selectType + "_selectOne_state",stateName,stateId);
				
			}else if(temp_Country == '224'){
				changeState(selectType + "_state",selectType + "_unitedStates_state",stateName,stateId);
			}else if(temp_Country == '36'){
				changeState(selectType + "_state",selectType + "_canada_state",stateName,stateId);
			}else{
				changeState(selectType + "_state",selectType + "_text_state",stateName,stateId);
			}
			
		}

		var changeState=function(divObjId,showId,stateName,stateId){
			$(divObjId).getChildren().each(function(item){
				if($(item).getProperty("id")== showId){
					$(item).setStyle("display","block");
					$(item).getFirst().setProperty("name",stateName);
					$(item).getFirst().setProperty("id",stateId);
					$(item).getFirst().setProperty("value", "");
				}else{
					$(item).setStyle("display","none");
					$(item).getFirst().setProperty("name","");
					$(item).getFirst().setProperty("id","");
				}
			});
		}