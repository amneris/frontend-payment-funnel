/*
 * Safari is known to be strict about permissions in iframes, especially when the domain of the iframe page is different from the domain of the parent page.
 * Some would even say paranoically strict.
 * Safari will block you from setting cookies for the third-party domain (the different domain in the iframe), unless you already have cookies set for that domain.
 */

function safariCookieFix(){
	if(navigator.userAgent.indexOf('Safari')!=-1 && navigator.userAgent.indexOf('Chrome')==-1){
		var cookies = document.cookie;
		if(top.location != document.location && !cookies){
			href=document.location.href;
			href=(href.indexOf('?')==-1) ? (href+'?') : (href+'&');
		    top.location.href =href+'safariref='+encodeURIComponent(document.referrer);
		}
		else {
			date=new Date();
			ts=date.getTime();
			// Set expire date to be 2 weeks. 
			date.setDate(date.getDate()+14);
			document.cookie='ts='+ts + ((date==null) ? "" : ";expires="+date.toGMTString());
			rerefidx=document.location.href.indexOf('safariref=');
			if(rerefidx!=-1){
			  href=decodeURIComponent(document.location.href.substr(rerefidx+10));
			  window.location.replace(href);
			}
		}
	}
}