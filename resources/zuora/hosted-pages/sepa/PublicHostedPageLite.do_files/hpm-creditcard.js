function fromatStrWithBlank(str, pos) {
	str = str.replace(/\s+/g, "");
	var rstr = "";
	for ( var i = 0; i < str.length; i++) {
		if (i == pos[0]) {
			rstr += " ";
			pos.shift();
		}
		rstr += str.charAt(i);
	}
	return rstr;
}

function isCreditCard(cc) {
	// use Luhn checksum algorithm to check if the credit card is valid
	return luhnCheck(cc);
}
/*---------------------------------------------------------------*/
/* Purpose : Validate CC number following Visa specs */
/*---------------------------------------------------------------*/
function isVisa(cc) {
	if (cc && cc.match(/^4[0-9]{12}(?:[0-9]{3})?$/g)) {
		return true
	}
	return (false);
}

/*---------------------------------------------------------------*/
/* Purpose : Validate CC number following MasterCard specs */
/*---------------------------------------------------------------*/
function isMC(cc) {
	if (cc && cc.match(/^5[1-5][0-9]{14}(?:\d{3})?$/g)) {
		return true
	}
	return (false);
}
/*---------------------------------------------------------------*/
/* Purpose : Validate CC number following AMEX specs */
/*---------------------------------------------------------------*/
function isAmex(cc) {
	if (cc && cc.match(/^3[47][0-9]{13}$/g)) {
		return true
	}
	return (false);
}
/*---------------------------------------------------------------*/
/* Purpose : Validate CC number following Discover specs */
/*---------------------------------------------------------------*/
function isDiscover(cc) {
	if (cc
			&& cc
					.match(/^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/)
			&& cc.length == 16) {
		return true
	}
	return (false);
}

/*---------------------------------------------------------------*/
/* Purpose : Validate CC number following JCB specs */
/*---------------------------------------------------------------*/
function isJCB(cc) {
	if (cc && cc.match(/^(?:2131|1800|35[0-9]{3})[0-9]{11}$/g)) {
		return true;
	}
	return false;
}

/*---------------------------------------------------------------*/
/* Purpose : Validate CC number following Diners specs */
/*---------------------------------------------------------------*/
function isDiners(cc) {
	if (cc && cc.match(/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/g)) {
		return true;
	}
	return false;
}