var ZCreditly = (function() {

  var preventDefault = function (e) {
    if (e.preventDefault) {
      e.preventDefault();
    } else { //For IE
      e.returnValue = false;
    }
  };

  var getInputValue = function(e, selector) {
    var inputKey = String.fromCharCode(e.which);
    var element = document.querySelector(selector);
    var caret = jQuery(element).caret(); //Cursor position
    var selectionText = getSelectionText(element);

    var inputValue = [element.value.slice(0, caret), inputKey, element.value.slice(caret + selectionText.length)].join('');
    return getNumber(inputValue);
  };

  var getNumber = function(string) {
    return string.replace(/[^\d]/g, "");
  };
  
  var isIE8 = function() {
  	if (document.all && document.querySelector && !document.addEventListener) {
  		return true;  
      }else {
      	return false;
      }
  };
  
  var removeErrors = function(element) {
    element.className = element.className.replace(/\bhas-error\b/,'');
  };

  var reachedMaximumLength = function(e, maximumLength, selector) {
    return getInputValue(e, selector).length > maximumLength;
  };

  var isEscapedKeyStroke = function(e) {
    if (e.metaKey || e.ctrlKey) return true;

    if (e.which === 32) return false;

    if (e.which === 0) return true;

    if (e.which < 33) return true;

    return false;
  };

  var isNumberEvent = function(e) {
    return (/^\d+$/.test(String.fromCharCode(e.which)));
  };

  var onlyAllowNumeric = function(e, maximumLength, selector) {
    e.preventDefault();
    return isNumberEvent(e);
  };

  var isAmericanExpress = function(number) {
    return number.match("^(34|37)");
  };

  var getSelectionText = function(element) {
    var selectionText ;
    if(document.selection) { //IE
      selectionText = document.selection.createRange().text;
    } else { //Chrome & FF
      var start = element.selectionStart;
      var end = element.selectionEnd ;
      selectionText = element.value.slice(start,end);
    }
    return selectionText;
  };

  var shouldProcessInput = function(e, maximumLength, selector) {
    var target = e.target;

    var keyCode = e.keyCode || e.which;
    if(!/\d/.test(String.fromCharCode(keyCode))) { //Only allow numbers
      preventDefault(e);
      return false;
    } else if(getSelectionText(target)){ //When selected something, pass.
      return true;
    } else if (reachedMaximumLength(e, maximumLength, selector)) {
      preventDefault(e);
      return false;
    } else {
      return (!isEscapedKeyStroke(e)) && onlyAllowNumeric(e, maximumLength, selector);
    }
  };

  var NumberInput = (function() {
    var americanExpressSpaces = [4, 10, 15];
    var defaultSpaces = [4, 8, 12, 16];

    var getMaximumLength = function(isAmericanExpressCard) {
      if (isAmericanExpressCard) {
        return 15;
      } else {
        return 16;
      }
    };

    var createNumberInput = function (selector) {

      var numberInputListener = function (e) {
        /*Convert native event to jQuery event to avoid cross-browser matters.*/
        var _e = jQuery.event.fix(e);
        if (shouldIgnore(_e)) {
          return true;
        }
        var element = document.querySelector(selector);
        removeErrors(element);
        var number = getInputValue(_e, selector);
        var isAmericanExpressCard = isAmericanExpress(number);
        var maximumLength = getMaximumLength(isAmericanExpressCard);

        if (shouldProcessInput(_e, maximumLength, selector)) {
          var newInput;
          var oldCurPos = jQuery(element).caret();
          if (isAmericanExpressCard) {
            newInput = addSpaces(number, americanExpressSpaces);
          } else {
            newInput = addSpaces(number, defaultSpaces);
          }
          preventDefault(e);
          element.value = newInput;
          setCursor(element, oldCurPos);
        }

        function setCursor(element, oldCurPos) {
          var newCurPos;
          if (isAmericanExpressCard) {
            newCurPos = (oldCurPos === 4 || oldCurPos === 11 ) ? oldCurPos + 2 : oldCurPos + 1;
          } else {
            newCurPos = oldCurPos % 5 === 4 ? oldCurPos + 2 : oldCurPos + 1;//%5==4 means the end of a segment.
          }
          jQuery(element).caret(newCurPos);
        }

        function shouldIgnore(_e) {
          var keyCode = _e.keyCode || _e.which;
          return keyCode === 37 || //left arrow
            keyCode === 39 || //right arrow
            keyCode === 8 || //backspace
            keyCode === 9 || // tab
            ((e.metaKey || e.ctrlKey)); //ctrl or command + a,c,v , copy or paste
        }
      };

      var numberPasteInputListener = function (e) {
        // just for paste values
        var $ = jQuery,
            element = document.querySelector(selector);

        setTimeout(function () {
          var number = element.value.replace(/ /g, "");
          var newInput;
          if (!$.isNumeric(number)) {
            $(element).val($(element).data('oldData') || '');
          } else {
            var isAmericanExpressCard = isAmericanExpress(number);
            if (isAmericanExpressCard) {
              newInput = addSpaces(number, americanExpressSpaces);
            } else {
              newInput = addSpaces(number, defaultSpaces);
            }
            $(element).val(newInput).data('oldData', newInput);
          }
        }, 10);
      };

      jQuery(selector)
        .on("keypress", numberInputListener)
        .on("paste", numberPasteInputListener);
    };

    var addSpaces = function(number, spaces) {
      var parts = [];
      var j = 0;
      for (var i=0; i<spaces.length; i++) {
        if (number.length > spaces[i]) {
          parts.push(number.slice(j, spaces[i]));
          j = spaces[i];
        } else {
          if (i < spaces.length) {
            parts.push(number.slice(j, spaces[i]));
          } else {
            parts.push(number.slice(j));
          }
          break;
        }
      }

      if (parts.length > 0) {
        return parts.join(" ");
      } else {
        return number;
      }
    };

    return {
      createNumberInput: createNumberInput
    };
  })();

  var CardTypeListener = (function() {
    var determineCardType = function(value) {
      if (/^(34|37)/.test(value)) {
        return "AmericanExpress";
      } else if (/^4/.test(value)) {
        return "Visa";
      } else if (/^5[0-5]/.test(value)) {
        return "MasterCard";
      } else if (/^(6011|622|64[4-9]|65)/.test(value)) {
        return "Discover";
      } else if(isJCB && isJCB(value)) {
        return "Discover"; /*JCB is supported as Discover*/
      }else if(isDiners && isDiners(value)){
        return "Discover"; /*Diners Club is supported as Discover*/
      } else {
        return "";
      }
    };
    
    var changeCardType = function(numberSelector) {
      var element = document.querySelector(numberSelector);
      var changeCardTypeListener = function (e) {
        var data = document.querySelector(numberSelector).value;
        var checkedType = determineCardType(getNumber(data));
        $(document.body).getElements("div[name='card-image-creditCardType']").each(function (item) {
          if (checkedType && $(item).className.indexOf("card-image-" + checkedType) > -1) {
            if ($(item).className.indexOf("card-image-" + checkedType + "-disabled") > -1) {
              $(item).removeClass("card-image-" + checkedType + "-disabled");
              $(item).addClass("card-image-" + checkedType);
            }
            $(item).setAttribute("aria-selected","true");
            $(item).setAttribute("aria-hidden","false");
            $(item).setAttribute("aria-disabled","false");
          } else {
            var classes = $(item).className.split(" "), ctype;
            for (var i = 0; i < classes.length; i++) {
              if (classes[i].indexOf("card-image-") > -1 && classes[i].indexOf("-disabled") < 0) {
                ctype = classes[i].substring("card-image-".length);
                break;
              }
            }

            if (typeof(ctype) != "undefined" && $(item).className.indexOf("card-image-" + ctype + "-disabled") < 0) {
              $(item).removeClass("card-image-" + ctype);
              $(item).addClass("card-image-" + ctype + "-disabled");
            }
            $(item).setAttribute("aria-selected","false");
            $(item).setAttribute("aria-hidden","true");
            $(item).setAttribute("aria-disabled","true");
          }
        });
        var cardType = jQuery("input[id='input-creditCardType']");
        cardType.val(checkedType);
      };
      
      if(isIE8()) {
//    	  element.attachEvent("changed_input", changeCardTypeListener);
    	  element.attachEvent("onkeypress", changeCardTypeListener);
    	  element.attachEvent("onkeydown", changeCardTypeListener);
    	  element.attachEvent("onkeyup", changeCardTypeListener);
      }else{
//    	  element.addEventListener("changed_input", changeCardTypeListener);
    	  element.addEventListener("keypress", changeCardTypeListener);
    	  element.addEventListener("keydown", changeCardTypeListener);
    	  element.addEventListener("keyup", changeCardTypeListener);
      }
      changeCardTypeListener();
    };
    return {
      changeCardType: changeCardType
    };

  })();

  var initialize = function(creditCardNumberSelector, detection) {
    NumberInput.createNumberInput(creditCardNumberSelector);
    if(detection) {
    	CardTypeListener.changeCardType(creditCardNumberSelector);
    }
  };

  return {
	initialize: initialize
  };
})();
