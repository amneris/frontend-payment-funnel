###### Sample Resource Bundle Template for HPM forms ######
# Please enter the translated text for each label or text field below
# After editing, save it and upload it to the appropriate locale using the Hosted Payment Page translation settings page
# Please note, in the template below, blank lines and lines beginning with # will be ignored

# Legend translation
page.label.required=

# Label translations
bankTransferType=

bankTransferAccountType=

bankAccountNumber=International Bank Account Number (IBAN)

bankAccountName=Titulaire du compte

bankCode=Code de la banque

bankName=Nom de la banque

bankBranchCode=Code branche

bankCheckDigit=Clef de contr\u00F4le

bankStreetNumber= Num\u00E9ro

bankStreetName=Rue

bankCity=Ville

bankPostalCode=Code postal

businessIdentificationCode=Business Identifier Code (BIC/SWIFT)

firstName=Pr\u00E9nom

lastName=Nom

streetNumber=Num\u00E9ro

streetName=Rue

city=Ville

state=R\u00E9gion ou province

postalCode=Code postal

country=Pays

email=E-mail

IBAN=International Bank Account Number (IBAN)

mandateReceivedStatus=

existingMandateStatus=

mandateId=

mandateCreationDateYear=

mandateCreationDateMonth=

mandateCreationDateDay=

mandateUpdateDateDay=

mandateUpdateDateMonth=

mandateUpdateDateYear=

agreement_checkbox=

# Error message translations which will be displayed when certain conditions are met
page.message.requiredField=Champ obligatoire

# Title, description and submit button translations
page.title=

page.desc=

page.label.submit=Envoyer

